﻿using System;
using System.Collections.Generic;
using Common.Managers;
using UnityEngine;
using MarkLight.Views.UI;
using MarkLight;
using MiceCompany;

public class ChView : Region
{
    public int ChIndex = -1;
    public _SpriteAsset ChSprite;
    public Label Title;
    public Button ChSelect;

    public void ChapterClicked()
    {
        GameManager.Instance.SetTimeScale(0);
        //Debug.Log("Ch: "+ChIndex + ". chapter seçildi");
        SubChaptersView.Instance.ChangeChapter(ChIndex);
        UIManager.Instance.ChangeSubView(typeof(SubChaptersView));

    }
}
