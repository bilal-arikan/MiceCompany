﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using MarkLight.Views.UI;
using MarkLight;

public class SubChSelectButton : UIView
{
    public int SubChIndex = -1;
    public CheckBox CheckBox;
    public Image LockImage;
    public _string Ranking;
    public Image Star1;
    public Image Star2;
    public Image Star3;
    public bool Unlocked
    {
        get
        {
            return !LockImage.IsActive;
        }
        set
        {
            LockImage.IsActive.Value = !value;
        }
    }

    [NonSerialized]
    public SubChView ContainerSubChView;

    public void OnClicked()
    {
        foreach (var btn in ContainerSubChView.SubChapters)
        {
            if(btn != this)
            {
                btn.CheckBox.IsChecked.Value = false;
            }
        }

        if (!CheckBox.IsChecked)
        {
            SubChaptersView.Instance.SubChapterSelected(-1);
        }
        else if (!Unlocked)
        {
            SubChaptersView.Instance.ShowUnlock(SubChIndex);
        }
        else
        {
            SubChaptersView.Instance.SubChapterSelected(SubChIndex);
        }

        //Debug.Log("SubCh: " + SubChIndex + ". Selected");

    }

    public void ShowStars(int count)
    {
        if(count == 0)
        {
            Star1.IsActive.Value = false;
            Star2.IsActive.Value = false;
            Star3.IsActive.Value = false;
        }
        else if (count == 1)
        {
            Star1.IsActive.Value = false;
            Star2.IsActive.Value = false;
            Star3.IsActive.Value = true;
        }
        else if(count == 2)
        {
            Star1.IsActive.Value = false;
            Star2.IsActive.Value = true;
            Star3.IsActive.Value = true;
        }
        else if (count == 3)
        {
            Star1.IsActive.Value = true;
            Star2.IsActive.Value = true;
            Star3.IsActive.Value = true;
        }
    }
}
