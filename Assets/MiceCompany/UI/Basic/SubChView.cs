﻿using System;
using System.Collections.Generic;
using MarkLight;
using UnityEngine;
using MarkLight.Views.UI;

public class SubChView : Region
{
    public int ChIndex = -1;
    public Image MapImage;
    public _SpriteAsset MapSprite;

    public List<SubChSelectButton> SubChapters = new List<SubChSelectButton>();

}
