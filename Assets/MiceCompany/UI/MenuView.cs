﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MarkLight.Views;
using MarkLight.Views.UI;
using Common.Managers;
using MiceCompany;
using UnityEngine;
using Facebook.Unity;

public class MenuView : SingletonView<MenuView>
{
    public Region DarkLeft;
    public Region DarkRight;
    public Button StartButton;
    public Button ProfileButton;
    public Button MarketButton;


    void OnEnable()
    {
#if UNITY_EDITOR
        //StartButton.IsDisabled.Value = false;
#endif
    }

    //Left
    public void ProfilePanel()
    {
        UIManager.Instance.ChangeSubView(typeof(ProfileView));
        DarkLeft.IsActive.Value = false;
        DarkRight.IsActive.Value = false;
    }
    public void SocialNetworkPanel()
    {
        DarkLeft.IsActive.Value = true;
    }
    public void NewsPanel()
    {
        UIManager.Instance.ChangeSubView(typeof(NewsView));
        DarkLeft.IsActive.Value = false;
        DarkRight.IsActive.Value = false;
    }
    public void SettingsPanel()
    {
        UIManager.Instance.ChangeSubView(typeof(OptionsView));
        DarkLeft.IsActive.Value = false;
        DarkRight.IsActive.Value = false;
    }
    // Right
    public void OpenStore()
    {
        UIManager.Instance.ChangeSubView(typeof(IAPView));
        DarkLeft.IsActive.Value = false;
        DarkRight.IsActive.Value = false;
    }
    public void Language()
    {
        UIManager.Instance.ChangeSubView(typeof(LangSelection));
        DarkLeft.IsActive.Value = false;
        DarkRight.IsActive.Value = false;
    }
    public void Credits()
    {
        UIManager.Instance.ChangeSubView(typeof(CreditsView));
        DarkLeft.IsActive.Value = false;
        DarkRight.IsActive.Value = false;
    }
    public void About()
    {
        UIManager.Instance.ChangeSubView(typeof(AboutView));
        DarkLeft.IsActive.Value = false;
        DarkRight.IsActive.Value = false;
        //DarkRight.IsActive.Value = true;
    }

    // Others
    public void PlayClick()
    {
        UIManager.Instance.ChangeSubView(typeof(ChaptersView));
        DarkLeft.IsActive.Value = false;
        DarkRight.IsActive.Value = false;
    }

    public void ExitGame()
    {
        UIManager.Instance.ShowAskToUser("Are you sure ?", (s) =>
        {
            if(s)
                Application.Quit();
        });
    }
    public void ShowConsole()
    {
        Common.CConsole.Show();
    }

    public void DarkRegionClick()
    {
        DarkLeft.IsActive.Value = false;
        DarkRight.IsActive.Value = false;
    }


    public void Facebook()
    {
        SocialManager.Instance.Connect(SocialManager.Network.Facebook, (s) =>
        {
            UIManager.Instance.ShowToast("Facebook LogIn: " + (s ? "Success" : " Failed"));
            UIManager.Instance.SetWaitingtState(false);
        });
        UIManager.Instance.SetWaitingtState(true);
        DarkLeft.IsActive.Value = false;
        DarkRight.IsActive.Value = false;
    }
    public void PlayServices()
    {
        SocialManager.Instance.Connect(SocialManager.Network.GooglePlay, (s) =>
        {
            UIManager.Instance.ShowToast("GooglePlay LogIn: " + (s ? "Success" : " Failed"));
            UIManager.Instance.SetWaitingtState(false);
        });
        UIManager.Instance.SetWaitingtState(true);
        DarkLeft.IsActive.Value = false;
        DarkRight.IsActive.Value = false;
    }
    public void GameJolt()
    {
        SocialManager.Instance.Connect(SocialManager.Network.GameJolt, (s) =>
        {
            UIManager.Instance.ShowToast("GamoJolt LogIn: " + (s ? "Success" : " Failed"));
            UIManager.Instance.SetWaitingtState(false);
        });
        UIManager.Instance.SetWaitingtState(true);
        DarkLeft.IsActive.Value = false;
        DarkRight.IsActive.Value = false;
    }

    public void BackToGame()
    {
        DarkLeft.IsActive.Value = false;
        DarkRight.IsActive.Value = false;
    }

    public void WebPage() { Application.OpenURL(ConfigManager.website_url); }
    public void CustomShare() { SocialManager.Instance.ShareText(ConfigManager.invite_title, ConfigManager.invite_message); }
    public void LikeApp() { SocialManager.Instance.OpenMarketPage(); }
    public void FacebookPage() { Application.OpenURL(ConfigManager.facebook_page); }
    public void FacebookShare() { FB.ShareLink(); }
    public void FacebookInvite() {  }
    public void TwitterPage() { Application.OpenURL(ConfigManager.twitter_page); }
    public void TwitterShare() {  }
    public void GooglePage() { Application.OpenURL(ConfigManager.googleplus_page); }
    public void GoogleShare() { }
    public void InstagramPage() { Application.OpenURL(ConfigManager.instagram_page); }
    public void InstagramShare() {  }
    public void TumblrPage() { }
    public void TumblrShare() { }
}
