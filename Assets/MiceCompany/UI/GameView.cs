﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using MarkLight;
using MarkLight.Views.UI;
using Common.Managers;
using Common.Tools;
using MiceCompany;
using Common.Data;

public class GameView : SingletonView<GameView>
{
    public ViewSwitcher GamePlayButtons;
    public Label TimerLabel;
    public _string CurrentScore;
    public _string Money;
    public _string SpecialMoney;
    public _string Arrives;
    public _string Deaths;
    public _string Alives;

    public Image ToWinArriveImage;
    public Image ToWinNotDeathImage;
    public Image ToWinSecondsImage;
    public _string ToWinArrive;
    public _string ToWinNotDeath;
    public _string ToWinSeconds;


    public Region MouseListRegion;
    public Region BuffListRegion;
    public Region PlacableListRegion;
    public MCList MouseList;
    public MCList BuffList;
    public MCList PlacableList;
    public MCList LootedList;


    protected void Start()
    {
        // Bir Item Haritadan kaldırılırsa
        EventManager.StartListeningObjectEvent(E.ItemRemoved, (o) =>
        {
            OnItemRemoved(o as Item);
            GameManager.Instance.UpdateCurrentSituation();
        });
        // Bir Item Haritaya yerleştirilirse
        EventManager.StartListeningObjectEvent(E.ItemPlaced, (o) =>
        {
            OnItemPlaced();
            GameManager.Instance.UpdateCurrentSituation();
        });
        // Bir Loot toplanırsa
        EventManager.StartListeningObjectEvent(E.LootableLooted, (o) =>
        {
            LootedList.AddListViewItem((o as Lootable).GetComponent<ShowableOnList>());
        });
    }

    private void OnEnable()
    {
        SimpleGesture.OnTap(GameManager.Instance.OnClickedToGameArea);
    }

    private void OnDisable()
    {
        MouseList.Clear();
        BuffList.Clear();
        PlacableList.Clear();
        LootedList.Clear();

        CurrentScore.Value = 0.ToString();
        Money.Value = 0.ToString();
        SpecialMoney.Value = "+ "+ 0.ToString();
        Alives.Value = 0.ToString();
        Arrives.Value = 0.ToString();
        Deaths.Value = 0.ToString();

        ToWinArrive.Value = 0.ToString();
        ToWinNotDeath.Value = 0.ToString();
        ToWinSeconds.Value = 0.ToString();

        GameManager.Instance.SelectedItemView = null;
        MouseList.Items.SelectedIndex = -1;
        BuffList.Items.SelectedIndex = -1;
        PlacableList.Items.SelectedIndex = -1;
        MouseList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);
        BuffList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);
        PlacableList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);

        SimpleGesture.StopTap(GameManager.Instance.OnClickedToGameArea);
    }

    void AnyItemSelected(ItemSelectionActionData data)
    {
        ShowableOnList selected = (ShowableOnList)data.Item;
        GameManager.Instance.SelectedItemView = selected.GetComponent<Item>();


        if (GameManager.Instance.SelectedItemView is PlayerMice)
        {
            BuffList.Items.SelectedIndex = -1;
            PlacableList.Items.SelectedIndex = -1;
            BuffList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);
            PlacableList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);
        }
        if (GameManager.Instance.SelectedItemView is Buff)
        {
            MouseList.Items.SelectedIndex = -1;
            PlacableList.Items.SelectedIndex = -1;
            MouseList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);
            PlacableList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);
        }
        if (GameManager.Instance.SelectedItemView is Placable)
        {
            BuffList.Items.SelectedIndex = -1;
            MouseList.Items.SelectedIndex = -1;
            BuffList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);
            MouseList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);
        }
    }

    public void AnyItemDeselected(ItemSelectionActionData data)
    {
        if (MouseList.Items.SelectedItem == null && BuffList.Items.SelectedItem == null && PlacableList.Items.SelectedItem == null)
        {
            GameManager.Instance.SelectedItemView = null;
        }
    }


    public void InitializeSelectedItems(
        List<PlayerMice> ownedMouses, List<Buff> ownedBuffs, List<Placable> ownedPlacables,
         List<PlayerMice> givenMouses, List<Buff> givenBuffs, List<Placable> givenPlacables)
    {
        GameManager.MainTimer.ResetTimer();
        TimerLabel.Text.Value = Timer.TimeToString(LevelLoadManager.CurrentSubCh.ToWinSeconds > 0 ? LevelLoadManager.CurrentSubCh.ToWinSeconds : 0);

        ToWinArrive.Value = LevelLoadManager.CurrentSubCh.ToWinArrive.ToString();
        ToWinArriveImage.IsActive.Value = LevelLoadManager.CurrentSubCh.ToWinArrive > 0;
        ToWinNotDeath.Value = LevelLoadManager.CurrentSubCh.ToWinNotDeath.ToString();
        ToWinNotDeathImage.IsActive.Value = LevelLoadManager.CurrentSubCh.ToWinNotDeath > 0;
        ToWinSeconds.Value = LevelLoadManager.CurrentSubCh.ToWinSeconds.ToString();
        ToWinSecondsImage.IsActive.Value = LevelLoadManager.CurrentSubCh.ToWinSeconds > 0;

        List<ShowableOnList> allMices = new List<ShowableOnList>();
        allMices.AddRange(givenMouses.ConvertAll(m => m.GetComponent<ShowableOnList>()));
        allMices.AddRange(ownedMouses.ConvertAll(m => m.GetComponent<ShowableOnList>()));
        List<ShowableOnList> allBuffs = new List<ShowableOnList>();
        allBuffs.AddRange(givenBuffs.ConvertAll(m => m.GetComponent<ShowableOnList>()));
        allBuffs.AddRange(ownedBuffs.ConvertAll(m => m.GetComponent<ShowableOnList>()));
        List<ShowableOnList> allPlacables = new List<ShowableOnList>();
        allPlacables.AddRange(givenPlacables.ConvertAll(m => m.GetComponent<ShowableOnList>()));
        allPlacables.AddRange(ownedPlacables.ConvertAll(m => m.GetComponent<ShowableOnList>()));

        MouseList.SetItems(allMices);
        BuffList.SetItems(allBuffs);
        PlacableList.SetItems(allPlacables);

        MouseListRegion.IsActive.Value = MouseList.Items.Count > 0;
        BuffListRegion.IsActive.Value = BuffList.Items.Count > 0;
        PlacableListRegion.IsActive.Value = PlacableList.Items.Count > 0;

        GamePlayButtons.SwitchTo(0, true);
    }

    public void OnItemPlaced()
    {
        if(MouseList.Items.SelectedIndex > -1)
        {
            MouseList.RemoveListViewItem(MouseList.Items.SelectedItem);
        }
        if (BuffList.Items.SelectedIndex > -1)
        {
            BuffList.RemoveListViewItem(BuffList.Items.SelectedItem);
            BuffList.Items.SelectedIndex = -1;
            BuffList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);
            GameManager.Instance.SelectedItemView = null;
        }
        if (PlacableList.Items.SelectedIndex > -1)
        {
            PlacableList.RemoveListViewItem(PlacableList.Items.SelectedItem);
        }


        MouseListRegion.IsActive.Value = MouseList.Items.Count > 0;
        BuffListRegion.IsActive.Value = BuffList.Items.Count > 0;
        PlacableListRegion.IsActive.Value = PlacableList.Items.Count > 0;
    }

    public void OnItemRemoved(Item i)
    {
        if(i is PlayerMice)
        {
            var item = PreLoadedAssets.Instance.MousePrefabs.Find(m => m.Name == i.Name);
            if (item != null)
                MouseList.AddListViewItem(item.GetComponent<ShowableOnList>());
            else
                Debug.LogError("Remove Mice: Name couldnt find " + i.Name);
        }
        else if(i is Buff)
        {
            var item = PreLoadedAssets.Instance.BuffPrefabs.Find(m => m.Name == i.Name);
            if (item != null)
                BuffList.AddListViewItem(item.GetComponent<ShowableOnList>());
            else
                Debug.LogError("Remove Buff: Name couldnt find " + i.Name);
        }
        else if (i is Placable)
        {
            var item = PreLoadedAssets.Instance.PlacablePrefabs.Find(m => m.Name == i.Name);
            if (item != null)
                PlacableList.AddListViewItem(item.GetComponent<ShowableOnList>());
            else
                Debug.LogError("Remove Placable: Name couldnt find " + i.Name);
        }

        MouseListRegion.IsActive.Value = MouseList.Items.Count > 0;
        BuffListRegion.IsActive.Value = BuffList.Items.Count > 0;
        PlacableListRegion.IsActive.Value = PlacableList.Items.Count > 0;
    }

    public void StartGame()
    {
        GameManager.Instance.StartPlaying();

        GamePlayButtons.SwitchTo(1,true);

        GameManager.Instance.SelectedItemView = null;
        BuffList.Items.SelectedIndex = -1;
        MouseList.Items.SelectedIndex = -1;
        PlacableList.Items.SelectedIndex = -1;
        BuffList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);
        MouseList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);
        PlacableList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);

        PlacableListRegion.IsActive.Value = false;
    }

    public void FinishGame()
    {
        GameManager.MainTimer.PauseTimer();
        EventManager.TriggerEvent(E.PlayerWon);
    }

    public void SetSpeed0()
    {
        GameManager.Instance.SetTimeScale(0);
    }

    public void SetSpeed05()
    {
        GameManager.Instance.SetTimeScale(0.5f);
    }

    public void SetSpeed1()
    {
        GameManager.Instance.SetTimeScale(1);
    }

    public void SetSpeed2()
    {
        GameManager.Instance.SetTimeScale(2);
    }

    public void ReloadCh()
    {
        LevelLoadManager.Instance.ChangeChapter(LevelLoadManager.CurrentIndex);
    }

    public void DayNight()
    {
        GameManager.Instance.SetLightSystem(!GameManager.Instance.IsNightMode);
    }

    public void BackToGame()
    {
        GameManager.Instance.Pause();
        UIManager.Instance.ChangeSubView(typeof(PauseView));
    }
}

