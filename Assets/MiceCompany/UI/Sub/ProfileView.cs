﻿using MarkLight;
using MarkLight.Views.UI;
using System.Collections.Generic;
using UnityEngine;
using Common.Managers;
using Common.Tools;
using MiceCompany;
using Common.Data;

public class ProfileView : SingletonView<ProfileView>
{
    public MCList InventoryList;

    public Region SummaryView;
    public _string ItemName;
    public _SpriteAsset ItemImage;
    public _string ItemInfo1;
    public _string ItemInfo2;

    public _SpriteAsset Image;
    public _string DisplayName;
    public _string Money;
    public _string SpecMoney;
    public _string PlayTime;
    public _string Level;
    public _string Xp;

    public Button AchGooglePlayButton;
    public Button AchGameJoltButton;
    public Button ScrGooglePlayButton;
    public Button ScrGameJoltButton;
    public Button ProfileFacebookButton;
    public Button ProfileGooglePlayButton;
    public Button ProfileGameJoltButton;


    private void Start()
    {
        EventManager.StartListeningObjectEvent(E.PlayerPhotoChanged, (profile) =>
        {
            Image.Value = new MarkLight.SpriteAsset(new MarkLight.UnityAsset("ProfileImage", PlayerManager.CurrentProfile.Photo));
        });
    }

    protected void OnEnable()
    {
        Image.Value = new MarkLight.SpriteAsset(new MarkLight.UnityAsset("ProfileImage", PlayerManager.CurrentProfile.Photo));
        PlayTime.Value = Timer.TimeToString( Time.time );
        Level.Value = PlayerManager.CurrentPlayer.Level.ToString();
        Xp.Value = PlayerManager.CurrentPlayer.Xp.ToString();

        DisplayName.Value = PlayerManager.CurrentProfile.DisplayName;
        Money.Value = PlayerManager.CurrentPlayer.Money.ToString();
        SpecMoney.Value = PlayerManager.CurrentPlayer.SpecialMoney.ToString();

        // Kullanıcının sahip oldukları
        InventoryList.SetItems(PlayerManager.CurrentPlayer.OwnedBuffs.ConvertAll(b => PreLoadedAssets.Instance.BuffPrefabs[b].GetComponent<ShowableOnList>()));

        // Set Achievement Buttons
        AchGooglePlayButton.IsDisabled.Value = !SocialManager.Instance.IsSignedInGooglePlay;
        AchGameJoltButton.IsDisabled.Value = !SocialManager.Instance.IsSignedInGameJolt;
        // Set ScoreTable Buttons
        ScrGooglePlayButton.IsDisabled.Value = !SocialManager.Instance.IsSignedInGooglePlay;
        ScrGameJoltButton.IsDisabled.Value = !SocialManager.Instance.IsSignedInGameJolt;
        // Set Change Profile Buttons
        ProfileFacebookButton.IsActive.Value = SocialManager.Instance.IsSignedInFacebook;
        ProfileGooglePlayButton.IsActive.Value = SocialManager.Instance.IsSignedInGooglePlay;
        ProfileGameJoltButton.IsActive.Value = SocialManager.Instance.IsSignedInGameJolt;
    }

    /*public void ShowProfile(Player.Profile p)
    {
        ProfileView.Instance.Image.Value = new MarkLight.SpriteAsset(new MarkLight.UnityAsset("ProfileImage", p.Photo));
        ProfileView.Instance.Username.Value = p.DisplayName;
        ProfileView.Instance.DisplayName.Value = PlayerManager.CurrentPlayer.ID;
        ProfileView.Instance.Email.Value = p.Email;
    }*/

    public void BackToGame()
    {
        UIManager.Instance.ChangeSubView(null);
    }

    public void OpenStore()
    {
        UIManager.Instance.ChangeSubView(typeof(IAPView));
    }

    public void ShowItemInfo(Item b)
    {
        ItemName.Value = b.name;
        ItemImage.Value = b.GetComponent<ShowableOnList>().ListViewSprite;

        if (b is Buff)
        {
            if((b as Buff).effectTime == Buff.EffectTime.InPerion)
                ItemInfo1.Value = (b as Buff).Time + "  Seconds\n" + (b as Buff).AreaSize;
            else if ((b as Buff).effectTime == Buff.EffectTime.InMoment)
                ItemInfo1.Value = "Just Moment\n" + (b as Buff).AreaSize;
            else if ((b as Buff).effectTime == Buff.EffectTime.Forever)
                ItemInfo1.Value = "Forever\n" + (b as Buff).AreaSize;

            ItemInfo2.Value = b.Summary;
        }
    }

    protected virtual void ListItemSelected(ItemSelectionActionData d)
    {
        Item selected = (d.Item as ShowableOnList).GetComponent<Item>();
        ShowItemInfo(selected);
        SummaryView.IsVisible.Value = true;
    }

    protected virtual void ListItemDeselected(ItemSelectionActionData d)
    {
        if(InventoryList.Items.SelectedIndex == -1)
            SummaryView.IsVisible.Value = false;
    }

    public void AchGooglePlay()
    {
        SocialManager.Instance.ShowAchievements(SocialManager.Network.GooglePlay);
    }
    public void AchGameJolt()
    {
        SocialManager.Instance.ShowAchievements(SocialManager.Network.GameJolt);
    }
    public void ScrGooglePlay()
    {
        SocialManager.Instance.ShowLeaderboard(SocialManager.Network.GooglePlay);
    }
    public void ScrGameJolt()
    {
        SocialManager.Instance.ShowLeaderboard(SocialManager.Network.GameJolt);
    }

    public void ProfileFacebook()
    {
        //PlayerManager.CurrentPlayer.PlayerProfiles.ActiveProfile = SocialManager.Network.Facebook;
        //ShowProfile(PlayerManager.CurrentPlayer.CurrentProfile);
    }
    public void ProfileGooglePlay()
    {
        //PlayerManager.CurrentPlayer.PlayerProfiles.ActiveProfile = SocialManager.Network.GooglePlay;
        //ShowProfile(PlayerManager.CurrentPlayer.CurrentProfile);
    }
    public void ProfileGameJolt()
    {
        //PlayerManager.CurrentPlayer.PlayerProfiles.ActiveProfile = SocialManager.Network.GameJolt;
        //ShowProfile(PlayerManager.CurrentPlayer.CurrentProfile);
    }

    public void EarnWatch()
    {
        AdsManager.Instance.TryShowReward((s) =>
        {
        },
        (r) =>
        {
            EventManager.TriggerObjectEvent(E.RewardVideoGift, r);
            OnEnable();
        });
    }

    public void EarnInvite()
    {
        SocialManager.Instance.SendInvite(
            ConfigManager.invite_title,
            ConfigManager.invite_message,
            ConfigManager.invite_image,
            "Download",
            ConfigManager.invite_deep_link,(count)=> {
                EventManager.TriggerObjectEvent(E.InvitedGift, count);
                OnEnable();
            });
    }

    public void EarnShare()
    {
        MenuView.Instance.DarkLeft.IsActive.Value = true;
        UIManager.Instance.ChangeSubView(null);
    }
}
