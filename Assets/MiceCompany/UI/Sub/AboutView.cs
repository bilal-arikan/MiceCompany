﻿using Common.Data;
using Common.Managers;
using MarkLight;
using MarkLight.Views.UI;
using System;
using System.Collections.Generic;

namespace MiceCompany
{
    public class AboutView : UIView
    {
        public MCList MiceList;
        public MCList BuffList;
        public MCList PlacableList;

        public Region SummaryView;
        public _string ItemName;
        public _SpriteAsset ItemImage;
        public _string ItemInfo1;
        public _string ItemInfo2;


        void Start()
        {
            LoadItems();
        }

        void LoadItems()
        {
            //yield return new WaitForSecondsRealtime(2);
            MiceList.SetItems(PreLoadedAssets.Instance.MousePrefabs.ConvertAll(m => m.GetComponent<ShowableOnList>()));
            //yield return null;
            BuffList.SetItems(PreLoadedAssets.Instance.BuffPrefabs.ConvertAll(m => m.GetComponent<ShowableOnList>()));
            //yield return null;
            PlacableList.SetItems(PreLoadedAssets.Instance.PlacablePrefabs.ConvertAll(m => m.GetComponent<ShowableOnList>()));
        }

        protected virtual void ListItemSelected(ItemSelectionActionData d)
        {
            Item selected = (d.Item as ShowableOnList).GetComponent<Item>();
            ShowItemInfo(selected);
            SummaryView.IsVisible.Value = true;
        }

        protected virtual void ListItemDeselected(ItemSelectionActionData d)
        {
            Item deselected = (d.Item as ShowableOnList).GetComponent<Item>();
            if (deselected is PlayerMice)
            {
                BuffList.Items.SelectedIndex = -1;
                PlacableList.Items.SelectedIndex = -1;
                BuffList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);
                PlacableList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);
            }
            if (deselected is Buff)
            {
                MiceList.Items.SelectedIndex = -1;
                PlacableList.Items.SelectedIndex = -1;
                MiceList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);
                PlacableList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);
            }
            if (deselected is Placable)
            {
                BuffList.Items.SelectedIndex = -1;
                MiceList.Items.SelectedIndex = -1;
                BuffList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);
                MiceList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);
            }

            if (MiceList.Items.SelectedItem == null && BuffList.Items.SelectedItem == null && PlacableList.Items.SelectedItem == null)
                SummaryView.IsVisible.Value = false;
        }

        public void ShowItemInfo(Item b)
        {
            ItemName.Value = b.Name;
            ItemImage.Value = b.GetComponent<ShowableOnList>().ListViewSprite;

            if (b is Buff)
            {
                if ((b as Buff).effectTime == Buff.EffectTime.InPerion)
                    ItemInfo1.Value = (b as Buff).Time + "  Seconds\n" + (b as Buff).AreaSize;
                else if ((b as Buff).effectTime == Buff.EffectTime.InMoment)
                    ItemInfo1.Value = "Just Moment\n" + (b as Buff).AreaSize;
                else if ((b as Buff).effectTime == Buff.EffectTime.Forever)
                    ItemInfo1.Value = "Forever\n" + (b as Buff).AreaSize;

                ItemInfo2.Value = b.Summary;
            }
            else if (b is PlayerMice)
            {
                PlayerMice m = (b as PlayerMice);
                ItemInfo1.Value = m.Health + " Health\n" + m.MoveSpeed + " Speed\n" + m.RotateSpeed + " Rotation";
                ItemInfo2.Value = m.VisionLength + " Vision\n" + m.SmellingPower + " Smelling";
            }
            else
            {
                ItemInfo1.Value = "";
                ItemInfo2.Value = "";
            }
        }

        public void BackToGame()
        {
            UIManager.Instance.ChangeSubView(null);
        }

    }


}
