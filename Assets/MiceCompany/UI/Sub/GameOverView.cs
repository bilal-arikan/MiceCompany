﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Common.Managers;
using Common.Tools;
using MarkLight;
using MarkLight.Views.UI;
using Common.Data;
using System.Linq;

namespace MiceCompany
{
    public class GameOverView : SingletonView<GameOverView>
    {
        public _string GameResult;
        public Image Star1;
        public Image Star2;
        public Image Star3;
        public _string YourScore;
        public _string OldScore;
        public Button NextLevelButton;
        public Button UnlockButton;
        public Label UnlockTimerLabel;

        public Label XpRevenueLabel;
        public Label MoneyRevenueLabel;
        public Label SMoneyRevenueLabel;
        public Image MiceArrivedAR;
        public Image MiceDeathAR;
        public Image MiceUsedAR;
        public Image CompleteTimeAR;
        public Label MiceArrivedLabel;
        public Label MiceDeathLabel;
        public Label MiceUsedLabel;
        public Label CompleteTimeLabel;

        public ViewSwitcher FriendScoresViewSwitcher;
        public FriendScoreSummary FriendScoreSummaryTemplate;
        //public List FriendScoresList;
        public ObservableList<FriendPassedSubCh> FriendScoresList = new ObservableList<FriendPassedSubCh>();

        public Button ScrGooglePlayButton;
        public Button ScrGameJoltButton;


        private void OnEnable()
        {
            // Set ScoreTable Buttons
            ScrGooglePlayButton.IsActive.Value = SocialManager.Instance.IsSignedInGooglePlay;
            ScrGameJoltButton.IsActive.Value = SocialManager.Instance.IsSignedInGameJolt;

            /*if (SocialManager.Instance.IsSignedInFacebook)
                FriendScoresViewSwitcher.SwitchTo(0);
            else
                FriendScoresViewSwitcher.SwitchTo(1);*/
        }

        public void InitializeFinishResult(PassedSubCh oldS, GamePlaySituation newS)
        {
            // Eski Skor bilgileri girilir
            OldScore.Value = oldS.Score + "";
            Star1.SetState("Disabled");
            Star2.SetState("Disabled");
            Star3.SetState("Disabled");

            // Yeni skor bilgileri animasyonlu oynatılmalı !!!
            if (newS.Passed && newS.Star > 0)
            {
                if(newS.Star == 0)
                {
                }
                else if (newS.Star == 1)
                {
                    Star1.SetState("Default");
                }
                else if (newS.Star == 2)
                {
                    Star1.SetState("Default");
                    Star2.SetState("Default");
                }
                else if (newS.Star == 3)
                {
                    Star1.SetState("Default");
                    Star2.SetState("Default");
                    Star3.SetState("Default");
                }
            }
            else
            {
                Star1.SetState("Disabled");
                Star2.SetState("Disabled");
                Star3.SetState("Disabled");
            }

            // Son bölümse 
            if(LevelLoadManager.CurrentSubCh.NextSubCh() == null )
            {
                NextLevelButton.IsActive.Value = false;
                UnlockButton.IsActive.Value = false;
            }
            // bölüm geçildiyse
            else if(oldS.Star >= 1 || newS.Passed)
            {
                // son bölüm deil ve sıradaki bölümün kilidi zaten açılmışsa
                if (PlayerManager.CurrentPlayer.UnlockedSubChapters.Contains(LevelLoadManager.CurrentSubCh.NextSubCh().IndexPoint))
                {
                    NextLevelButton.IsActive.Value = true;
                    UnlockButton.IsActive.Value = false;
                }
                // son bölüm deil ve sıradaki bölümün kilidi Açılmamışsa
                else
                {
                    NextLevelButton.IsActive.Value = false;
                    UnlockButton.IsActive.Value = true;
                }
            }
            else
            {
                NextLevelButton.IsActive.Value = false;
                UnlockButton.IsActive.Value = false;
            }

            YourScore.Value = newS.Score + "";
        }

        public void InitializeStatsRegion(PassedSubCh oldS, GamePlaySituation newS)
        {
            if (newS.Passed)
            {
                XpRevenueLabel.SetState("Green");
                XpRevenueLabel.Text.Value = "+ " + newS.InGameRevenueXp + " Xp";
                MoneyRevenueLabel.SetState("Green");
                MoneyRevenueLabel.Text.Value = "+ " + newS.InGameRevenueMoney;
                SMoneyRevenueLabel.SetState("Green");
                SMoneyRevenueLabel.Text.Value = "+ " + newS.InGameRevenueSpecMoney;
            }
            else
            {
                XpRevenueLabel.SetState("Red");
                XpRevenueLabel.Text.Value = "+ 0 Xp";
                MoneyRevenueLabel.SetState("Red");
                MoneyRevenueLabel.Text.Value = "+ 0";
                SMoneyRevenueLabel.SetState("Red");
                SMoneyRevenueLabel.Text.Value = "+ 0";
            }

            // Hedefe Ulaşan Fareler
            if (newS.PassedArriveRequirement)
            {
                MiceArrivedAR.SetState("Accept");
                MiceArrivedLabel.SetState("Green");
                MiceArrivedLabel.Text.Value = newS.ArrivedMices + "";
            }
            else
            {
                MiceArrivedAR.SetState("Reject");
                MiceArrivedLabel.SetState("Red");
                MiceArrivedLabel.Text.Value = newS.ArrivedMices + "";
            }

            // Ölen fareler
            if (newS.PassedNotDeathRequirement)
            {
                MiceDeathAR.SetState("Accept");
                MiceDeathLabel.SetState("Green");
                MiceDeathLabel.Text.Value = newS.DeathMices + "";
            }
            else
            {
                MiceDeathAR.SetState("Reject");
                MiceDeathLabel.SetState("Red");
                MiceDeathLabel.Text.Value = newS.DeathMices + "";
            }

            // Başka bir istatistik koyulmalı
            // Kullanılan Fareler
            /*if (GameManager.CurentlyLoadedSubCh.ToWinNotDeath < 0 || GameManager.Instance.DeathMices.Count <= GameManager.CurentlyLoadedSubCh.ToWinNotDeath)
            {
                MiceUsedAR.SetState("Accept");
                MiceUsedLabel.SetState("Default");
                MiceUsedLabel.Text.Value = GameManager.Instance.UsedMices.Count + "";
            }
            else
            {
                MiceUsedAR.SetState("Accept");
                MiceUsedLabel.SetState("Default");
                MiceUsedLabel.Text.Value = GameManager.Instance.UsedMices.Count + "";
            }*/

            // Tamamlama Süresi
            if (newS.PassedTimeRequirement)
            {
                CompleteTimeAR.SetState("Accept");
                CompleteTimeLabel.SetState("Default");
                CompleteTimeLabel.Text.Value = Timer.TimeToString( GameManager.MainTimer.CurrentTime) + "";
            }
            else
            {
                CompleteTimeAR.SetState("Reject");
                CompleteTimeLabel.SetState("Red");
                CompleteTimeLabel.Text.Value = Timer.TimeToString(GameManager.MainTimer.CurrentTime) + "";
            }
        }

        public void InitializeFriendsRegion(List<FriendPassedSubCh> FriendScores)
        {

            foreach (var i in FriendScores)
            {
                if (i.FriendProfile.Photo == null)
                    Common.Core.DownloadPhoto(i.FriendProfile.PhotoUrl, (s) =>
                    {
                        i.FriendProfile.Photo = s;
                        FriendScoresList.Add(i);
                        FriendScoresList.Sort( (f1,f2) => f1.Rank.CompareTo(f2.Rank));
                    });
                else
                {
                    FriendScoresList.Add(i);
                    FriendScoresList.Sort((f1, f2) => f1.Rank.CompareTo(f2.Rank));
                }
            }
        }

        public void PlayNextCh()
        {
            FriendScoresList.Clear();
            UnlockTimerLabel.IsActive.Value = false;

            if(LevelLoadManager.CurrentSubCh.NextSubCh() != null)
                LevelLoadManager.Instance.ChangeChapter(LevelLoadManager.CurrentSubCh.NextSubCh().IndexPoint);
        }

        public void UnlockNextCh()
        {
            if (LevelLoadManager.CurrentSubCh.NextSubCh() != null)
            {
                LevelLoadManager.Instance.TryUnlockSubChapter(
                    LevelLoadManager.CurrentSubCh.NextSubCh(), 
                    (s) =>
                    {
                        NextLevelButton.IsActive.Value = s;
                        UnlockButton.IsActive.Value = !s;
                        UnlockTimerLabel.IsActive.Value = false;
                    },
                    (timerChanged) =>
                    {
                        if (!UnlockTimerLabel.IsActive)
                            UnlockTimerLabel.Activate();
                        UnlockTimerLabel.Text.Value = ((int)timerChanged).ToString("00");
                    }
                    );
            }
            else
            {
                Debug.LogError("Kilidi Açılacbilecek başka bölüm yok !!!");
            }
        }

        public void ReplayCh()
        {
            FriendScoresList.Clear();
            UnlockTimerLabel.IsActive.Value = false;
            LevelLoadManager.Instance.CancelUnlockSubChapter();
            LevelLoadManager.Instance.ChangeChapter(LevelLoadManager.CurrentIndex);
        }

        public void ReturnToMainMenu()
        {
            FriendScoresList.Clear();
            UnlockTimerLabel.IsActive.Value = false;
            LevelLoadManager.Instance.CancelUnlockSubChapter();
            LevelLoadManager.Instance.ChangeChapter(new Point());

            AdsManager.Instance.TryShowInterstitial();
        }

        public bool IsStar1(int starCount){return starCount >= 1;}
        public bool IsStar2(int starCount){return starCount >= 2;}
        public bool IsStar3(int starCount){return starCount >= 3;}

        public void ScrGooglePlayButtonClick()
        {
            SocialManager.Instance.ShowLeaderboard( SocialManager.Network.GooglePlay);
        }
        public void ScrGameJoltButtonClick()
        {
            SocialManager.Instance.ShowLeaderboard( SocialManager.Network.GameJolt);
        }
    }
}
