﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MarkLight.Views.UI;
using MarkLight;

namespace MiceCompany
{
    public class MCListItem : ListItem
    {
        [ChangeHandler("IsNecessaryChanged")]
        public _bool IsNecessary;
        public _int ItemCount;

        public void IsNecessaryChanged()
        {
            if (IsNecessary)
            {
                SetState("Necessary");
                IsDisabled.DirectValue = true;
            }
            else
            {
                SetState(DefaultItemStyle);
                IsDisabled.DirectValue = false;
            }
        }

        /// <summary>
        /// Called when mouse is clicked.
        /// </summary>
        public override void ListItemMouseClick()
        {
            if (State == "Necessary")
                return;

            base.ListItemMouseClick();
        }

        /// <summary>
        /// Called when mouse enters.
        /// </summary>
        public override void ListItemMouseEnter()
        {
            if (State == "Necessary")
                return;

            base.ListItemMouseEnter();
        }

        /// <summary>
        /// Called when mouse exits.
        /// </summary>
        public override void ListItemMouseExit()
        {
            if (State == "Necessary")
                return;

            base.ListItemMouseExit();
        }

        /// <summary>
        /// Called when mouse down.
        /// </summary>
        public override void ListItemMouseDown()
        {
            if (State == "Necessary")
                return;

            base.ListItemMouseDown();
        }

        /// <summary>
        /// Called when mouse up.
        /// </summary>
        public override void ListItemMouseUp()
        {
            if (State == "Necessary")
                return;

            base.ListItemMouseUp();
        }
    }
}
