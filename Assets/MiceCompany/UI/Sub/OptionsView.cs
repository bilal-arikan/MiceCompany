﻿using MarkLight;
using MarkLight.Views.UI;
using System.Collections.Generic;
using UnityEngine;
using Common.Managers;

public class OptionsView : UIView
{
    public Slider SoundSlider;
    public Slider MusicSlider;

    public RadioButton Q1;
    public RadioButton Q2;
    public RadioButton Q3;

    public _string BugText;
    public _string Version;

    public Group GroupLogOut;
    public Group LogOutFacebook;
    public Group LogOutGoogle;
    public Group LogOutGameJolt;


    void OnEnable()
    {
        SoundSlider.Value.Value = SoundManager.Instance.SfxVolume;
        MusicSlider.Value.Value = SoundManager.Instance.MusicVolume;

        GroupLogOut.IsActive.Value = GameManager.Instance.Status == GameManager.GameStatus.MainMenu;
        LogOutFacebook.IsActive.Value = SocialManager.Instance.IsSignedInFacebook;
        LogOutGoogle.IsActive.Value = SocialManager.Instance.IsSignedInGooglePlay;
        LogOutGameJolt.IsActive.Value = SocialManager.Instance.IsSignedInGameJolt;

        BugText.Value = PlayerManager.CurrentPlayer != null ? PlayerManager.CurrentPlayer.ID : "NoUser";

        AdsManager.Instance.TryShowBanner();
    }

    void OnDisable()
    {
        AdsManager.Instance.HideBanner();
    }


    public void SoundValueChanged()
    {
        SoundManager.Instance.SfxVolume = SoundSlider.Value.Value;
    }

    public void MusicValueChanged()
    {
        SoundManager.Instance.MusicVolume = MusicSlider.Value.Value;
    }

    public void QualityClick()
    {
        if (Q1.IsChecked.Value)
        {
            ConfigManager.Instance.SetGraphicQuality(0);
        }
        else if (Q2.IsChecked.Value)
        {
            ConfigManager.Instance.SetGraphicQuality(1);
        }
        else if (Q3.IsChecked.Value)
        {
            ConfigManager.Instance.SetGraphicQuality(2);
        }
        else
            Debug.LogError("QualitySettings: RadioButton error");
    }

    public void ReportBug()
    { 
        ConfigManager.Instance.ReportCrash(BugText.Value,null);
    }

    public void ResetGame()
    {
        UIManager.Instance.ShowAskToUser("Are you sure\nReset Game ?", (s) => {
            if (s)
            {
                DatabaseManager.Local.Reset();
                PlayerManager.Instance.LoadPlayerProfile();
                PlayerManager.Instance.LoadPlayerData();
            }
        });
    }

    public void ShowConsole()
    {
        AdsManager.Instance.HideBanner();
        Common.CConsole.Show();
    }

    public void BackToGame()
    {
        UIManager.Instance.ChangeSubView(null);
    }

    public void Facebook()
    {
        SocialManager.Instance.Disconnect(SocialManager.Network.Facebook);
        LogOutFacebook.IsActive.Value = false;
    }
    public void PlayServices()
    {
        SocialManager.Instance.Disconnect(SocialManager.Network.GooglePlay);
        LogOutGoogle.IsActive.Value = SocialManager.Instance.IsSignedInGooglePlay;
    }
    public void GameJolt()
    {
        SocialManager.Instance.Disconnect(SocialManager.Network.GameJolt);
        LogOutGameJolt.IsActive.Value = SocialManager.Instance.IsSignedInGameJolt;
    }
}
