﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Managers;
using MarkLight.Views.UI;
using MarkLight.Views;

public class LangSelection: UIView
{
    void OnEnable()
    {
        AdsManager.Instance.TryShowBanner();
    }

    private void OnDisable()
    {
        AdsManager.Instance.HideBanner();
    }

    void EN()
    {
        LanguageManager.ChangeLanguage("en");
    }
    void TR()
    {
        LanguageManager.ChangeLanguage("tr");
    }
    void JA()
    {
        LanguageManager.ChangeLanguage("ja");
    }
    void ZH()
    {
        LanguageManager.ChangeLanguage("zh");
    }
    void KO()
    {
        LanguageManager.ChangeLanguage("ko");
    }
    void DE()
    {
        LanguageManager.ChangeLanguage("de");
    }
    void HI()
    {
        LanguageManager.ChangeLanguage("hi");
    }
    void RU()
    {
        LanguageManager.ChangeLanguage("ru");
    }
    void ES()
    {
        LanguageManager.ChangeLanguage("es");
    }
    void AR()
    {
        LanguageManager.ChangeLanguage("ar");
    }

    public void BackToGame()
    {
        UIManager.Instance.ChangeSubView(null);
    }
}
