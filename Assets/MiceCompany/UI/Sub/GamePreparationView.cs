﻿using System;
using UnityEngine;
using System.Collections.Generic;
using Common.Managers;
using Common.Data;
using MarkLight;
using MarkLight.Views.UI;
using MiceCompany;

public class GamePreparationView : SingletonView<GamePreparationView>
{
    public Button ButtonReady;
    public MCList NecessaryMiceList;
    public MCList NecessaryPlacableList;

    public Image ToWinArriveImage;
    public Image ToWinNotDeathImage;
    public Image ToWinSecondsImage;
    public _string ToWinArrive;
    public _string ToWinNotDeath;
    public _string ToWinSeconds;

    public Image ProfileImage;
    public _string PlayerLevel;
    public _string ProfileInfo1;
    public _string ProfileInfo2;
    public _string ProfileInfo3;
    public _string Star1Score;
    public _string Star2Score;
    public _string Star3Score;


    public _string RemainingMiceCount;
    public _string RemainingBuffCount;

    public SubChapter SubChInfo;


    public void OnEnable()
    {
        SubChInfo = LevelLoadManager.CurrentSubCh;

        ToWinArrive.Value = SubChInfo.ToWinArrive + "";
        ToWinArriveImage.IsActive.Value = SubChInfo.ToWinArrive > 0;
        ToWinNotDeath.Value = SubChInfo.ToWinNotDeath + "";
        ToWinNotDeathImage.IsActive.Value = SubChInfo.ToWinNotDeath > 0;
        ToWinSeconds.Value = SubChInfo.ToWinSeconds + "";
        ToWinSecondsImage.IsActive.Value = SubChInfo.ToWinSeconds > 0;

        ProfileImage.Sprite.Value = new SpriteAsset(new UnityAsset("ProfileImage", PlayerManager.CurrentProfile.Photo));
        PlayerLevel.Value = PlayerManager.CurrentPlayer.Level + "";
        ProfileInfo1.Value = PlayerManager.CurrentProfile.DisplayName;
        ProfileInfo2.Value = PlayerManager.CurrentPlayer.Money.ToString();
        Instance.ProfileInfo3.Value = PlayerManager.CurrentPlayer.SpecialMoney.ToString();

        Star1Score.Value = SubChInfo.StarScore[1].ToString();
        Star2Score.Value = SubChInfo.StarScore[2].ToString();
        Star3Score.Value = SubChInfo.StarScore[3].ToString();

        // Zorunlu Verilen Fareler
        NecessaryMiceList.SetItems(SubChInfo.GivenMices.ConvertAll(m => PreLoadedAssets.Instance.MousePrefabs[m].GetComponent<ShowableOnList>()));
        // Zorunlu Verilen Eşyalar
        NecessaryPlacableList.SetItems(SubChInfo.GivenPlacables.ConvertAll(m => PreLoadedAssets.Instance.PlacablePrefabs[m].GetComponent<ShowableOnList>()));


        NecessaryMiceList.PresentedListItems.ForEach(li => ((MCListItem)li).IsDisabled.Value = true);
        NecessaryPlacableList.PresentedListItems.ForEach(li => ((MCListItem)li).IsDisabled.Value = true);

        //Debug.Log("PREPERATION Set ");
    }

    public void ReadyToPlay()
    {
        GameView.Instance.InitializeSelectedItems(
            // şimdilik sahip olduğumuz fare yok
            new List<PlayerMice>(),
            // Yasaklı olmayan sahip olduklarımızı ekle
                SubChInfo.DisallowedBuffs.Contains(-1) ?
                // -1 varsa hiç birini ekleme
                new List<Buff>() :
                // yoksa izin verilenleri ekle
                PlayerManager.CurrentPlayer.OwnedBuffs.FindAll(bIndex => !SubChInfo.DisallowedBuffs.Contains(bIndex)).ConvertAll(b => PreLoadedAssets.Instance.BuffPrefabs[b]),
            // şimdilik sahip olduğumuz placable yok
            new List<Placable>(),
            // Bölüm başında verilenleri ekle
            SubChInfo.GivenMices.ConvertAll(i => (PlayerMice)PreLoadedAssets.Instance.MousePrefabs[i]),
            // Bölüm başında verilenleri ekle
            SubChInfo.GivenBuffs.ConvertAll(i => PreLoadedAssets.Instance.BuffPrefabs[i]),
            // Bölüm başında verilenleri 
            SubChInfo.GivenPlacables.ConvertAll(i => PreLoadedAssets.Instance.PlacablePrefabs[i])
        );

        UIManager.Instance.ChangeSubView(null);
    }

    public void ReturnToMainMenu()
    {
        UIManager.Instance.ShowAskToUser("Are you sure\n Return to the MainMenu ?", (s) => {
            if (s)
            {
                LevelLoadManager.Instance.ChangeChapter(new Point());
            }
        });
    }
}
