﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MarkLight;
using MarkLight.Views.UI;
using Common.Managers;

public class IAPView : UIView
{
    public void NoAds()
    {
        IAPManager.Instance.BuyProductID(IAP.NonCons.NoAds);
    }

    public void NoAds2()
    {
        IAPManager.Instance.BuyProductID(IAP.NonCons.NoAdsShort);
    }

    public void GoldFifty()
    {
        IAPManager.Instance.BuyProductID(IAP.Cons.Gold50);
    }

    public void GoldTwoHundred()
    {
        IAPManager.Instance.BuyProductID(IAP.Cons.Gold200);
    }

    public void BackToGame()
    {
        UIManager.Instance.ChangeSubView(null);

    }
}
