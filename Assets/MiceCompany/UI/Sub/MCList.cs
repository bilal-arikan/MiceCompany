﻿using System;
using System.Collections.Generic;
using MarkLight.Views.UI;
using MarkLight;
using UnityEngine;

namespace MiceCompany
{
    public class MCList : UIView
    {
        [NonSerialized]
        public ObservableList<ShowableOnList> Items = new ObservableList<ShowableOnList>();
        public List List;
        public List<MCListItem> SelectedListItems
        {
            get
            {
                return PresentedListItems.FindAll(li => li.IsSelected);
            }
        }
        public List<MCListItem> PresentedListItems
        {
            get
            {
                return List.PresentedListItems.ConvertAll(li => li as MCListItem);
            }
        }

        Dictionary<ShowableOnList, int> ItemCounts = new Dictionary<ShowableOnList, int>();

        private void OnDisable()
        {
            
        }

        public void Clear()
        {
            ItemCounts.Clear();
            Items.Clear();
            List.Clear();
        }

        protected virtual void ListItemSelected(ItemSelectionActionData d)
        {
        }

        protected virtual void ListItemDeselected(ItemSelectionActionData d)
        {
        }

        public MCListItem GetListItem(ShowableOnList item)
        {
            return PresentedListItems.Find(k => k.Item.Value == (object)item);
        }


        public void SetItems(List<ShowableOnList> items)
        {
            Items.Clear();
            ItemCounts.Clear();

            foreach (ShowableOnList i in items)
            {
                AddListViewItem(i);
            }
            return;
            // ilk önce itemler Dictionarye kaydedilir
            /*foreach(Item i in items)
            {
                if(ItemCounts.ContainsKey(i))
                {
                    ItemCounts[i] = ItemCounts[i] + 1;
                }
                else
                {
                    ItemCounts.Add(i, 1);
                }
            }

            //Dictionarye göre Liste yerleştirilir
            foreach ( var i in ItemCounts)
            {
                Debug.Log("Set: " + i.Key.name + ":" + i.Value);

                Items.Add(i.Key);
                GetListItem(i.Key).ItemCount.Value = i.Value;
            }*/
            
        }


        /// <summary>
        /// Still there are Items
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public void AddListViewItem(ShowableOnList item)
        {

            if (ItemCounts.ContainsKey(item))
            {
                ItemCounts[item] = ItemCounts[item] + 1;
                GetListItem(item).ItemCount.Value = ItemCounts[item];
                //Debug.Log("Add: " + item.name + ":" + ItemCounts[item]);
            }
            else
            {
                ItemCounts.Add(item, 1);
                Items.Add(item);
                GetListItem(item).ItemCount.Value = 1;
                //Debug.Log("Add: " + item.name + ":" + ItemCounts[item]);
            }

        }

        /// <summary>
        /// Still there are Items
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool RemoveListViewItem(ShowableOnList item)
        {
            if (ItemCounts.ContainsKey(item))
            {
                if(ItemCounts[item] > 1)
                {
                    ItemCounts[item] = ItemCounts[item] - 1;
                    GetListItem(item).ItemCount.Value = ItemCounts[item];
                    //Debug.Log("Rmv: " + item.name + ":" + ItemCounts[item]);
                    return true;
                }
                else
                {
                    ItemCounts.Remove(item);
                    Items.Remove(item);
                    //Debug.Log("Rmv: " + item.name );
                }
            }
            Items.SelectedIndex = -1;
            return false;
        }

        /// <summary>
        /// Still there are Items
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public void AddListViewItemUnlimited(ShowableOnList item)
        {

            if (ItemCounts.ContainsKey(item))
            {
                ItemCounts[item] = int.MaxValue;
                GetListItem(item).ItemCount.Value = ItemCounts[item];
                //Debug.Log("Add: " + item.name + ":" + ItemCounts[item]);
            }
            else
            {
                ItemCounts.Add(item, int.MaxValue);
                Items.Add(item);
                GetListItem(item).ItemCount.Value = 1;
                //Debug.Log("Add: " + item.name + ":" + ItemCounts[item]);
            }

        }
    }
}
