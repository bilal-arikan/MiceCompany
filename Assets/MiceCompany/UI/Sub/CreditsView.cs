﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MarkLight;
using MarkLight.Views;
using MarkLight.Views.UI;
using Common.Managers;
using UnityEngine;
using MiceCompany;
using Common.Data;
using System.Collections;

public class CreditsView : UIView
{
    public _string Version;

    public void BackToGame()
    {
        UIManager.Instance.ChangeSubView(null);
    }


    void Start()
    {
        Version.Value = "v " + Application.version;
    }

    
}
