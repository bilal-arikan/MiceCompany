﻿using MarkLight;
using UnityEngine;
using MarkLight.Views.UI;
using Common.Managers;
using MiceCompany;

public class PauseView : UIView
{

    public void Options()
    {
        UIManager.Instance.ChangeSubView(typeof(OptionsView));
    }

    public void MainMenu()
    {
        LevelLoadManager.Instance.ChangeChapter(new Point());
    }
    
    
    public void BackToGame()
    {
        GameManager.Instance.UnPause();
        UIManager.Instance.ChangeSubView(null);
    }

    public void MusicOnOff()
    {
        SoundManager.Instance.MusicOn = !SoundManager.Instance.MusicOn;
    }
    public void SoundOnOff()
    {
        SoundManager.Instance.SfxOn = !SoundManager.Instance.SfxOn;
    }
}