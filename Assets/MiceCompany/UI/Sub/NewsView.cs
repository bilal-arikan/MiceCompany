﻿using System;
using System.Collections.Generic;
using Common.Managers;
using MarkLight;
using MarkLight.Views.UI;
using UnityEngine;

public class NewsView : SingletonView<NewsView>
{


    public Button GetNewVersionButton;
    public List NewsList;
    public ObservableList<ConfigManager.New> News = new ObservableList<ConfigManager.New>();
    public List ReleaseList;
    public ObservableList<string> Releases = new ObservableList<string>();

    private void Start()
    {
        //GetNewsAndReleaseNotes
        ConfigManager.Instance.GetNewsAndReleaseNotes(
        (news) =>
        {
            NewsView.Instance.News.Clear();
            foreach (var n in news)
                Common.Core.DownloadPhoto(n.ImageUrl, (image) =>
                {
                    NewsView.Instance.News.Insert(0, new ConfigManager.New()
                    {
                        Image = image,
                        ImageUrl = n.ImageUrl,
                        DeepUrl = n.DeepUrl
                    });
                });
        },
        (releases) =>
        {
            NewsView.Instance.Releases.Clear();
            foreach (var r in releases)
                NewsView.Instance.Releases.Insert(0, r);

        });

        GetNewVersionButton.IsDisabled.Value = ConfigManager.CurrentVersion >= ConfigManager.PlaystoreVersion;
    }

    private void OnEnable()
    {
        if(News.Count > 0)
            NewsList.ScrollTo(0, null, new ElementMargin(0));

        // Resmi yüklenmemiş haberlerin resimlerini tekrar yükle
        foreach(var n in News)
        {
            if(n.Image == null)
            {
                Debug.Log("New: Getting Again...");
                Common.Core.DownloadPhoto(n.ImageUrl, (image) =>
                {
                    Debug.Log("News Get :" + (image != null));
                    n.Image = image;
                });
            }
        }

        AdsManager.Instance.TryShowBanner();
    }

    private void OnDisable()
    {
        AdsManager.Instance.HideBanner();
    }

    public void ClickedToNews(ItemSelectionActionData d)
    {
        Application.OpenURL((d.Item as ConfigManager.New).DeepUrl);
        News.SelectedIndex = -1;
    }

    public void GetNewVersion()
    {
        SocialManager.Instance.OpenMarketPage();
    }

    public void BackToGame()
    {
        UIManager.Instance.ChangeSubView(null);
    }
}

