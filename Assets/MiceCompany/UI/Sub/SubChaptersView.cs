﻿using MiceCompany;
using System.Collections.Generic;
using UnityEngine;
using MarkLight;
using MarkLight.Views.UI;
using Common.Managers;

public class SubChaptersView : SingletonView<SubChaptersView>
{
    public _string LevelHeader;

    public UIView SubInfoPanel;
    public UIView UnlockPanel;
    /*public UIView FriendsInfoPanel;
    public Image Friend1Image;
    public Image Friend2Image;
    public Image Friend3Image;*/

    public Image ToWinArriveImage;
    public Image ToWinNotDeathImage;
    public Image ToWinSecondsImage;
    public _string ToWinArrive;
    public _string ToWinNotDeath;
    public _string ToWinSeconds;
    public Label UnlockTimerLabel;

    public UICanvas MainCanvas;

    public ViewSwitcher SubChaptersViewSwitcher;
    //public List<List<SubChSelectButton>> AllSubChapterViews = new List<List<SubChSelectButton>>() { null };

    public int ActiveChapter = 1;
    public int SelectedSubCh = 1;

    public void SubChapterSelected(int subch)
    {
        SelectedSubCh = subch;
        if (subch == -1)
        {


            SubInfoPanel.IsActive.Value = false;
            foreach (var sch in SubChaptersViewSwitcher.GetChildren<SubChView>(false))
            {
                foreach (var btn in sch.SubChapters)
                {
                    btn.CheckBox.IsChecked.Value = false;
                }
            }


        }
        else
        {
            var selectedSubCh = LevelLoadManager.Instance.AllChapters[ActiveChapter][subch];

            SubInfoPanel.IsActive.Value = true;
            LevelHeader.Value = subch + ". " + selectedSubCh.Header;

            ToWinArriveImage.IsActive.Value = selectedSubCh.ToWinArrive > 0;
            ToWinArrive.Value = selectedSubCh.ToWinArrive + "";
            ToWinNotDeathImage.IsActive.Value = selectedSubCh.ToWinNotDeath > -1;
            ToWinNotDeath.Value = selectedSubCh.ToWinNotDeath + "";
            ToWinSecondsImage.IsActive.Value = selectedSubCh.ToWinSeconds > 0;
            ToWinSeconds.Value = selectedSubCh.ToWinSeconds + "";

        }

        UnlockPanel.IsActive.Value = false;
        UnlockTimerLabel.IsActive.Value = false;
        LevelLoadManager.Instance.CancelUnlockSubChapter();
    }
    
    public void ShowUnlock(int subch)
    {
        SelectedSubCh = subch;
        SubInfoPanel.IsActive.Value = false;
        UnlockPanel.IsActive.Value = true;
        LevelHeader.Value = subch + ". " + LevelLoadManager.Instance.AllChapters[ActiveChapter][subch].Header;

    }

    public void ChangeChapter(int ch)
    {
        if (ch != -1)
        {
            SubChaptersViewSwitcher.SwitchTo(ch-1);
        }
        else
        {
            SubInfoPanel.IsActive.Value = false;
            SubChaptersViewSwitcher.SwitchTo(0);
        }
        ActiveChapter = ch;
    }

    public void StartClick()
    {
        if (ActiveChapter != -1 && SelectedSubCh != -1)
        {
            LevelLoadManager.Instance.ChangeChapter(new Point(ActiveChapter, SelectedSubCh));
        }
        else
        {
            Debug.LogError("Normalde Start butonunun gözükmemesi Lazımdı :S");
        }
    }

    public void UnlockClick()
    {
        if (ActiveChapter != -1 && SelectedSubCh != -1)
        {
            LevelLoadManager.Instance.TryUnlockSubChapter(LevelLoadManager.Instance.AllChapters[ActiveChapter][SelectedSubCh],(s)=>
            {
                if (s)
                {
                    SubChapterSelected(SelectedSubCh);
                }
            },
            (timerChanged) =>
            {
                if (!UnlockTimerLabel.IsActive)
                    UnlockTimerLabel.Activate();
                UnlockTimerLabel.Text.Value = ((int)timerChanged).ToString("00");
            });
        }
    }

    public void BackToGame()
    {

        SubChapterSelected(-1);
        GameManager.Instance.SetTimeScale( MainMenuController.Instance.GameSpeed );
        UIManager.Instance.ChangeSubView(typeof(ChaptersView));
    }
}
