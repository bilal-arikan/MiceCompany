﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MarkLight;
using MarkLight.Views;
using MarkLight.Views.UI;
using Common.Managers;
using MiceCompany;

public class ChaptersView : SingletonView<ChaptersView>
{
    public bool VisibleBackground = true;
    public ViewSwitcher ChaptersViewSwitcher;

    private void OnEnable()
    {
        SimpleGesture.On4AxisSwipeRight(LeftClick);
        SimpleGesture.On4AxisSwipeLeft(RightClick);
    }

    private void OnDisable()
    {
        SimpleGesture.Stop4AxisSwipeRight(LeftClick);
        SimpleGesture.Stop4AxisSwipeLeft(RightClick);
    }

    public void LeftClick()
    {
        ChaptersViewSwitcher.Previous(true, false);
        SubChaptersView.Instance.SubChaptersViewSwitcher.Previous(false, false);
        SubChaptersView.Instance.SubChapterSelected(-1);
    }

    public void RightClick()
    {
        ChaptersViewSwitcher.Next(true, false);
        SubChaptersView.Instance.SubChaptersViewSwitcher.Next(false, false);
        SubChaptersView.Instance.SubChapterSelected(-1);
    }

    public void BackToGame()
    {
        UIManager.Instance.ChangeSubView(null);
    }
}
