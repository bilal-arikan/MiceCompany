﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MiceCompany
{
    public class MiceTrapController : MonoBehaviour
    {
        //public bool TrapTriggered = false;
        public float ColliderEnableDelay = 0.5f;
        public float ColliderDisableDelay = 0.1f;
        Animator anim;
        Collider2D coll;

        // Use this for initialization
        void Start()
        {
            anim = GetComponent<Animator>();
            coll = GetComponent<Collider2D>();
        }

#if UNITY_EDITOR
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.K))
                TriggerTrap();
        }
#endif

        public void TriggerTrap()
        {
            anim.SetTrigger("IsActivated");
            StartCoroutine(TrapCollider());

        }

        IEnumerator TrapCollider()
        {
            yield return new WaitForSeconds(ColliderEnableDelay);
            coll.enabled = true;
            yield return new WaitForSeconds(ColliderDisableDelay);
            coll.enabled = false;
        }
    }
}
