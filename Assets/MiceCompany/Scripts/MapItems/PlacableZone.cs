﻿using Common.Data;
using Common.Managers;
using MiceCompany;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class PlacableZone : MonoBehaviour
{
    private void Start()
    {
        EventManager.StartListening(E.GameStarted, () => 
        {
            var rend = GetComponent<SpriteRenderer>();
            if (rend != null)
                rend.enabled = false;
        });
    }
}
