﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

//[ExecuteInEditMode]
public class ShowableOnList : MonoBehaviour
{
    public string TemplateId;
    SpriteRenderer rend
    {
        get
        {
            return GetComponent<SpriteRenderer>();
        }
    }

    public MarkLight.SpriteAsset ListViewSprite
    {
        get
        {
            return new MarkLight.SpriteAsset(rend.sprite.GetHashCode().ToString(), rend.sprite);
        }
        set
        {
            rend.sprite = value.Sprite;
        }
    }

    protected virtual void Awake()
    {
        if (!gameObject.IsCached())
            gameObject.CacheComponents();
    }
    protected virtual void OnDestroy()
    {
        gameObject.UnCacheComponents();
    }

    public string GetTemplateId()
    {
        return TemplateId;
    }
}
