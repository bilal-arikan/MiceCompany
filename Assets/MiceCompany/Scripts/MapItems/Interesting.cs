﻿using Common.Data;
using Common.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MiceCompany
{
    public class Interesting : MonoBehaviour
    {
        public bool MakesBlind = true;
        public float Radius
        {
            get
            {
                if (coll != null)
                    return coll.radius;
                else
                    return 0;
            }
        }
        public float Power = 1;
        CircleCollider2D coll;
        AudioSource audi;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="prefab"></param>
        /// <returns></returns>
        public static Interesting SpawnOnPointerPose(GameObject interestingPrefab)
        {
            var worldPos = InputManager.ScreenToWorld();

            var obj = Instantiate(
            interestingPrefab,
            new Vector3(worldPos.x, worldPos.y, 0),new Quaternion(),
            GameManager.Instance.BuffRoot);

            return obj.GetComponent<Interesting>();
        }

        private void Start()
        {
            coll = GetComponent<CircleCollider2D>();
            audi = GetComponent<AudioSource>();
            if (audi != null)
                audi.Play();
        }
        
        private void OnDrawGizmos()
        {
            if (Power > 0)
                Gizmos.color = Color.black;
            else
                Gizmos.color = Color.red;

            Gizmos.DrawWireSphere(transform.position, Radius * transform.lossyScale.x);
        }
    }
}
