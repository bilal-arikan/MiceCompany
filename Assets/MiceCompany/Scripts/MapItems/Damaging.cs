﻿using Common.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MiceCompany
{
    public class Damaging : MonoBehaviour 
    {

        public bool InstantKill = false;
        public int DamageAmount = 1;
        public float DamageForce = 5000f;
        public DeathStyle DamageStyle = DeathStyle.Standart;

        /// <summary>
        /// 0 = Once
        /// > 0  every that seconds
        /// </summary>
        public float ApplyEffectPeriod = 0;

        public List<Item> EffectedItems = new List<Item>();
        private Animator anim;
        AudioSource audi;

        void Start()
        {
            anim = GetComponent<Animator>();
            audi = GetComponent<AudioSource>();
        }


        protected virtual void OnCollisionEnter2D(Collision2D other)
        {
            if (DamageAmount == 0)
                return;

            Mice m = other.gameObject.GetCachedComponent<Mice>();
            if (m != null)
            {
                m.gameObject.GetCachedComponent<Rigidbody2D>().AddForce(
                    (m.transform.position - transform.position).normalized * DamageForce);

                if (InstantKill)
                {
                    m.AddToHealth(-1000, DamageStyle);
                }
                else if (ApplyEffectPeriod > 0)
                {
                    EffectedItems.Add(m);
                    StartCoroutine(AddDamageCo(m));
                }
                else
                    m.AddToHealth(-DamageAmount, DamageStyle);
            }
        }

        protected virtual void OnTriggerEnter2D(Collider2D other)
        {
            if (DamageAmount == 0)
                return;

            Mice m = other.gameObject.GetCachedComponent<Mice>();
            if (m != null)
            {
                m.gameObject.GetCachedComponent<Rigidbody2D>().AddForce(
                    (m.transform.position - transform.position).normalized * DamageForce);

                if (InstantKill)
                {
                    m.AddToHealth(-1000, DamageStyle);
                }
                else if (ApplyEffectPeriod > 0)
                {
                    EffectedItems.Add(m);
                    StartCoroutine(AddDamageCo(m));
                }
                else
                    m.AddToHealth(-DamageAmount, DamageStyle);
            }
        }

        protected virtual void OnCollisionExit2D(Collision2D other)
        {
            if (DamageAmount == 0)
                return;

            Item item = other.gameObject.GetCachedComponent<Item>();
            if (item != null)
                EffectedItems.Remove(item);
        }

        protected virtual void OnTriggerExit2D(Collider2D other)
        {
            if (DamageAmount == 0)
                return;

            Item item = other.gameObject.GetCachedComponent<Item>();
            if (item != null)
                EffectedItems.Remove(item);
        }


        public void SetEnabled(bool isEnabled,bool disableCollider)
        {
            enabled = isEnabled;

            GetComponent<Collider2D>().enabled = !disableCollider;
        }

        public void ActivateDamaging()
        {
            if (!enabled)
            {
                enabled = true;
                GetComponent<Collider2D>().enabled = true;

                if (anim != null)
                {
                    anim.SetBool("IsActivated", true);
                }
                if (audi != null)
                {
                    audi.pitch = 1;
                    audi.Play();
                }
            }
        }

        public void DeactivateDamaging(bool disableCollider)
        {
            if (enabled)
            {
                enabled = false;

                GetComponent<Collider2D>().enabled = !disableCollider;

                if (anim != null)
                {
                    anim.SetBool("IsActivated", false);
                }
                if (audi != null)
                {
                    audi.pitch = -1;
                    audi.Play();
                }
            }
        }

        /// <summary>
        /// Damaging aktif olduğu müddetçe her Periodda hasar verilir
        /// </summary>
        /// <returns></returns>
        protected virtual IEnumerator AddDamageCo(Mice item)
        {
            do
            {
                item.AddToHealth(-DamageAmount, DamageStyle);

                yield return new WaitForSeconds(ApplyEffectPeriod);
            }
            //Damaging Aktifse, Item hayattaysa, Periyot > 0 sa ve Item etki alanından çıkmadıysa
            while ( isActiveAndEnabled && item != null && ApplyEffectPeriod > 0 && EffectedItems.Contains(item));
        }

    }
}
