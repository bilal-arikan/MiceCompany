﻿using Common.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace MiceCompany
{
    public class Selectable : MonoBehaviour
    {
        public bool IsSelectable = false;
        public bool IsRotatable = false;
        public bool IsRemovable = false;
        public bool IsMovable = true;
        public bool IsSelected
        {
            get
            {
                return SelectedObject == this;
            }
        }

        public static Selectable SelectedObject;

        public Item LinkedItem;

        private void Start()
        {
            LinkedItem = GetComponent<Item>();
        }

        /*protected virtual void OnMouseUpAsButton()
        {
            //float deltaDistance = Vector2.Distance(Input.touches.Length > 0 ? Input.GetTouch(0).position : (Vector2)Input.mousePosition, FirstClickPosition);
            //if(deltaDistance < 3f)

            SetIsSelected(!IsSelected);
        }*/

        public void SetIsSelected(bool isSelected)
        {
            //Debug.Log("SetIsSelected " + isSelected);
            if (GameManager.Instance.Status == GameManager.GameStatus.BeforeGameStart && IsSelectable && isSelected != IsSelected)
            {
                if (isSelected)
                {
                    if(LinkedItem != null)
                        LinkedItem.OnSelected();
                    SelectedObject = this;
                    SelectionCircle.Instance.AttachTo(this);
                }
                else
                {
                    if (LinkedItem != null)
                        LinkedItem.OnDeselected();
                    SelectedObject = null;
                    SelectionCircle.Instance.Detach();
                }
            }
        }

        protected virtual void OnDestroy()
        {
            if (IsSelected)
                SelectionCircle.Instance.Detach();
        }
    }
}
