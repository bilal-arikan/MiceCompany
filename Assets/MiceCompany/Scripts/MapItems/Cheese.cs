﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cheese : Item
{
    [SerializeField]
    int amount = 5;
    public int Amount
    {
        get
        {
            return amount;
        }
    }

    PolygonCollider2D coll;

    protected void Start()
    {
        coll = GetComponent<PolygonCollider2D>();
    }

    protected void OnCollisionEnter2D(Collision2D other)
    {
        var mouse = other.gameObject.GetCachedComponent<PlayerMice>();
        if (mouse != null && mouse.Team != MiceCompany.MiceTeam.DarkSide && mouse.Team != MiceCompany.MiceTeam.Zombie)
        {
            mouse.Arrive();
        }
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {
        var mouse = other.gameObject.GetCachedComponent<PlayerMice>();
        if (mouse != null && mouse.Team != MiceCompany.MiceTeam.DarkSide && mouse.Team != MiceCompany.MiceTeam.Zombie)
        {
            mouse.Arrive();
        }
    }

    public void Eat(int count)
    {

    }
}
