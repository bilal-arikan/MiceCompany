﻿using Common.Managers;
using Common.PathFinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MiceCompany
{
    public class Door : Placable
    {
        public bool IsOpen = false;
        public bool IsLocked = false;
        public Lootable Key;
        public AudioClip DoorOpenClip;
        public AudioClip DoorCloseClip;
        public AudioClip DoorLockedClip;
        PolygonCollider2D polyColl;
        Animator anim;
        AudioSource audi;

        List<Vector2[]> OpenedCollPoints = new List<Vector2[]>();
        List<Vector2[]> ClosedCollPoints = new List<Vector2[]>();


        protected void Start()
        {
            polyColl = GetComponent<PolygonCollider2D>();
            anim = GetComponent<Animator>();
            audi = GetComponent<AudioSource>();

            ClosedCollPoints.Clear();
            for (int i=0; i<polyColl.pathCount; i++)
            {
                ClosedCollPoints.Add(polyColl.GetPath(i));
            }

            OpenedCollPoints = new List<Vector2[]>()
            {
                {new Vector2[]{
                    new Vector2(ClosedCollPoints[0][2].x*3/4,ClosedCollPoints[0][0].y),
                    new Vector2(ClosedCollPoints[0][3].x*3/4,ClosedCollPoints[0][1].y),
                    new Vector2(ClosedCollPoints[0][2].x,ClosedCollPoints[0][2].y),
                    new Vector2(ClosedCollPoints[0][3].x,ClosedCollPoints[0][3].y)
                    }},
                {new Vector2[]{
                    new Vector2(ClosedCollPoints[1][0].x,ClosedCollPoints[1][0].y),
                    new Vector2(ClosedCollPoints[1][1].x,ClosedCollPoints[1][1].y),
                    new Vector2(ClosedCollPoints[1][0].x*3/4,ClosedCollPoints[1][2].y),
                    new Vector2(ClosedCollPoints[1][1].x*3/4,ClosedCollPoints[1][3].y)
                    }}
            };
        }

        public void Open(bool closeIfOpened = false)
        {
            if (closeIfOpened && IsOpen)
            {
                Close();
                return;
            }
            // Kapalıysa Aç
            else if (!IsOpen)
            {
                // Kilitliyse ve Anahtar yoksa kilitli sesini oynat ve bişey yapma
                if (IsLocked)
                {
                    if (audi != null)
                    {
                        audi.clip = DoorLockedClip;
                        audi.Play();
                    }
                    return;
                }

                for(int i = 0; i<OpenedCollPoints.Count; i++ )
                    polyColl.SetPath(i, OpenedCollPoints[i]);

                if (anim != null)
                {
                    anim.SetBool("IsActivated", true);
                }
                if (audi != null)
                {
                    audi.clip = DoorOpenClip;
                    audi.Play();
                }

                IsOpen = true;
                //Debug.Log("Open");

                PathFinder.Instance.CalculateAllPathsToTarget();
            }
        }

        public void Close(bool openIfClosed = false)
        {
            if (openIfClosed && !IsOpen)
            {
                Open();
                return;
            }
            // Açıksa kapat
            else if (IsOpen)
            {
                for (int i = 0; i < ClosedCollPoints.Count; i++)
                    polyColl.SetPath(i, ClosedCollPoints[i]);

                if (anim != null)
                {
                    anim.SetBool("IsActivated", false);
                }
                if (audi != null)
                {
                    audi.clip = DoorCloseClip;
                    audi.Play();
                }
                IsOpen = false;
                //Debug.Log("Close");

                PathFinder.Instance.CalculateAllPathsToTarget();
            }
        }
    }
}
