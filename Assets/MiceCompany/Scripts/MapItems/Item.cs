﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Managers;
using UnityEngine;
using MiceCompany;
using System.Reflection;
using Common.PathFinding;
using Common.Data;
using UnityEngine.Events;
using Common.Tools;
using Common;

//[RequireComponent(typeof(Collider2D), typeof(SpriteRenderer))]
public abstract class Item : MonoBehaviour
{
    [Header("Item Properties:")]
    public string Name;
    public string Summary;
    public int PreLoadedIndex
    {
        get
        {
            if (this is PlayerMice)
                return PreLoadedAssets.Instance.MousePrefabs.FindIndex(pref => pref.Name == Name);
            else if (this is Buff)
                return PreLoadedAssets.Instance.BuffPrefabs.FindIndex(pref => pref.Name == Name);
            else if (this is Placable)
                return PreLoadedAssets.Instance.PlacablePrefabs.FindIndex(pref => pref.Name == Name);
            else
                return -1;
        }
    }
    public bool IsGhost = false;
    public GameObject InstantiatePrefab;

    [SerializeField]
    public List<Type> PlacableZones = new List<Type>();

    /*protected virtual void OnCollisionEnter2D(Collision2D other)
    {
    }

    protected virtual void OnCollisionStay2D(Collision2D collider)
    {
    }

    protected virtual void OnCollisionExit2D(Collision2D other)
    {
    }

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
    }

    protected virtual void OnTriggerStay2D(Collider2D other)
    {
    }

    protected virtual void OnTriggerExit2D(Collider2D other)
    {
    }*/

    protected virtual void Awake()
    {
        if (!gameObject.IsCached())
            gameObject.CacheComponents();
    }

    public virtual void OnSelected()
    {
        //SetOutlined(true, Color.red, 15);
    }
    public virtual void OnDeselected()
    {
        //SetOutlined(false, Color.red, 15);
    }

    public void SetOutlined(bool outlined, Color c, float size = 4)
    {
        var rend = GetComponent<SpriteRenderer>();
        if (rend != null)
        {
            MaterialPropertyBlock mpb = new MaterialPropertyBlock();
            rend.GetPropertyBlock(mpb);
            mpb.SetColor("Tint", c);
            mpb.SetFloat("_Outline", outlined ? 1f : 0);
            mpb.SetColor("_OutlineColor", c);
            mpb.SetFloat("_OutlineSize", size);
            rend.SetPropertyBlock(mpb);
        }
    }

    public virtual void SetIsGhost(bool isGhost)
    {
        Collider2D coll = GetComponent<Collider2D>();
        if (coll != null)
            coll.enabled = !isGhost;

        SpriteRenderer rend = GetComponent<SpriteRenderer>();
        if (rend != null)
            rend.color = (isGhost ? new Color(rend.color.r, rend.color.g, rend.color.b,0.3f) : new Color(rend.color.r, rend.color.g, rend.color.b, 1f));

        foreach (var o in GetComponentsInChildren<Transform>())
            o.gameObject.SetActive(!isGhost);

        IsGhost = isGhost;
    }


    public virtual bool IsSuitableToPlace(Collider2D other)
    {
        if (PlacableZones.Count == 0)
            return true;

        if (other != null)
        {
            foreach (var t in PlacableZones)
            {
                if (other.gameObject.GetCachedComponent(t) != null)
                {
                    //Debug.Log("Suitable" + PlacableZones.Count);
                    return true;
                }
            }
        }
        return false;
    }

    public virtual void Remove()
    {
        Destroy(this.gameObject);
        //Debug.Log("Item Removed " + this.GetType());
        EventManager.TriggerObjectEvent(E.ItemRemoved, this);
    }


    protected virtual void OnDestroy()
    {
        gameObject.UnCacheComponents();
        EventManager.TriggerObjectEvent(E.ItemDestroyed, this);
    }
}
