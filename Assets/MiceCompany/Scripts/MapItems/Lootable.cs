﻿using Common.Data;
using Common.Managers;
using MarkLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace MiceCompany
{
    public class Lootable : MonoBehaviour
    {
        public string Name;
        public int RevenueMoney = 0;
        public int RevenueSpecMoney = 0;
        public int RevenueXp = 0;
        public AudioClip LootedAudio;

        public UnityEvent OnLooted;


        void Start()
        {
        }

        protected virtual void OnTriggerEnter2D(Collider2D other)
        {
            PlayerMice mouse = other.gameObject.GetCachedComponent<PlayerMice>();
            if (mouse != null)
            {
                Loot();
            }
        }

        public void Loot()
        {
            var prefab = PreLoadedAssets.Instance.LootablePrefabs.Find(l => l.Name == Name);
            GameManager.Instance.LootedItems.Add(prefab);
            OnLooted.Invoke();

            SoundManager.Instance.PlaySound(LootedAudio, Camera.main.transform.position);

            EventManager.TriggerObjectEvent(E.LootableLooted, prefab);

            Destroy(gameObject);
        }

    }
}
