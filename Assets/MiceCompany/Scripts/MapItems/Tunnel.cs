﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MiceCompany
{
    public class Tunnel : MonoBehaviour
    {
        public Tunnel ConnectedTunnel;
        private Animator anim;
        AudioSource audi;

        void Start()
        {
            anim = GetComponent<Animator>();
            audi = GetComponent<AudioSource>();
        }

        protected void OnTriggerEnter2D(Collider2D other)
        {
            PlayerMice mouse = other.gameObject.GetCachedComponent<PlayerMice>();
            if (mouse != null && ConnectedTunnel != null)
            {
                OnMiceEnter(mouse);
                mouse.transform.position = ConnectedTunnel.transform.position;
                ConnectedTunnel.OnMiceExit(mouse);
            }
        }

        protected void OnCollisionEnter2D(Collision2D other)
        {
            PlayerMice mouse = other.gameObject.GetCachedComponent<PlayerMice>();
            if (mouse != null && ConnectedTunnel != null)
            {
                OnMiceEnter(mouse);
                mouse.transform.position = ConnectedTunnel.transform.position;
                ConnectedTunnel.OnMiceExit(mouse);
            }
        }

        public virtual void OnMiceEnter(PlayerMice m)
        {
            if (audi != null)
                audi.Play();
        }

        public virtual void OnMiceExit(PlayerMice m)
        {

        }

        public void ActivateTunnel()
        {
            if (!enabled)
            {
                enabled = true;
                GetComponent<Collider2D>().enabled = true;

                if (anim != null)
                {
                    anim.SetBool("IsActivated", true);
                }
            }
        }

        public void DeactivateTunnel(bool disableCollider)
        {
            if (enabled)
            {
                enabled = false;

                GetComponent<Collider2D>().enabled = !disableCollider;

                if (anim != null)
                {
                    anim.SetBool("IsActivated", false);
                }
            }
        }
    }
}
