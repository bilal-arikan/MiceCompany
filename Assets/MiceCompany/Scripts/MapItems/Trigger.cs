﻿using Common.Managers;
using Common.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace MiceCompany
{
    public class Trigger : MonoBehaviour
    {
        [SerializeField]
        bool isActivated = false;
        public bool IsActivated
        {
            get
            {
                return isActivated;
            }
        }
        public bool DeactivateOnExitArea = false;
        public GameObject ActivatorObject;
        public AudioClip TriggerActivateClip;
        public AudioClip TriggerNotAllowedClip;
        public AudioClip TriggerDeactivateClip;
        Animator anim;
        Timer timer;
        public List<Lootable> NecessaryToTrigger = new List<Lootable>();
        public List<Type> ByTriggerable = new List<Type>() {typeof(PlayerMice)};

        public UnityEvent OnTriggerActivated;
        public UnityEvent OnTriggerNotAllowed;
        public UnityEvent OnTriggerDeactivated;

        void Start()
        {
            anim = GetComponent<Animator>();
            timer = GetComponent<Timer>();
        }

        public void ActivateTrigger(GameObject activator)
        {
            if (!IsActivated)
            {
                ActivatorObject = activator;

                OnTriggerActivated.Invoke();
                if (anim != null)
                    anim.SetBool("IsTriggered", true);

                SoundManager.Instance.PlaySound(TriggerActivateClip, transform.position);

                isActivated = true;

                if (timer != null && timer.TargetTime > 0)
                    timer.StartTimer();
            }
        }

        public void NotAllowed()
        {
            OnTriggerNotAllowed.Invoke();
            SoundManager.Instance.PlaySound(TriggerNotAllowedClip, transform.position);

        }

        public void DeactivateTrigger()
        {
            if (IsActivated)
            {
                ActivatorObject = null;

                OnTriggerDeactivated.Invoke();
                if (anim != null)
                    anim.SetBool("IsTriggered", false);

                SoundManager.Instance.PlaySound(TriggerNotAllowedClip, transform.position);

                isActivated = false;

                if (timer != null)
                    timer.ResetTimer();
            }
        }

        public void DestroyObject(GameObject go)
        {
            Destroy(go);
        }

#if UNITY_EDITOR
        private void OnMouseDown()
        {
            bool accept = true;
            // Tetiklemek için zorunlu bi nesne gerekiyosa
            if (NecessaryToTrigger.Count > 0)
            {
                // Bütün nesneler GameManager.Looted de mevcutsa
                accept = NecessaryToTrigger.TrueForAll(
                    nl => GameManager.Instance.LootedItems.Exists(l => l.Name == nl.Name));
            }

            if (accept)
                ActivateTrigger(null);
            else
                NotAllowed();
        }
#endif

        protected virtual void OnCollisionEnter2D(Collision2D other)
        {
            foreach (var bys in ByTriggerable)
            {
                if (other.gameObject.GetCachedComponent(bys) != null)
                {
                    bool accept = true;
                    // Tetiklemek için zorunlu bi nesne gerekiyosa
                    if (NecessaryToTrigger.Count > 0)
                    {
                        // Bütün nesneler GameManager.Looted de mevcutsa
                        accept = NecessaryToTrigger.TrueForAll(
                            nl => GameManager.Instance.LootedItems.Exists(l => l.Name == nl.Name));
                    }

                    if (accept)
                        ActivateTrigger(other.gameObject);
                    else
                        NotAllowed();
                }
            }
        }

        protected virtual void OnCollisionExit2D(Collision2D other)
        {
            if(DeactivateOnExitArea && other.gameObject == ActivatorObject)
            {
                DeactivateTrigger();
            }
        }

        protected virtual void OnTriggerEnter2D(Collider2D other)
        {
            foreach (var bys in ByTriggerable)
            {
                if (other.gameObject.GetCachedComponent(bys) != null)
                {
                    bool accept = true;
                    // Tetiklemek için zorunlu bi nesne gerekiyosa
                    if (NecessaryToTrigger.Count > 0)
                    {
                        // Bütün nesneler GameManager.Looted de mevcutsa
                        accept = NecessaryToTrigger.TrueForAll(
                            nl => GameManager.Instance.LootedItems.Exists(l => l.Name == nl.Name));
                    }

                    if (accept)
                        ActivateTrigger(other.gameObject);
                    else
                        NotAllowed();
                }
            }
        }

        protected virtual void OnTriggerExit2D(Collider2D other)
        {
            if (DeactivateOnExitArea && other.gameObject == ActivatorObject)
            {
                DeactivateTrigger();
            }
        }
    }
}
