﻿using Common.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MiceCompany
{
    public class AutoMiceSpawner : MonoBehaviour
    {
        public List<Transform> SpawnPoints = new List<Transform>();
        public List<Mice> MousePrefabs = new List<Mice>();
        public float MinWaitSpawnTime = 1;
        public float MaxWaitSpawnTime = 2;
        public int MaxMouseCount = 20;


        void Start()
        {
            StartCoroutine(MouseSpawnCo());
        }

        IEnumerator MouseSpawnCo()
        {
            yield return new WaitForEndOfFrame();
            while (true)
            {
                if (MousePrefabs.Count > 0 && SpawnPoints.Count > 0 && Resources.FindObjectsOfTypeAll(typeof(PlayerMice)).Length < MaxMouseCount)
                {
                    int mouseIndex = MousePrefabs.Count == 1 ? 0 : UnityEngine.Random.Range(0, MousePrefabs.Count);
                    int spawnIndex = SpawnPoints.Count == 1 ? 0 : UnityEngine.Random.Range(0, SpawnPoints.Count);
                    //Debug.Log("Fare Oluşturuluyor " + mouseIndex + ":"+spawnIndex + ":"+ MousePrefabs.Count+":"+ SpawnPoints.Count);

                    if (GameManager.Instance.MiceRoot != null)
                        Instantiate(MousePrefabs[mouseIndex], SpawnPoints[spawnIndex].position, SpawnPoints[spawnIndex].rotation, GameManager.Instance.MiceRoot);
                    else
                        Debug.LogWarning("AutoMiceSpawner: MiceRoot Null");
                }
                else
                {
                    //Debug.Log("Fare Oluşturulamadı !!! " + Resources.FindObjectsOfTypeAll(typeof(Mouse)).Length);
                }

                yield return new WaitForSeconds(UnityEngine.Random.Range(MinWaitSpawnTime, MaxWaitSpawnTime));
            }
        }
    }
}
