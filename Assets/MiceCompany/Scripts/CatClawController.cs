﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatClawController : MonoBehaviour {

    Animator anim;
    Collider2D coll;
    public float ColliderEnableDelay = 0.5f;
    public float ColliderDisableDelay = 0.3f;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        coll = GetComponent<Collider2D>();
	}

#if UNITY_EDITOR
    void Update () {
        if (Input.GetKeyDown(KeyCode.H))
            TriggerClaw();
        if (Input.GetKeyDown(KeyCode.J))
            TriggerEyeFlash();
    }
#endif

    public void TriggerClaw()
    {
        anim.SetTrigger("Claw");
        StartCoroutine(ClawCollider());
    }

    public void TriggerEyeFlash()
    {
        anim.SetTrigger("EyeFlash");
    }

    IEnumerator ClawCollider()
    {
        yield return new WaitForSeconds(ColliderEnableDelay);
        coll.enabled = true;
        yield return new WaitForSeconds(ColliderDisableDelay);
        coll.enabled = false;
    }
}
