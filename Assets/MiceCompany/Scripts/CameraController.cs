﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Tools;
using UnityEngine;
using Common.Managers;
using MiceCompany;

namespace MiceCompany
{
    public class CameraController : Singleton<CameraController>
    {
        public Camera GameCamera
        {
            get
            {
                return Camera.main;
            }
        }

        protected bool isClickedToObject = false;
        protected Vector2 FirstClickPosition;

        private float targetZoomvalue;
        private float defaultCamSize;
        public float CurrentZoomValue
        {
            get
            {
                if (GameCamera != null)
                    return GameCamera.orthographicSize / defaultCamSize;
                else
                    return 0;
            }
        }
        public float MaxZoom = 1.2f;
        public float MinZoom = 0.3f;

        public Vector2 GameBorderTopRight;
        public Vector2 GameBorderTopLeft
        {
            get
            {
                return new Vector2(GameBorderBottomLeft.x, GameBorderTopRight.y);
            }
        }
        public Vector2 GameBorderBottomLeft;
        public Vector2 GameBorderBottomRight
        {
            get
            {
                return new Vector2(GameBorderTopRight.x, GameBorderBottomLeft.y);
            }
        }
        public Vector4 BorderRightTopLeftBottom
        {
            get
            {
                return new Vector4(
                    GameBorderTopRight.x,
                    GameBorderTopLeft.y,
                    GameBorderBottomLeft.x,
                    GameBorderBottomRight.y);
            }
        }

        void OnEnable()
        {
            defaultCamSize = GameCamera.orthographicSize;
            CalculateCameraBorders();

            SimpleGesture.WhilePinching(this.ZoomUp);
            SimpleGesture.WhileStretching(this.ZoomDown);
            SimpleGesture.While1FingerPanning(this.SwipeMap);
        }

        private void OnDisable()
        {
            SimpleGesture.StopPinching(this.ZoomUp);
            SimpleGesture.StopStretching(this.ZoomDown);
            SimpleGesture.Stop1FingerPanning(this.SwipeMap);
        }

        public void Update()
        {
            if (Input.GetAxis("Mouse ScrollWheel") < 0)
            {
                ZoomMap(0.1f);
            }
            if (Input.GetAxis("Mouse ScrollWheel") > 0)
            {
                ZoomMap(-0.1f);
            }
        }


        public void CalculateCameraBorders()
        {
            Vector3 diff = new Vector3(1, 1, 0);
            Bounds camBounds = Camera.main.OrthographicBounds();
            GameBorderTopRight = (camBounds.center + camBounds.extents) + diff;
            GameBorderBottomLeft = (camBounds.center - camBounds.extents) - diff;

            float top = GameBorderTopRight.y;
            float bottom = GameBorderBottomLeft.y;
            float right = GameBorderTopRight.x;
            float left = GameBorderBottomLeft.x;

            foreach (Damaging i in FindObjectsOfType<Damaging>())
            {
                if (i == null) continue;

                if (top < i.transform.position.y)
                    top = i.transform.position.y;
                if (bottom > i.transform.position.y)
                    bottom = i.transform.position.y;
                if (right < i.transform.position.x)
                    right = i.transform.position.x;
                if (left > i.transform.position.x)
                    left = i.transform.position.x;
            }

            GameBorderTopRight = new Vector2(right, top);
            GameBorderBottomLeft = new Vector2(left, bottom);
        }

        public void SwipeMap(GestureInfoPan pan)
        {
            if ( !InputManager.IsClickedUI && !SelectionCircle.Instance.IsClicked)
            {
                GameCamera.transform.position -= (Vector3)pan.deltaDirection * 0.05f * CurrentZoomValue; /*new Vector3(
                        -InputManager.PointerDelta.x * 0.02f * CurrentZoomValue,
                        -InputManager.PointerDelta.y * 0.02f * CurrentZoomValue,
                        0);*/
                SetCameraInsideOfBorders();
            }
        }

        public void ZoomUp(GestureInfoZoom zoom)
        {
            ZoomMap(zoom.deltaDistance * 0.003f);
        }

        public void ZoomDown(GestureInfoZoom zoom)
        {
            ZoomMap(zoom.deltaDistance * -0.003f);
        }

        public void ZoomMap(float zoomValue)
        {
            if (!InputManager.IsClickedUI && !SelectionCircle.Instance.IsClicked)
            {
                GameCamera.orthographicSize = defaultCamSize * Mathf.Clamp(
                    CurrentZoomValue + zoomValue,
                    MinZoom,
                    MaxZoom);
                SetCameraInsideOfBorders();
            }
        }

        public void SetCameraInsideOfBorders()
        {
            GameCamera.transform.position = new Vector3(
                Mathf.Clamp(GameCamera.transform.position.x, BorderRightTopLeftBottom.z, BorderRightTopLeftBottom.x),
                Mathf.Clamp(GameCamera.transform.position.y, BorderRightTopLeftBottom.w, BorderRightTopLeftBottom.y),
                GameCamera.transform.position.z);
        }


        void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(GameBorderTopRight, GameBorderTopLeft);
            Gizmos.DrawLine(GameBorderTopRight, GameBorderBottomRight);
            Gizmos.DrawLine(GameBorderBottomLeft, GameBorderTopLeft);
            Gizmos.DrawLine(GameBorderBottomLeft, GameBorderBottomRight);
        }
    }

}
