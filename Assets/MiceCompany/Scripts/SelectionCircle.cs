﻿using System;
using System.Collections.Generic;
using Common.Managers;
using Common.Tools;
using MarkLight;
using UnityEngine;

namespace MiceCompany
{
    public class SelectionCircle : Singleton<SelectionCircle>
    {

        public Collider2D RotateCollider;
        public Collider2D RemoveCollider;
        public Collider2D MoveCollider;

        public Selectable AttachedObject;
        public Quaternion FirstRotation;
        public Vector2 FirstPosition;
        public Vector2 FirstClickPosition;
        public Vector2 PointerOffset;

        public bool IsClicked
        {
            get
            {
                return IsClickedToRotator || IsClickedToRemover || IsClickedToMover;
            }
        }
        public bool IsClickedToRotator;
        public bool IsClickedToRemover;
        public bool IsClickedToMover;

        protected override void Awake()
        {
            EventManager.StartListening(E.GameOnPreparing, () =>
            {
                Detach();
            });
            EventManager.StartListening(E.GameStarted, () =>
            {
                Detach();
            });

            base.Awake();
            FirstRotation = transform.rotation;
        }

        void Update()
        {

            if (AttachedObject != null)
            {
                transform.localRotation = new Quaternion();
                transform.transform.position = AttachedObject.transform.position;
            }
        }

        protected virtual void OnMouseDown()
        {
            FirstClickPosition = InputManager.PointerPosition;
            FirstPosition = AttachedObject.transform.position;
            FirstRotation = AttachedObject.transform.rotation;

            PointerOffset = (Vector2)AttachedObject.transform.position - InputManager.ScreenToWorld();

            RaycastHit2D hit = InputManager.ScreenRay2D();
            if (hit.collider != null)
            {
                IsClickedToRotator  = hit.collider == RotateCollider;
                IsClickedToMover    = hit.collider == MoveCollider;
                IsClickedToRemover  = hit.collider == RemoveCollider;
            }
        }


        void OnMouseDrag()
        {
            if (SelectionCircle.Instance.IsClickedToMover && AttachedObject.IsMovable)
            {
                //convert the screen mouse position to world point and adjust with offset
                var curPosition = InputManager.ScreenToWorld() + PointerOffset;

                //update the position of the object in the world
                AttachedObject.transform.position = curPosition;
            }
            if (SelectionCircle.Instance.IsClickedToRotator && AttachedObject.IsRotatable)
            {
                Vector2 deltaAngle = InputManager.PointerPosition - FirstClickPosition;
                //Debug.Log("newRot:" + deltaAngle + ":"+ FirstClickPosition + ":"+ (Input.touches.Length > 0 ? Input.GetTouch(0).position : (Vector2)Input.mousePosition)+ ":" + ((Input.touches.Length > 0 ? Input.GetTouch(0).position : (Vector2)Input.mousePosition)-FirstClickPosition));
                AttachedObject.transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(deltaAngle.y, deltaAngle.x) * Mathf.Rad2Deg);
            }
        }

        protected virtual void OnMouseUp()
        {
            if (IsClickedToMover && AttachedObject.LinkedItem != null)
            {
                bool moveSuccess = false;
                var hits = Physics2D.RaycastAll(AttachedObject.LinkedItem.transform.position, Vector2.zero);

                foreach (var h in hits)
                {
                    if (AttachedObject.LinkedItem is Placable && h.collider.gameObject.GetCachedComponent<PlacableZone>() != null)
                    {
                        moveSuccess = true;
                        EventManager.TriggerObjectEvent(E.ItemMoved, AttachedObject.LinkedItem);
                        goto End;
                    }
                    else if (AttachedObject.LinkedItem is PlayerMice && h.collider.gameObject.GetCachedComponent<SpawnZone>() != null)
                    {
                        moveSuccess = true;
                        EventManager.TriggerObjectEvent(E.ItemMoved, AttachedObject.LinkedItem);
                        goto End;
                    }
                }
                End:
                if(!moveSuccess)
                    AttachedObject.transform.position = FirstPosition;
            }

            if(IsClickedToRotator)
                EventManager.TriggerObjectEvent(E.ItemRotated, AttachedObject.LinkedItem);


            IsClickedToRotator = false;
            IsClickedToMover = false;
            IsClickedToRemover = false;
        }

        protected virtual void OnMouseUpAsButton()
        {

            float deltaDistance = Vector2.Distance(InputManager.PointerPosition, FirstClickPosition);
            if (deltaDistance < 3f && IsClickedToRemover)
            {
                if (AttachedObject.LinkedItem != null)
                    AttachedObject.LinkedItem.Remove();
                else
                    Destroy(AttachedObject.gameObject);
            }

        }

        public void AttachTo(Selectable obj)
        {
            AttachedObject = obj;
            transform.position = AttachedObject.transform.position;
            // Oyun başlamışsa Kaldırma, Çevirme ve Hark-eket ettirme yapılamaz
            if(GameManager.Instance.Status != GameManager.GameStatus.GameInProgress && GameManager.Instance.Status != GameManager.GameStatus.Paused)
            {
                RotateCollider.gameObject.SetActive(obj.IsRotatable);
                MoveCollider.gameObject.SetActive(obj.IsMovable);
                RemoveCollider.gameObject.SetActive(obj.IsRemovable);
            }
            else
            {
                RotateCollider.gameObject.SetActive(false);
                MoveCollider.gameObject.SetActive(false);
                RemoveCollider.gameObject.SetActive(false);
            }

            enabled = true;
            gameObject.SetActive(true);

            GameManager.Instance.SelectedItemView = null;
            GameView.Instance.BuffList.Items.SelectedIndex = -1;
            GameView.Instance.MouseList.Items.SelectedIndex = -1;
            GameView.Instance.PlacableList.Items.SelectedIndex = -1;
            GameView.Instance.BuffList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);
            GameView.Instance.MouseList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);
            GameView.Instance.PlacableList.PresentedListItems.ForEach(i => i.IsSelected.Value = false);
        }

        public void Detach()
        {
            //if (enabled)
            //{
            if (AttachedObject != null && AttachedObject.LinkedItem != null)
                AttachedObject.LinkedItem.OnDeselected();

            Selectable.SelectedObject = null; 
            AttachedObject = null;
                transform.parent = null;
                //transform.localPosition = new Vector3(0, 0, 0);
            gameObject.SetActive(false);

            IsClickedToRotator = false;
            IsClickedToMover = false;
            IsClickedToRemover = false;
            //}
        }

    }
}
