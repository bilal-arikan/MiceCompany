﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using Common.Managers;
using Common.Tools;

namespace MiceCompany
{
    [RequireComponent(typeof(CameraFollower))]
    public class MainMenuController : Singleton<MainMenuController>
    {
        public float GameSpeed = 1f;
        public CameraFollower CamFollower;
        public int ChangeFocusedMiceTime = 2;

        void Start()
        {
            CamFollower = GetComponent<CameraFollower>();
            GameManager.Instance.SetTimeScale(GameSpeed);
            StartCoroutine(ChangeFocusedMouse());
            StartCoroutine(UpdateLighting());
        }



        IEnumerator ChangeFocusedMouse()
        {
            yield return new WaitForSecondsRealtime(2);

            // MainMenüde ve Cameranın takip ettiği bu fare bu ise başka fare seç
            while (LevelLoadManager.Instance.IsMapChanging == false)
            {
                if(MainMenuController.Instance.CamFollower.Followed == null && GameManager.Instance.PlayerMicesInTheMap.Count>0)
                    MainMenuController.Instance.CamFollower.Followed =
                        GameManager.Instance.PlayerMicesInTheMap[GameManager.Instance.PlayerMicesInTheMap.Count - 1].GetComponent<Rigidbody2D>();

                yield return new WaitForSecondsRealtime(ChangeFocusedMiceTime);
            }
        }

        IEnumerator UpdateLighting()
        {
            yield return new WaitForSecondsRealtime(1);

            while (true)
            {
                if(GameManager.Instance.LightCamera == null)
                {
                    yield return new WaitForEndOfFrame();
                    continue;
                }

                float mod60 = Time.time % 60;

                bool lightsOn = mod60 > 5 && mod60 < 55; //Darkness
                float lumen = 0;

                if (mod60 > 5 && mod60 < 25)
                {
                    lumen = 1 - Mathf.Clamp01((mod60-5) / 20);
                }
                else if( mod60 >= 25 && mod60 <= 35)
                {
                    lumen = 0;
                }
                else if (mod60 > 35 && mod60 < 55)
                {
                    lumen = Mathf.Clamp01(((mod60 + 5) / 20) - 2);
                }
                else
                {
                    lumen = 1f;
                }

                GameManager.Instance.SetLightSystem(lightsOn, lumen * 3f / 5 + 0.3f);

                /*lumen =  isPM ?
                    1 - (mod60 / 60)
                    :
                    (mod60 / 60);*/


                yield return new WaitForEndOfFrame();
                yield return new WaitForEndOfFrame();

            }
        }
    }
}
