﻿//***********************************************************************//
//***********************************************************************//
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Common.Tools;
using Common.Data;
using MiceCompany;
#if Light2D
using Light2D;
#endif
using Common.PathFinding;
using System;
using System.Linq;
using Light2D;

namespace Common.Managers
{

    /// <summary>
    /// The game manager is a persistent singleton that handles points and time
    /// </summary>
    public partial class GameManager : Singleton<GameManager> 
    {
        [Space(20)]
        public Transform MiceRoot;
        public Transform BuffRoot;
        public Transform PlacableRoot;
        public Item SelectedItemView;
        public List<PlayerMice> PlayerMicesInTheMap = new List<PlayerMice>();
        public List<PlayerMice> DeathMices = new List<PlayerMice>();
        public List<PlayerMice> ArrivedMices = new List<PlayerMice>();
        public List<int> UsedMices = new List<int>();
        public List<int> UsedBuffs = new List<int>();
        public List<Lootable> LootedItems = new List<Lootable>();

        public GamePlaySituation CurrentGamePlaySituation;

        protected Timer mainTimer;
        public static Timer MainTimer
        {
            get
            {
                return Instance.mainTimer;
            }
        }

        public bool IsNightMode
        {
            get
            {
                return LightingSystem.isActiveAndEnabled;
            }
        }
        public float LightValue
        {
            get
            {
                if (!IsNightMode)
                    return 1;
                else
                    return LightCamera.backgroundColor.r;
            }
        }
        public LightingSystem LightingSystem;
        public Camera LightCamera;

        protected override void Awake()
        {
            //Application.targetFrameRate = 300;
            mainTimer = new GameObject("MainTimer", typeof(Timer)).GetComponent<Timer>();
            mainTimer.gameObject.transform.parent = transform;

            _savedTimeScale = TimeScale;
            Time.timeScale = TimeScale;

            MainTimer.TimeChanged = (time) =>
            {
                GameView.Instance.TimerLabel.Text.Value = Timer.TimeToString(Mathf.Abs(MainTimer.TargetTime - time));
            };
            MainTimer.TimerFinished = (time) =>
            {
                UpdateCurrentSituation();

                if(CurrentGamePlaySituation.Passed)
                    EventManager.TriggerEvent(E.PlayerWon);
                else
                    EventManager.TriggerEvent(E.PlayerLost);

            };

            // Bir Fare Doğarsa
            EventManager.StartListeningObjectEvent(E.MiceBorn, (o) =>
            {
                if(o is PlayerMice)
                    GameManager.Instance.PlayerMicesInTheMap.Add(o as PlayerMice);

                if (Status != GameStatus.MainMenu)
                    UpdateCurrentSituation();
            });
            // Bir Fare Peynire Ulaşırsa
            EventManager.StartListeningObjectEvent(E.MiceArrived, (o) =>
            {
                if (Status == GameStatus.MainMenu)
                    return;

                ArrivedMices.Add(o as PlayerMice);
                UpdateCurrentSituation();

                // Fareler bitti ve bölüm geçildiyse
                if (PlayerMicesInTheMap.Count + GameView.Instance.MouseList.PresentedListItems.Count == 0 && CurrentGamePlaySituation.Passed)
                {
                    Debug.Log("Kullanılabilecek Başka Fare kalmadı !");
                    MainTimer.PauseTimer();
                    EventManager.TriggerEvent(E.PlayerWon);
                }
                // Fareler bitmedi ama bölüm geçildiyse
                else if (CurrentGamePlaySituation.Passed)
                {
                    EventManager.TriggerEvent(E.PlayerWon);
                }
            });
            // Bir Fare Ölürse
            EventManager.StartListeningObjectEvent(E.MiceDeath, (o) =>
            {
                if (Status == GameStatus.MainMenu || !(o is PlayerMice))
                    return;

                DeathMices.Add(o as PlayerMice);

                UpdateCurrentSituation();

                if (!CurrentGamePlaySituation.PassedNotDeathRequirement)
                {
                    EventManager.TriggerEvent(E.PlayerLost);
                }
                // Eldeki ve Haritadaki bütün fareler biterse
                else if (PlayerMicesInTheMap.Count + GameView.Instance.MouseList.PresentedListItems.Count == 0)
                {
                    Debug.Log("Kullanılabilecek Başka Fare kalmadı !");
                    EventManager.TriggerEvent(E.PlayerLost);
                }
                //else
                //Debug.Log(DeathMices.Count + ":" + ArrivedMices.Count + ":" + LevelLoadManager.CurrentSubCh.GivenMices.Count + ":" + PlayerMicesInTheMap.Count);

            });
            // Bir Fare Haritaya yerleştirilirse
            EventManager.StartListeningObjectEvent(E.ItemPlaced, (o) =>
            {
                if(o is PlayerMice)
                {
                    UsedMices.Add((o as PlayerMice).PreLoadedIndex);
                }
                if (o is Buff)
                {
                    UsedBuffs.Add((o as Buff).PreLoadedIndex);
                    SelectedItemView = null;
                }
            });
            // Bir Fare Haritadan kaldırılırsa
            EventManager.StartListeningObjectEvent(E.ItemRemoved, (o) =>
            {
                if (o is PlayerMice)
                {
                    UsedMices.Remove((o as PlayerMice).PreLoadedIndex);
                }
            });
            // Bölüm Kazanılırsa
            EventManager.StartListening(E.PlayerWon, () => 
            {
                // Timerdan kaynaklı PlayerWon'sa
                if (MainTimer.IsPaused)
                {
                    SetTimeScale(0);
                    StartCoroutine(GameOver());
                }
                // Timer bitmediyse Geçildi butonunu göster
                else
                {
                    GameView.Instance.GamePlayButtons.SwitchTo(2,true);
                }

            });
            // Bölüm Kaybedilirse
            EventManager.StartListening(E.PlayerLost, () =>
            {
                SetTimeScale(0);
                StartCoroutine(GameOver());
            });
            // Bir Loot toplanırsa
            EventManager.StartListeningObjectEvent(E.LootableLooted, (o) =>
            {
                GameManager.Instance.UpdateCurrentSituation();
            });
            // Bir eşya Koyulur, Kaldırılır, Çevirilir, hareket ettirilirse PathFindingi tekrar hesapla
            EventManager.StartListeningObjectEvent(E.ItemPlaced, (p) => { if (p is Placable) PathFinder.Instance.CalculateAllPathsToTarget(); });
            EventManager.StartListeningObjectEvent(E.ItemMoved,  (p) => { if (p is Placable) PathFinder.Instance.CalculateAllPathsToTarget(); });
            EventManager.StartListeningObjectEvent(E.ItemRotated, (p) => { if (p is Placable) PathFinder.Instance.CalculateAllPathsToTarget(); });
            EventManager.StartListeningObjectEvent(E.ItemRemoved, (p) => { if (p is Placable) PathFinder.Instance.CalculateAllPathsToTarget(); });
        }

        void Start()
        {
            OnChapterLoaded(LevelLoadManager.CurrentIndex);
            if (LevelLoadManager.CurrentIndex.X != 0)
                SetStatus(GameStatus.BeforeGameStart);
            else
                SetStatus(GameStatus.MainMenu);
        }

        public void OnClickedToGameArea()
        {
#if UNITY_EDITOR
            //Debug.Log("Tap:" + InputManager.IsClickedUI);
#endif
            if (InputManager.IsClickedUI)
                return;

            RaycastHit2D[] hits = InputManager.ScreenRayAll2D();

            bool spawnZone = false;
            bool placableZone = false;
            bool floor = false;
            Selectable selectableItem = null;
            PlayerMice clickedMouse = null;
            Vector2 point = new Vector2();

            foreach (var hit in hits)
            {
#if UNITY_EDITOR
                Debug.Log("Hit:" + hit.collider.name);
#endif
                point = hit.point;

                if (!spawnZone && hit.collider.gameObject.GetCachedComponent<SpawnZone>())
                    spawnZone = true;
                if (!placableZone && hit.collider.gameObject.GetCachedComponent<PlacableZone>())
                    placableZone = true;
                if (!floor && hit.collider.gameObject.GetCachedComponent<Floor>())
                    floor = true;
                if (clickedMouse == null && hit.collider.gameObject.GetCachedComponent<PlayerMice>())
                    clickedMouse = hit.collider.gameObject.GetCachedComponent<PlayerMice>();

                //Daha önce bir selectable a tıklanmadıysa
                if(selectableItem == null)
                    selectableItem = hit.collider.gameObject.GetCachedComponent<Selectable>();
                // Selectable Componenti var IsSelectable deilse
                if (selectableItem != null && !selectableItem.IsSelectable)
                    selectableItem = null;
            }

            // Mouse tıklamadı, Zemine tıkladı ve Selectable a tıkladıysa
            // Interesting oluştur
            if (!clickedMouse && floor && Time.timeScale > 0 && !selectableItem && SelectedItemView == null)
                Interesting.SpawnOnPointerPose(PreLoadedAssets.InterestingPositive);

            // Eğer Mouse Seçilmişse
            if (SelectedItemView is PlayerMice && spawnZone && !selectableItem)
                SpawnItem(SelectedItemView, point, UnityEngine.Random.Range(0, 180));

            // eğer Buff Seçilmişse
            if (SelectedItemView is Buff && floor && !selectableItem)
            {
                if (clickedMouse != null)
                    SpawnItem(SelectedItemView, point, 0, clickedMouse);
                else
                    SpawnItem(SelectedItemView, point, 0);
            }

            // eğer Placable Seçilmişse
            if (SelectedItemView is Placable && placableZone && !selectableItem)
                SpawnItem(SelectedItemView, point, 0);

            if (selectableItem)
                selectableItem.SetIsSelected(!selectableItem.IsSelected);

            //Debug.Log(selectableItem + ":" + clickedMouse);
        }

        /// <summary>
        /// Update
        /// </summary>
        void Update()
        {

        }


        /// <summary>
        /// Chapter Loading Finished
        /// </summary>
        internal void OnChapterLoaded(Point sc)
        {
            MiceRoot = GameObject.FindWithTag("MiceRoot").transform;
            if (MiceRoot == null)
                Debug.LogError("MiceContainer Tag atanmalı !!!");
            BuffRoot = GameObject.FindWithTag("BuffRoot").transform;
            if (BuffRoot == null)
                Debug.LogError("BuffContainer Tag atanmalı !!!");
            PlacableRoot = GameObject.FindWithTag("PlacableRoot").transform;
            if (PlacableRoot == null)
                Debug.LogError("PlacableContainer Tag atanmalı !!!");

            LightingSystem = GameObject.FindObjectOfType<LightingSystem>();
            if (LightingSystem == null)
                Debug.LogError("LightingSystem NULL !!!");

            LightCamera = Camera.main.GetComponentsInChildren<Camera>()[1];
            if (LightCamera == null)
                Debug.LogError("LightCamera NULL !!!");

            DeathMices.Clear();
            ArrivedMices.Clear();
            UsedMices.Clear();
            UsedBuffs.Clear();
            LootedItems.Clear();

            if (LevelLoadManager.CurrentIndex.X == 0)
            {
                MainTimer.ResetTimer();
                SetStatus(GameStatus.MainMenu);
                UIManager.Instance.ChangeMainView(typeof(MenuView));
                return;
            }

            MiceRoot.transform.position = MiceRoot.transform.position.WithZ(-5);
            PlacableRoot.transform.position = PlacableRoot.transform.position.WithZ(-4);

            PlayerMicesInTheMap.Clear();
            var mices = FindObjectsOfType<PlayerMice>();
            PlayerMicesInTheMap.AddRange(mices);

            // Başlangıçta mevcut olan fareler haritadan kaldırılamaz
            foreach(var m in PlayerMicesInTheMap)
            {
                var s = m.GetComponent<Selectable>();
                if (s != null)
                    s.IsRemovable = false;
            }

            SetTimeScale(0);
            SetStatus(GameStatus.BeforeGameStart);

            UIManager.Instance.ChangeMainView(typeof(GameView));
            UIManager.Instance.ChangeSubView(typeof(GamePreparationView));


            UpdateCurrentSituation();
        }

        public void StartPlaying()
        {
            if (LevelLoadManager.CurrentIndex.Equals(new Point(1, 1)))
            {
                SocialManager.Instance.SetAchievement(PlayerManager.Instance.AllAchievements[A.FirstPlaying]);
                PlayerManager.Instance.SetAchievement(PlayerManager.Instance.AllAchievements[A.FirstPlaying]);
            }


            CurrentGamePlaySituation = new GamePlaySituation();
            GameManager.Instance.SetStatus(GameManager.GameStatus.GameInProgress);
            GameManager.Instance.SetTimeScale(1);

            MainTimer.ResetTimer();
            MainTimer.TargetTime = LevelLoadManager.CurrentSubCh.ToWinSeconds > 0 ? 
                LevelLoadManager.CurrentSubCh.ToWinSeconds : 0;
            MainTimer.StartTimer();
        }

        public void UpdateCurrentSituation()
        {
            if (Status == GameStatus.MainMenu)
                return;

            // ilk önce oyunun geçilip geçilmediği
            CurrentGamePlaySituation.PassedArriveRequirement =
                (LevelLoadManager.CurrentSubCh.ToWinArrive <= 0 || LevelLoadManager.CurrentSubCh.ToWinArrive <= ArrivedMices.Count);
            CurrentGamePlaySituation.PassedNotDeathRequirement =
                (LevelLoadManager.CurrentSubCh.ToWinNotDeath <= -1 || DeathMices.Count == 0 || LevelLoadManager.CurrentSubCh.ToWinNotDeath > DeathMices.Count);
            CurrentGamePlaySituation.PassedTimeRequirement =
                LevelLoadManager.CurrentSubCh.ToWinSeconds <= 0 ||  // Süre sınırı yoksa
                (CurrentGamePlaySituation.PassedArriveRequirement && CurrentGamePlaySituation.PassedNotDeathRequirement) || // Diğer şartlar sağlandıysa
                LevelLoadManager.CurrentSubCh.ToWinSeconds > MainTimer.CurrentTime; // yada sürenin altında bittiyse

            // Ardında Sayısal bilgiler
            CurrentGamePlaySituation.AliveMices = GameManager.Instance.PlayerMicesInTheMap.Count;
            CurrentGamePlaySituation.DeathMices = GameManager.Instance.DeathMices.Count;
            CurrentGamePlaySituation.ArrivedMices = GameManager.Instance.ArrivedMices.Count;
            CurrentGamePlaySituation.RemainingTime = MainTimer.TargetTime;
            //Geçilmediyse süreyi güncellemeye devam et (bu şekilde geçildiği süre kayıtlı kalır)
            if(!CurrentGamePlaySituation.Passed)
                CurrentGamePlaySituation.PassedTime = MainTimer.TargetTime > 0 ? MainTimer.TargetTime - MainTimer.CurrentTime : MainTimer.CurrentTime;

            CurrentGamePlaySituation.Score = CalculateScore(Status != GameStatus.GameOver);

            int starCount = 0;
            if (CurrentGamePlaySituation.Score >= LevelLoadManager.CurrentSubCh.StarScore[1])
                starCount++;
            if (CurrentGamePlaySituation.Score >= LevelLoadManager.CurrentSubCh.StarScore[2])
                starCount++;
            if (CurrentGamePlaySituation.Score >= LevelLoadManager.CurrentSubCh.StarScore[3])
                starCount++;
            CurrentGamePlaySituation.Star = starCount;

            CurrentGamePlaySituation.InGameRevenueMoney = 0;
            CurrentGamePlaySituation.InGameRevenueSpecMoney = 0;
            CurrentGamePlaySituation.InGameRevenueXp = 0;
            foreach (var l in LootedItems)
            {
                CurrentGamePlaySituation.InGameRevenueMoney += l.RevenueMoney;
                CurrentGamePlaySituation.InGameRevenueSpecMoney += l.RevenueSpecMoney;
                CurrentGamePlaySituation.InGameRevenueXp += l.RevenueXp;
            }
            CurrentGamePlaySituation.InGameRevenueMoney += LevelLoadManager.CurrentSubCh.StarMoney[starCount];
            CurrentGamePlaySituation.InGameRevenueSpecMoney += LevelLoadManager.CurrentSubCh.StarSpecMoney[starCount];
            CurrentGamePlaySituation.InGameRevenueXp += LevelLoadManager.CurrentSubCh.StarXp[starCount];

            GameView.Instance.Money.Value = "+ " + CurrentGamePlaySituation.InGameRevenueMoney;
            GameView.Instance.SpecialMoney.Value = "+ " + CurrentGamePlaySituation.InGameRevenueSpecMoney;
            GameView.Instance.CurrentScore.Value = CurrentGamePlaySituation.Score + "";
            GameView.Instance.Alives.Value = CurrentGamePlaySituation.AliveMices + "";
            GameView.Instance.Deaths.Value = CurrentGamePlaySituation.DeathMices + "";
            GameView.Instance.Arrives.Value = CurrentGamePlaySituation.ArrivedMices + "";
        }

        public virtual int CalculateScore(bool isPlaying)
        {
            int score = 0;

            // Kalan süreden +puan
            if (!isPlaying && CurrentGamePlaySituation.Passed)
            {
                // Süre Zorunluluksa
                if (LevelLoadManager.CurrentSubCh.ToWinSeconds > 0)
                    score += (int)(LevelLoadManager.CurrentSubCh.ToWinSeconds - MainTimer.CurrentTime) * 5;
                // Süre Zorunluluğu yoksa
                else
                {
                    score += (int)(
                        (-LevelLoadManager.CurrentSubCh.ToWinSeconds*3) // Her yıldız için olası bitirme süresi
                        / MainTimer.CurrentTime) // bitirdiği süre  ToWinSeconds*3 den büyükse 0 puan
                        * 10; 
                }

                // Bölümü bitirebildiği için + puan
                score += 50;
            }

            // Hedefe ulaşan farelerden +puan
            score += ArrivedMices.Count * 10;

            // Ölmeyen farelerden +puan
            if (LevelLoadManager.CurrentSubCh.ToWinNotDeath > -1)
            {
                score += LevelLoadManager.CurrentSubCh.ToWinNotDeath - DeathMices.Count * 5;
            }

            // Kullanılmayan farelerden  +puan


            if (score < 0)
                return 0;
            else
                return score;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="isWon"></param>
        /// <returns></returns>
        public IEnumerator GameOver()
        {
            SetStatus(GameStatus.GameOver);

            UpdateCurrentSituation();

            bool saveNewScore = false;
            // Yeni skor kaydedilecekmi kaydedilmiyecek mi?
            if (!PlayerManager.CurrentPlayer.PassedSubChapterScores.ContainsKey(LevelLoadManager.CurrentIndex))
                saveNewScore = true;
            // Daha önce kazanılan yıldızdan fazla kazanıldıysa
            else if (PlayerManager.CurrentPlayer.PassedSubChapterScores[LevelLoadManager.CurrentIndex].Star < CurrentGamePlaySituation.Star)
                saveNewScore = true;
            // yıldız eşit ama Daha önce yapılan skordan fazla skor yapıldıysa
            else if (PlayerManager.CurrentPlayer.PassedSubChapterScores[LevelLoadManager.CurrentIndex].Star == CurrentGamePlaySituation.Star &&
                    PlayerManager.CurrentPlayer.PassedSubChapterScores[LevelLoadManager.CurrentIndex].Score < CurrentGamePlaySituation.Score)
                saveNewScore = true;

            // Initialize GameOverView
            if (PlayerManager.CurrentPlayer.PassedSubChapterScores.ContainsKey(LevelLoadManager.CurrentIndex))
            {
                GameOverView.Instance.InitializeFinishResult(
                    PlayerManager.CurrentPlayer.PassedSubChapterScores[LevelLoadManager.CurrentIndex],
                    CurrentGamePlaySituation);
                GameOverView.Instance.InitializeStatsRegion(
                    PlayerManager.CurrentPlayer.PassedSubChapterScores[LevelLoadManager.CurrentIndex],
                    CurrentGamePlaySituation);
            }
            else
            {
                GameOverView.Instance.InitializeFinishResult(
                    new PassedSubCh(), 
                    CurrentGamePlaySituation);
                GameOverView.Instance.InitializeStatsRegion(
                    new PassedSubCh(),
                    CurrentGamePlaySituation);
            }

            // Skoru kaydet, Skor değişse bile tekrar Skorlar alındığında şu ankini üzerine yazıyor
            if (CurrentGamePlaySituation.Passed && saveNewScore)
            {
                // şimdilik iptal
                PlayerManager.Instance.SaveScore(CurrentGamePlaySituation.Result());

                // Geçilen Chaptera ait bütün skorlar alınır
                var indexes = PlayerManager.CurrentPlayer.PassedSubChapterScores.Keys.Where(p => p.X == LevelLoadManager.CurrentIndex.X);
                int sum = 0;
                foreach (var i in indexes)
                    sum += PlayerManager.CurrentPlayer.PassedSubChapterScores[i].Score;
                // Chapter: GooglePlay,GameJolt veya Facebook a kaydedilir
                SocialManager.Instance.SetScore(LevelLoadManager.CurrentCh, sum);

                int sumAll = 0;
                foreach (var i in PlayerManager.CurrentPlayer.PassedSubChapterScores.Keys)
                    sumAll += PlayerManager.CurrentPlayer.PassedSubChapterScores[i].Score;
                // Main: GooglePlay,GameJolt veya Facebook a kaydedilir
                SocialManager.Instance.SetScore(new Chapter()
                {
                    GooglePlayScoreTableId = LevelLoadManager.CurrentCh.GooglePlayScoreTableId,
                    GameJoltScoreTableId = LevelLoadManager.CurrentCh.GameJoltScoreTableId
                }, sumAll);

            }

            yield return new WaitForSecondsRealtime(0.5f);

            if (CurrentGamePlaySituation.Passed)
            {
                PlayerManager.CurrentPlayer.Money += CurrentGamePlaySituation.InGameRevenueMoney;
                PlayerManager.CurrentPlayer.SpecialMoney += CurrentGamePlaySituation.InGameRevenueSpecMoney;
                PlayerManager.CurrentPlayer.Xp += CurrentGamePlaySituation.InGameRevenueXp;
            }

            // Kullanılan buffları envanterden çıkar
            RemoveUsedItemsFromInventory();

            // Geçilen Bölüm ve Kazanılanları Kaydet
            PlayerManager.Instance.SavePlayerData();

            PlayerManager.Instance.InitializeChapters();
            UIManager.Instance.ChangeSubView(typeof(GameOverView));
            MainTimer.ResetTimer();

        }
#if FirebaseDatabase
                        /*GetFriendScores(
                    PlayerManager.CurrentPlayer.PassedSubChapterScores[new Point(LevelLoadManager.ActiveCh, LevelLoadManager.ActiveSubCh)],
                    CurrentGamePlaySituation.Result(),
                (friends) =>
                {
                    GameOverView.Instance.InitializeFriendsRegion(friends);
                });*/

        public void GetFriendScores(PassedSubCh oldS, PassedSubCh newS,Action<List<FriendPassedSubCh>> friendsResult)
        {
            Debug.Log("GettingScores:" + "Scores/" + LevelLoadManager.CurrentSubChName);
            // Scoreları alma fonksiyonu
            DatabaseManager.Instance.Get<object>("Scores/" + LevelLoadManager.CurrentSubChName, (obj, snap) =>
            {
                // Score Listesi Alındıktan sonra burdaki Id lere göre Profiller alınacak
                GetProfilesMergedWithScores(snap, (nonShortedList)=> {
                    // Kendi bilgilerinide Ekle
                        FriendPassedSubCh mySummary = new FriendPassedSubCh();
                        mySummary.FriendProfile = PlayerManager.CurrentProfile;
                        if (newS.Score > oldS.Score)
                            mySummary.FriendPSC = newS;
                        else
                            mySummary.FriendPSC = oldS;

                    //Mevcutsa var olanla değiştir
                    if (nonShortedList.Exists(fs => fs.Id == PlayerManager.CurrentPlayer.ID))
                        nonShortedList[nonShortedList.FindIndex(fs => fs.Id == PlayerManager.CurrentPlayer.ID)] = mySummary;
                    else
                        nonShortedList.Add(mySummary);


                    // Score a göre Sırala
                    var ordered = nonShortedList.OrderByDescending(fs => fs.FriendPSC.Score);

                    // Sıra numarasını ekle
                    for (int i = 0; i < nonShortedList.Count; i++)
                        ordered.ElementAt(i).Rank = i + 1;

                    friendsResult.Invoke(ordered.ToList());
                });
            }, "Score", 20);
        }

        void GetProfilesMergedWithScores(Firebase.Database.DataSnapshot scoreList, Action<List<FriendPassedSubCh>> friendsResult)
        {
            List<string> ids = new List<string>();

            foreach (var kv in scoreList.Children)
            {
                ids.Add("Profiles/" + kv.Key);
                //Debug.Log("O:" + kv.Key);
            }

            DatabaseManager.Instance.Get(ids, (snaps) =>
            {
                List<FriendPassedSubCh> friendScores = new List<FriendPassedSubCh>();
                // Alınan Profil bilgileri ile Önceden alınan Score ları birleştir
                foreach (var kv in snaps)
                {
                    var p2 = ObjectSerializer.FromJson<Profile>(
                            kv.Value.GetRawJsonValue());
                    var psc2 = ObjectSerializer.FromJson<PassedSubCh>(
                            scoreList.Child(kv.Value.Key).GetRawJsonValue());

                    friendScores.Add(new FriendPassedSubCh()
                    {
                        Id = kv.Value.Key,
                        FriendProfile = p2,
                        FriendPSC = psc2
                    });
                }


                //CConsole.Log(ObjectSerializer.ToJson(friendScores, true), new Color(1f, 0, 1f));
                friendsResult.Invoke(friendScores);
            });
        }
#endif


        void RemoveUsedItemsFromInventory()
        {
            // oyun başında verilenler kullanılan bufflardan çıkarılır
            foreach (var b in LevelLoadManager.CurrentSubCh.GivenBuffs)
                UsedBuffs.Remove(b);

            // kalanlarda Oyuncunun envanterinden çıkarılır
            foreach (var b in UsedBuffs)
                PlayerManager.CurrentPlayer.OwnedBuffs.Remove(b);
        }

        public void SetLightSystem(bool nightMode, float ambientLight = -1)
        {
            if (LightCamera == null || LightingSystem == null)
                return;

            if(ambientLight >= 0)
            {
                ambientLight = ambientLight / 2;
                LightCamera.backgroundColor = new Color(ambientLight, ambientLight, ambientLight, 1);
                // birazcık Mavi gece efekti için
                //LightCamera.backgroundColor = new Color(ambientLight, ambientLight, Mathf.Clamp01(ambientLight + 0.05f), 1);
            }


            if (nightMode)
            {
                LightingSystem.enabled = true;
                LightCamera.enabled = true;
            }
            else
            {
                LightingSystem.enabled = false;
                LightCamera.enabled = false;
            }
        }


        public Item SpawnItem(Item prefab, Vector2 pos, float rot, Item connected = null)
        {
            SelectionCircle.Instance.Detach();

            if (prefab is PlayerMice)
            {
                if (MiceRoot != null)
                {
                    var obj = Instantiate(prefab.gameObject, pos, Quaternion.Euler(0, 0, rot), MiceRoot);
                    obj.transform.localPosition = obj.transform.localPosition.WithZ(0);
                    obj.name = prefab.name;
                    EventManager.TriggerObjectEvent(E.ItemPlaced, obj.GetComponent<PlayerMice>());

                    return obj.GetComponent<PlayerMice>();
                }
                else
                    return null;
            }
            else if (prefab is Buff)
            {
                Buff b = prefab as Buff;
                if (b.AreaSize == Buff.EffectSize.OneItem)
                    b = Buff.Use(prefab as Buff, connected as PlayerMice);
                else  if(b.AreaSize == Buff.EffectSize.OnArea)
                    b = Buff.Use(prefab as Buff, pos);
                else
                    b = Buff.Use(prefab as Buff);
                EventManager.TriggerObjectEvent(E.ItemPlaced, b);
                return b;

            }
            else if (prefab is Placable)
            {
                if (PlacableRoot != null)
                {
                    var obj = Instantiate(prefab.gameObject, pos, Quaternion.Euler(0, 0, rot), PlacableRoot);
                    obj.transform.localPosition = obj.transform.localPosition.WithZ(0);
                    obj.name = prefab.name;
                    EventManager.TriggerObjectEvent(E.ItemPlaced, obj.GetComponent<Placable>());

                    return obj.GetComponent<Placable>();
                }
                else
                    return null;

            }
            else
                return null;

        }
    }
}