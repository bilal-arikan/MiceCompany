﻿//***********************************************************************//
//***********************************************************************//
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Common.Tools;
using Common.Data;
using MiceCompany;

namespace Common.Managers
{
    public partial class PlayerManager: Singleton<PlayerManager>
    {
        protected override void Awake()
        {
            base.Awake();
#if UNITY_EDITOR
            MenuView.Instance.ProfileButton.IsDisabled.Value = false;
#endif
            // kullanıcı bilgileri değiştiğinde firebase kullanıcısını güncelle
            EventManager.StartListeningObjectEvent(E.PlayerProfileChanged, (pl) =>
            {
                Debug.Log("PlayerManager: PlayerProfileChanged ");

                if(CurrentProfile != null)
                    Core.DownloadPhoto(CurrentProfile.PhotoUrl, (sprite) =>
                    {
                        if (sprite != null)
                        {
                            CurrentProfile.Photo = sprite;
                            EventManager.TriggerObjectEvent(E.PlayerPhotoChanged, this, true);
                        }
                    });
                //ŞİMDİLİK GEREK YOK
                /*if (AuthenticationManager.UserIsValid)
                {
                    // 3 saniye sonra günceller
                    AuthenticationManager.Instance.UpdateProfile(p.DisplayName, p.PhotoUrl, (s) =>
                    {
                        Debug.Log("Update Profile " + (s ? "Success" : "Failed"));
                    });
                }*/
            });
            // Firebase e giriş yapıldığında
            EventManager.StartListening(E.AppStarted, () =>
            {
                Debug.Log("PlayerManager: AppStarted ");
                //LoadPlayerProfile(); Profil bilgileri Networkden gelecek
                LoadPlayerData();
                LoadPlayerProfile();
            });
#if FirebaseAuthentication
            EventManager.StartListeningObjectEvent(E.FirebaseSignIn, (userId) =>
            {
                Debug.Log("PlayerManager FirebaseSignIn " + userId);
                //LoadPlayerProfile(); Profil bilgileri Networkden gelecek
                LoadPlayerData();
            });
            // Firebaseden çıkış yapıldığında
            EventManager.StartListeningObjectEvent(E.FirebaseSignOut, (userId) =>
            {
                Debug.Log("PlayerManager FirebaseSignOut " + userId);
                currentPlayer = null;
                currentProfile = null;
                EventManager.TriggerObjectEvent(E.PlayerDataChanged, null);
                EventManager.TriggerObjectEvent(E.PlayerProfileChanged, null);
            });
#endif
            // InitializeChapters when Player data Loaded or Unloaded
            EventManager.StartListeningObjectEvent(E.PlayerDataChanged, (p) =>
            {
                Debug.Log("PlayerManager: PlayerDataChanged ");

                InitializeChapters();
                MenuView.Instance.StartButton.IsDisabled.Value = !(CurrentPlayer != null);
                MenuView.Instance.MarketButton.IsDisabled.Value = !(CurrentPlayer != null);
                MenuView.Instance.ProfileButton.IsDisabled.Value = !(CurrentPlayer != null);

                // Data Load yapıldıysa
                if(CurrentPlayer != null)
                {
                    CheckDailyGifts();

                    ProfileView.Instance.Level.Value = PlayerManager.CurrentPlayer.Level.ToString();
                    ProfileView.Instance.Xp.Value = PlayerManager.CurrentPlayer.Xp.ToString();
                    ProfileView.Instance.InventoryList.SetItems(
                        PlayerManager.CurrentPlayer.OwnedBuffs.ConvertAll(b => PreLoadedAssets.Instance.BuffPrefabs[b].gameObject.GetCachedComponent<ShowableOnList>()));
                }
                // Data Unload yapıldıysa
                else
                {
                    ProfileView.Instance.Level.Value = "";
                    ProfileView.Instance.Xp.Value = "";
                    ProfileView.Instance.InventoryList.SetItems(new List<ShowableOnList>());
                }

                UIManager.Instance.SetWaitingtState(false);
            });
            // Level Up
            EventManager.StartListeningObjectEvent(E.PlayerLevelUp, (level) =>
            {
                UIManager.Instance.ShowToast("Level Up (" + (int)level + ")");
            });
            // When a SubCh Unlocked
            EventManager.StartListeningObjectEvent(E.SubChapterUnlocked, (s) =>
            {
                SubChapter sch = (SubChapter)s;
                if(!PlayerManager.CurrentPlayer.UnlockedSubChapters.Contains(sch.IndexPoint))
                    PlayerManager.CurrentPlayer.UnlockedSubChapters.Add(sch.IndexPoint);
                sch.LinkedButton.Unlocked = true;

                SavePlayerData();
                //UIManager.Instance.ShowToast("Chapter " + sch.Index.X.ToString("00") + sch.Index.Y.ToString("00") + " Unlocked");
            });
            // Günlük giriş bonusu
            EventManager.StartListeningObjectEvent(E.DailyGift, (day) =>
            {
                UIManager.Instance.ShowToast("Tebrikler " + (int)day + " Günlük Giriş Hediyesi kazandınız");
                CurrentPlayer.Money += (int)day * 100;
                SavePlayerData();
            });
            // Video izleme bonusu
            EventManager.StartListeningObjectEvent(E.RewardVideoGift, (amount) =>
            {
                UIManager.Instance.ShowToast("Congratulations You Earned 300 Money");
                PlayerManager.CurrentPlayer.Money += 300;
                SavePlayerData();
            });
            // Invite bonusu
            EventManager.StartListeningObjectEvent(E.InvitedGift, (amount) =>
            {
                UIManager.Instance.ShowToast("Invite Gifts:" + (int)amount * 100 +"Money");
                PlayerManager.CurrentPlayer.Money += (int)amount * 100;
                SavePlayerData();
            });
            // Item Satın Alındığında
            EventManager.StartListeningObjectEvent(E.ItemPurchased, (itemId) =>
            {
                //UIManager.Instance.ShowToast(itemId + " Purchased");
                if (itemId.ToString() == IAP.NonCons.NoAds)
                {
                    AdsManager.Instance.enabled = false;
                    UIManager.Instance.ShowToast("All Advertisements Disabled");
                }
                else if(itemId.ToString() == IAP.Cons.Gold50)
                {
                    CurrentPlayer.SpecialMoney += 50;
                    SavePlayerData();
                    UIManager.Instance.ShowToast("50 Diamond Given");
                }
                else if (itemId.ToString() == IAP.Cons.Gold200)
                {
                    CurrentPlayer.SpecialMoney += 200;
                    SavePlayerData();
                    UIManager.Instance.ShowToast("200 Diamond Given");
                }
            });


            // Load Player
            CConsole.ActionsNoArg.Add("loadfromlocal", () =>
            {
                var profile = DatabaseManager.Local.Get<Profile>("Profiles/null");
                if (profile != null)
                {
                    currentProfile = profile;
                    CConsole.Log("Profile Loaded: From Local", Color.cyan);
                }
                // Ordanda alınamazsa yeni Player oluştur
                else
                {
                    currentProfile = new Profile();
                    currentProfile.PhotoUrl = ConfigManager.invite_image;
                    CConsole.Log("No Data (New Profile Created)", Color.cyan);
                }
                EventManager.TriggerObjectEvent(E.PlayerProfileChanged, CurrentProfile , true);
                var player = DatabaseManager.Local.Get<Player>("Players/null");
                if (player != null)
                {
                    currentPlayer = player;
                    isFirstPlaying = false;
                    CConsole.Log("Player Loaded: From Local", Color.cyan);
                }
                // Ordanda alınamazsa yeni Player oluştur
                else
                {
                    currentPlayer = new Player();
                    isFirstPlaying = true;
                    SavePlayerData();
                    CConsole.Log("No Data (New Player Created)", Color.cyan);
                }
                EventManager.TriggerObjectEvent(E.PlayerDataChanged, CurrentPlayer, true);
            });
            // Save Player
            CConsole.ActionsNoArg.Add("savetolocal", () =>
            {
                DatabaseManager.Local.Set("Players/null",CurrentPlayer);
                DatabaseManager.Local.Set("Profiles/null", CurrentProfile);
                EventManager.TriggerObjectEvent(E.PlayerDataSaved, CurrentPlayer, true);
            });
        }
        


        /// <summary>
        /// Initialize Chapters acording to CurrentPlayer
        /// </summary>
        public void InitializeChapters()
        {
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Önce bütün SubChleri kilitle
            for (int i = 1; i <= LevelLoadManager.Instance.AllChapters.Count; i++)
                // İlk SubCh ler hariç
                for (int j = 1; j <= LevelLoadManager.Instance.AllChapters[i].Count; j++)
                    LevelLoadManager.Instance.AllChapters[i][j].LinkedButton.Unlocked = j == 1;
            // sonra kilidi açılmış bölümlerin kilidin aç
            if(CurrentPlayer != null)
                for (int i = 0; i < CurrentPlayer.UnlockedSubChapters.Count; i++)
                    LevelLoadManager.Instance.AllChapters
                        [CurrentPlayer.UnlockedSubChapters.ElementAt(i).X]
                        [CurrentPlayer.UnlockedSubChapters.ElementAt(i).Y].LinkedButton.Unlocked = true;

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Önce bütün bölümleri disable et (ilk SubChler hariç)
            for (int i = 1; i <= LevelLoadManager.Instance.AllChapters.Count; i++)
            {
                // Önce bütün Ch'leri disable et (ilk Ch hariç)
                LevelLoadManager.Instance.AllChapters[i].LinkedChView.ChSelect.IsDisabled.Value = i > 1;
                // Önce bütün SubCh'leri disable et (ilk SubChler hariç)
                for (int j = 1; j <= LevelLoadManager.Instance.AllChapters[i].Count; j++)
                {
                    LevelLoadManager.Instance.AllChapters[i][j].LinkedButton.CheckBox.IsDisabled.Value = j > 1;
                    LevelLoadManager.Instance.AllChapters[i][j].LinkedButton.ShowStars(0);
                }
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Ardından Giriş Yapıldıysa kullanıcın geçtiği bölümlere göre Yıldız ekle veya aç
            if(CurrentPlayer != null)
            {
                foreach (var pscKey in CurrentPlayer.PassedSubChapterScores.Keys)
                {
                    // ilk initialize deilse ve önceden kilitliyse (yani yenice açılıyor)
                    if (
                        ChaptersInitialized &&
                        LevelLoadManager.Instance.AllChapters[pscKey.X].IsLast &&
                        LevelLoadManager.Instance.AllChapters[pscKey.X][pscKey.Y].IsLast &&
                        !LevelLoadManager.Instance.AllChapters[pscKey.X][pscKey.Y].LinkedButton.Unlocked)
                    {
                        UIManager.Instance.ShowToast("Congratulations Game COMPLETED :D");
                    }

                    LevelLoadManager.Instance.AllChapters[pscKey.X][pscKey.Y].LinkedButton.Unlocked = true;
                    LevelLoadManager.Instance.AllChapters[pscKey.X][pscKey.Y].LinkedButton.CheckBox.IsDisabled.Value = false;
                    LevelLoadManager.Instance.AllChapters[pscKey.X][pscKey.Y].LinkedButton.ShowStars(CurrentPlayer.PassedSubChapterScores[pscKey].Star);
                }

                for (int i = 1; i <= LevelLoadManager.Instance.AllChapters.Count; i++)
                {
                    // Bu Chapterin kaç SubChapteri geçilmiş
                    int passedSubChCount = CurrentPlayer.PassedSubChapterScores.Keys.Count(key => key.X == i);

                    //Son SubCh değilse ve  Önceki bölümler geçilmişse sıradakinin kilidini aç
                    if (passedSubChCount > 0 && passedSubChCount < LevelLoadManager.Instance.AllChapters[i].Count)
                    {
                        LevelLoadManager.Instance.AllChapters[i][passedSubChCount + 1].LinkedButton.CheckBox.IsDisabled.Value = false;
                        LevelLoadManager.Instance.AllChapters[i][passedSubChCount + 1].LinkedButton.ShowStars(0);
                    }
                    //Ch nin bütün bölümleri geçildiyse Sıradaki Ch yi aç
                    else if (CurrentPlayer.PassedSubChapterScores.ContainsKey(new Point(i, 1)) && // ilk bölümü geçilmediyse gerisine bakmaya gerke yok
                        passedSubChCount == LevelLoadManager.Instance.AllChapters[i].Count //Ch nin bütün bölümleri geçildiyse
                        && !LevelLoadManager.Instance.AllChapters[i].IsLast) // son Ch değilse
                    {
                        // Daha önce initialize edildiyse
                        if (ChaptersInitialized && LevelLoadManager.Instance.AllChapters[i].NextCh().LinkedChView.ChSelect.IsDisabled.Value)
                        {
                            UIManager.Instance.ShowToast("Next Chapter Now Available");
                        }

                        LevelLoadManager.Instance.AllChapters[i].NextCh().LinkedChView.ChSelect.IsDisabled.Value = false; // Sıradaki Ch yi aç
                    }

                    /*//Son SubCh değilse ve  Önceki bölümler geçilmişse sıradakinin kilidini aç
                    if (CurrentPlayer.PassedSubChapterScores[i].Count > 0 && CurrentPlayer.PassedSubChapterScores[i].Count < MiceCompanyOptions.AllChapters[i].Count)
                    {
                        MiceCompanyOptions.AllChapters[i][CurrentPlayer.PassedSubChapterScores[i].Count + 1].LinkedButton.CheckBox.IsDisabled.Value = false;
                        MiceCompanyOptions.AllChapters[i][CurrentPlayer.PassedSubChapterScores[i].Count + 1].LinkedButton.ShowStars(0);
                    }
                    //Ch nin bütün bölümleri geçildiyse Sıradaki Ch yi aç
                    else if (CurrentPlayer.GetPassedChapter(i, 1) != null && // ilk bölümü geçilmediyse gerisine bakmaya gerke yok
                        CurrentPlayer.PassedSubChapterScores[i].Count == MiceCompanyOptions.AllChapters[i].Count //Ch nin bütün bölümleri geçildiyse
                        && !MiceCompanyOptions.AllChapters[i].IsLast) // son Ch değilse
                    {
                        // yani ilk initialize deilse
                        if (ChaptersInitialized && MiceCompanyOptions.AllChapters[i].NextCh().LinkedChView.ChSelect.IsDisabled.Value)
                            UIManager.Instance.ShowToast("Next Chapter Now Available");

                        MiceCompanyOptions.AllChapters[i].NextCh().LinkedChView.ChSelect.IsDisabled.Value = false; // Sıradaki Ch yi aç
                    }*/
                }
            }
                
            ChaptersInitialized = CurrentPlayer != null;
            Debug.Log("Chapters Initialized");


        }
    }
}
