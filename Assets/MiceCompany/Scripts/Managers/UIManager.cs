﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
using UnityEngine;
using MarkLight;
using Common.Tools;
using Common.Managers;
using MarkLight.Views.UI;
using Common.Data;

namespace Common.Managers
{
    public partial class UIManager : Singleton<UIManager>
    {
        public bool ChapterViewsInitialized = false;

        private void Start()
        {
            EventManager.StartListeningObjectEvent(E.ReceivedMessage, (m) =>
            {
                Firebase.Messaging.FirebaseMessage msg = m as Firebase.Messaging.FirebaseMessage;
                //if(msg.)
            });
        }

        public void InitChapters()
        {

            for (int i = 1; i <= LevelLoadManager.Instance.AllChapters.Count; i++)
            {
                //Chapters
                ChView ch = ChaptersView.Instance.ChaptersViewSwitcher.CreateView<ChView>();
                ch.ChIndex = i;
                ch.ChSprite.Value = new SpriteAsset(
                    new UnityAsset("ChapterFront", LevelLoadManager.Instance.AllChapters[i].FrontSpritePreLoaded));
                ch.Title.Text.Value = LevelLoadManager.Instance.AllChapters[i].Header;
                if (i > 1)
                    ch.Offset.Value = new ElementMargin(1200, 0);
                ch.ChSelect.IsDisabled.Value = true;

                ch.InitializeViews();
                LevelLoadManager.Instance.AllChapters[i].LinkedChView = ch;

                //SubChapters
                SubChView sch = SubChaptersView.Instance.SubChaptersViewSwitcher.CreateView<SubChView>();
                sch.ChIndex = i;
                sch.MapSprite.Value = new SpriteAsset(
                    new UnityAsset("ChapterMap", LevelLoadManager.Instance.AllChapters[i].MapSpritePreLoaded));
                sch.InitializeViews();
                LevelLoadManager.Instance.AllChapters[i].LinkedSubChView = sch;


                for (int j = 1; j <= LevelLoadManager.Instance.AllChapters[i].Count; j++)
                {
                    var btn = sch.MapImage.CreateView<SubChSelectButton>();
                    btn.InitializeViews();
                    sch.SubChapters.Add(btn);
                    btn.Ranking.Value = j + ".";
                    btn.ContainerSubChView = sch;
                    btn.SubChIndex = j;
                    btn.Offset.Value = new ElementMargin(
                        LevelLoadManager.Instance.AllChapters[i][j].MapPosition.x,
                        LevelLoadManager.Instance.AllChapters[i][j].MapPosition.y);

                    btn.CheckBox.IsDisabled.Value = true;// !MiceCompanyOptions.AllChapters[i].SubChapters[j].Unlocked;

                    LevelLoadManager.Instance.AllChapters[i][j].LinkedButton = btn;
                }
            }

            // ilk Ch yi her halukarda aç
            /*MiceCompanyOptions.AllChapters[1].LinkedChView.ChSelect.IsDisabled.Value = false; 
            for (int i = 1; i <= MiceCompanyOptions.AllChapters.Count; i++)
            {
                // Her Ch nin ilk SubCh sini otomatikman aç
                MiceCompanyOptions.AllChapters[i][1].LinkedButton.CheckBox.IsDisabled.Value = false;
                MiceCompanyOptions.AllChapters[i][1].LinkedButton.ShowStars(0);
            }*/
            ChapterViewsInitialized = true;

            base.Awake();


        }
    }
}