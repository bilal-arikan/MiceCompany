﻿using Common.Managers;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace MiceCompany
{
    public class BuffTimeSpeed : MonoBehaviour
    {
        public float TimeMultiplierValue = 0.5f;
        public float DefaultTimeScale = 1;

        void Start()
        {
            DefaultTimeScale = Time.timeScale;
            GameManager.Instance.SetTimeScale(TimeMultiplierValue);
        }

        
        void OnDestroy()
        {
            GameManager.Instance.SetTimeScale(DefaultTimeScale);
        }
    }
}
