﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MiceCompany
{
    public class BuffLight : MonoBehaviour, IBuffEffect
    {
        public Light2D.LightSprite LightPrefab;

        void IBuffEffect.ApplyTo(Mice m)
        {
            Instantiate(LightPrefab,m.transform);
            m.GetComponent<Light2D.LightObstacleMesh>().enabled = false;
        }

        void IBuffEffect.UnApplyFrom(Mice m)
        {
            Destroy(m.GetComponentInChildren<Light2D.LightSprite>().gameObject);
            m.GetComponent<Light2D.LightObstacleMesh>().enabled = true;
        }
    }
}
