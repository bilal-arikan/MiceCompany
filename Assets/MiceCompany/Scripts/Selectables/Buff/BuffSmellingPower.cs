﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MiceCompany
{
    public class BuffSmellingPower : MonoBehaviour, IBuffEffect
    {
        public BuffEffectType EffectType;
        public float Value = 0;

        void IBuffEffect.ApplyTo(Mice m)
        {
            if (EffectType == BuffEffectType.DirectValue && Value >= 0)
                m.SmellingPower = Mathf.Clamp01(Value);
            else if(EffectType == BuffEffectType.Sum && Value != 0)
                m.SmellingPower = Mathf.Clamp01(m.SmellingPower + Value);
            else if(EffectType == BuffEffectType.Multiply && Value != 1)
                m.SmellingPower = Mathf.Clamp01(m.SmellingPower * Value);
        }

        void IBuffEffect.UnApplyFrom(Mice m)
        {
            if (EffectType == BuffEffectType.DirectValue && Value >= 0)
                m.SmellingPower = Common.Data.PreLoadedAssets.Instance.MousePrefabs.Find(toFind => toFind.Name == m.Name).SmellingPower;
            else if (EffectType == BuffEffectType.Sum && Value != 0)
                m.SmellingPower = Mathf.Clamp01(m.SmellingPower - Value);
            else if (EffectType == BuffEffectType.Multiply && Value != 1)
                m.SmellingPower = Value != 0 ?
                    Mathf.Clamp01(m.SmellingPower / Value) :
                    m.SmellingPower = Common.Data.PreLoadedAssets.Instance.MousePrefabs.Find(toFind => toFind.Name == m.Name).SmellingPower;
        }
    }
}
