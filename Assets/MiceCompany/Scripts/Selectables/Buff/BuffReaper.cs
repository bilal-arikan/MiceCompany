﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MiceCompany
{
    public class BuffReaper : MonoBehaviour, IBuffEffect
    {
        void IBuffEffect.ApplyTo(Mice m)
        {
            m.Death(DeathStyle.Blowed);
        }

        void IBuffEffect.UnApplyFrom(Mice m)
        {
            
        }
    }
}
