﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MiceCompany
{
    public class BuffHealth : MonoBehaviour, IBuffEffect
    {
        public DeathStyle DeathStyle = DeathStyle.Standart;
        public bool DeathIfZero = true;
        public int Value = 1;

        void IBuffEffect.ApplyTo(Mice m)
        {
            // Ölebiliyorsa yada son Health 0 dan büyükse
            if (DeathIfZero || m.Health + Value >= 1)
                m.AddToHealth(Value, DeathStyle);
        }

        void IBuffEffect.UnApplyFrom(Mice m)
        {

        }
    }
}
