﻿using Common;
using Common.Managers;
using MiceCompany;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Health)(DirectionFinder)(Interestings)(MoveSpeed)(RotateSpeed)
/// (SmellingPower)(MinThinkTime)(MaxThinkTime)(DecideSpeed)(TargetAngle)(rb2D)
/// (ListViewId)(ListViewCount)(IsSelectable)(IsRotatable)(IsRemovable)(IsMovable)
/// (IsAlive)(IsSelected)(FirstPosition)(FirstRotation)(FirstClickPosition)(PlacableZones)
/// (PlacableZoneColliders
/// </summary>
public class Buff : Item
{
    public enum EffectTime
    {
        InMoment,
        InPerion,
        Forever
    }

    public enum EffectSize
    {
        OneItem,
        OnArea,
        AllMap
    }



    /// <summary>
    /// time > 0  InPeriod
    /// time == 0 InMoment
    /// time < 0 Forever
    /// </summary>
    [Header("Buff Properties:")]
    public float Time = 0;
    /// <summary>
    /// 0 = Once
    /// > 0  every that seconds
    /// </summary>
    public float ApplyEffectPeriod = 0;
    /// <summary>
    /// </summary>
    public EffectTime effectTime
    {
        get
        {
            if (Time > 0)
                return EffectTime.InPerion;
            else if (Time == 0)
                return EffectTime.InMoment;
            else
                return EffectTime.Forever;

        }
    }

    public List<MiceTeam> TeamsTpApply = new List<MiceTeam>();

    public EffectSize AreaSize;
    public bool EffectOnlyInsideArea = false;
    public List<Mice> EffectedMices = new List<Mice>();
    public Color EffectOutlineColor;
    public int EffectOutlineColorSize = 10;
    public List<IBuffEffect> AllEffects = new List<IBuffEffect>();
    public Mice LinkedMouse;


    protected void Start()
    {
        AllEffects.Clear();
        var comps = GetComponents<MonoBehaviour>();
        foreach (var e in comps)
            if (e is IBuffEffect)
                AllEffects.Add(e as IBuffEffect);

        if (AreaSize == EffectSize.AllMap)
            foreach (PlayerMice m in FindObjectsOfType<PlayerMice>())
                ApplyEffects(m);

        if (AreaSize == EffectSize.OneItem && LinkedMouse != null)
        {
            ApplyEffects(LinkedMouse);
            LinkedMouse = null;
        }

        StartCoroutine(OnActiveCo());
        StartCoroutine(OnDestroyCo());
    }

    void LateUpdate()
    {
        if (LinkedMouse != null)
            transform.position = LinkedMouse.transform.position;
    }

    protected override void OnDestroy()
    {
        //base.OnDestroy();

        foreach (var m in EffectedMices)
            UnApplyEffects(m);
        EffectedMices.Clear();
    }

    /// <summary>
    /// </summary>
    public static Buff Use(Buff b)
    {
        Buff newB = Instantiate(b, GameManager.Instance.BuffRoot);
        //newB.EffectedMices = GameManager.Instance.AllItemsInTheMap.FindAll(i => i is Mouse).ConvertAll(i => i as Mouse);
        return newB;
    }

    /// <summary>
    /// </summary>
    public static Buff Use(Buff b, Vector2 position)
    {
        Buff newB = Instantiate(b,position,new Quaternion(), GameManager.Instance.BuffRoot);
        return newB;
    }

    /// <summary>
    /// </summary>
    public static Buff Use(Buff b, Mice item)
    {
        Buff newB = Instantiate(b, GameManager.Instance.BuffRoot);
        newB.LinkedMouse = item;
        return newB;
    }

    void ApplyEffects(Mice m)
    {
        if (EffectedMices.Contains(m))
            return;

        // Belirli Takımlara etki etmesi gerekiyosa ve Fare O takımdan deilse etki etme
        if (TeamsTpApply.Count != 0 && !TeamsTpApply.Contains(m.Team))
            return;

        if (EffectOutlineColor != Color.black && AllEffects.Count > 0)
            m.SetOutlined(true, EffectOutlineColor, EffectOutlineColorSize);

        AllEffects.ForEach((effect) =>
        {
            effect.ApplyTo(m);
        });

        EffectedMices.Add(m);
    }

    void UnApplyEffects(Mice m)
    {
        if (m == null)
        {
            return;
        }

        if (AllEffects.Count > 0)
            m.SetOutlined(false, EffectOutlineColor);

        AllEffects.ForEach((effect) =>
        {
            effect.UnApplyFrom(m);
        });

    }


    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        if(AreaSize == EffectSize.OnArea)
        {
            Mice m = other.gameObject.GetCachedComponent<Mice>();
            if (m != null)
            {
                ApplyEffects(m);
            }
        }
    }

    protected virtual void OnTriggerExit2D(Collider2D other)
    {
        if(EffectOnlyInsideArea)
        {
            Mice m = other.gameObject.GetCachedComponent<Mice>();
            if (m != null)
                UnApplyEffects(m);
            EffectedMices.Remove(m);
        }
    }

    /// <summary>
    /// Buff aktif olduğu müddetçe her Periodda efektler uygulanır
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator OnActiveCo()
    {
        do
        {
            foreach (var m in EffectedMices)
                ApplyEffects(m);

            //Debug.Log("Buff Applied " + GetTemplateId() + EffectedMices.Count);


            yield return new WaitForSeconds(ApplyEffectPeriod);
        }
        while (effectTime != EffectTime.InMoment && ApplyEffectPeriod > 0);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    protected IEnumerator OnDestroyCo()
    {

        if (Time >= 0)
            yield return new WaitForSeconds(Time);
        else
            yield return new WaitWhile(() =>
                GameManager.Instance.Status == GameManager.GameStatus.GameInProgress ||
                GameManager.Instance.Status == GameManager.GameStatus.Paused);

        Destroy(gameObject);
    }


}

