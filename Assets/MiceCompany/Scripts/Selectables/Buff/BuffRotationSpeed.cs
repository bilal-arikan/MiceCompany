﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MiceCompany
{
    public class BuffRotationSpeed : MonoBehaviour, IBuffEffect
    {
        public BuffEffectType EffectType;
        public float Value = 0;

        void IBuffEffect.ApplyTo(Mice m)
        {
            if (EffectType == BuffEffectType.DirectValue && Value >= 0)
                m.RotateSpeed = Value;
            else if(EffectType == BuffEffectType.Sum && Value != 0)
                m.RotateSpeed = Mathf.Clamp(m.RotateSpeed + Value, 0,10000);
            else if(EffectType == BuffEffectType.Multiply && Value != 1)
                m.RotateSpeed = Mathf.Clamp(m.RotateSpeed * Value, 0, 10000);
        }

        void IBuffEffect.UnApplyFrom(Mice m)
        {
            if (EffectType == BuffEffectType.DirectValue && Value >= 0)
                m.RotateSpeed = Common.Data.PreLoadedAssets.Instance.MousePrefabs.Find(toFind => toFind.Name == m.Name).RotateSpeed;
            else if (EffectType == BuffEffectType.Sum && Value != 0)
                m.RotateSpeed = Mathf.Clamp(m.RotateSpeed - Value, 0, 10000);
            else if (EffectType == BuffEffectType.Multiply && Value != 1)
                m.RotateSpeed = Value != 0 ?
                    Mathf.Clamp(m.RotateSpeed / Value, 0, 10000) :
                    m.RotateSpeed = Common.Data.PreLoadedAssets.Instance.MousePrefabs.Find(toFind => toFind.Name == m.Name).RotateSpeed;
        }
    }
}
