﻿using Common.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MiceCompany
{
    public class BuffDestroyItem : MonoBehaviour
    {
        public List<string> ToDestroyObjectNames = new List<string>();
        public AudioClip DestroyingObjectAudio;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            //Debug.Log("T"+collision.gameObject.name);
            if(ToDestroyObjectNames.Count > 0 && ToDestroyObjectNames.Exists( name => collision.gameObject.name.StartsWith(name) ))
            {
                if (DestroyingObjectAudio != null)
                    SoundManager.Instance.PlaySound(DestroyingObjectAudio, collision.transform.position);

                collision.gameObject.Destroy();
            }
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            //Debug.Log("C"+collision.gameObject.name);
            if (ToDestroyObjectNames.Count > 0 && ToDestroyObjectNames.Exists(name => collision.gameObject.name.StartsWith(name)))
            {
                if (DestroyingObjectAudio != null)
                    SoundManager.Instance.PlaySound(DestroyingObjectAudio, collision.transform.position);

                collision.gameObject.Destroy();
            }
        }
    }
}
