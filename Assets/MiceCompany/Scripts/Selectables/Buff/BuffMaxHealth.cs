﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MiceCompany
{
    public class BuffMaxHealth : MonoBehaviour, IBuffEffect
    {
        public int Value = 1;

        void IBuffEffect.ApplyTo(Mice m)
        {
            m.MaxHealth = Mathf.Clamp(m.MaxHealth + Value, 1, 10000);

            if (Value > 0)
                m.AddToHealth(Value);
            // Max health < Health olursa
            else if (Value < 0 && m.Health > m.MaxHealth)
                m.AddToHealth(m.MaxHealth - m.Health);

        }

        void IBuffEffect.UnApplyFrom(Mice m)
        {
            m.MaxHealth = Mathf.Clamp(m.MaxHealth - Value, 1, 10000);

            // Max health < Health olursa
            if (Value > 0 && m.Health > m.MaxHealth)
                m.AddToHealth(m.MaxHealth - m.Health);
        }
    }
}
