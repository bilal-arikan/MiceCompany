﻿using Common.PathFinding;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace MiceCompany
{
    public class BuffFakeCheese : MonoBehaviour
    {
        public GameObject DefaultPathFinderTarget;

        private void Start()
        {
            DefaultPathFinderTarget = PathFinder.Instance.TargetObject;
            PathFinder.Instance.TargetObject = gameObject;
            PathFinder.Instance.CalculateAllPathsToTarget();
        }

        private void OnDestroy()
        {
            PathFinder.Instance.TargetObject = DefaultPathFinderTarget;
            PathFinder.Instance.CalculateAllPathsToTarget();
        }
    }
}
