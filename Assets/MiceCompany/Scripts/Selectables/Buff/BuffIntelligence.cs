﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MiceCompany
{
    public class BuffIntelligence : MonoBehaviour, IBuffEffect
    {
        public float MinThinkTime = 1;
        public float MaxThinkTime = 2;

        void IBuffEffect.ApplyTo(Mice m)
        {
            m.MinThinkTime = MinThinkTime;
            m.MaxThinkTime = MaxThinkTime;
        }

        void IBuffEffect.UnApplyFrom(Mice m)
        {
            var def = Common.Data.PreLoadedAssets.Instance.MousePrefabs.Find(toFind => toFind.Name == m.Name);
            m.MinThinkTime = def.MinThinkTime;
            m.MaxThinkTime = def.MaxThinkTime;
        }
    }
}
