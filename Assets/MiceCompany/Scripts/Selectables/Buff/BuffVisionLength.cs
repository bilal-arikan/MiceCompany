﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MiceCompany
{
    public class BuffVisionLength : MonoBehaviour, IBuffEffect
    {
        public BuffEffectType EffectType;
        public float Value = 0;

        void IBuffEffect.ApplyTo(Mice m)
        {
            if (EffectType == BuffEffectType.DirectValue && Value >= 0)
                m.VisionLength = Value;
            else if(EffectType == BuffEffectType.Sum && Value != 0)
                m.VisionLength = Mathf.Clamp(m.VisionLength + Value, 0,100);
            else if(EffectType == BuffEffectType.Multiply && Value != 1)
                m.VisionLength = Mathf.Clamp(m.VisionLength * Value, 0, 100);
        }

        void IBuffEffect.UnApplyFrom(Mice m)
        {
            if (EffectType == BuffEffectType.DirectValue && Value >= 0)
                m.VisionLength = Common.Data.PreLoadedAssets.Instance.MousePrefabs.Find(toFind => toFind.Name == m.Name).VisionLength;
            else if (EffectType == BuffEffectType.Sum && Value != 0)
                m.VisionLength = Mathf.Clamp(m.VisionLength - Value, 0, 100);
            else if (EffectType == BuffEffectType.Multiply && Value != 1)
                m.VisionLength = Value != 0 ?
                    Mathf.Clamp(m.VisionLength / Value, 0, 100) :
                    m.VisionLength = Common.Data.PreLoadedAssets.Instance.MousePrefabs.Find(toFind => toFind.Name == m.Name).VisionLength;
        }
    }
}
