﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiceCompany
{
    public enum BuffEffectType
    {
        DirectValue,
        Sum,
        Multiply
    }

    public interface IBuffEffect
    {
        void ApplyTo(Mice item);
             
        void UnApplyFrom(Mice item);
    }
}
