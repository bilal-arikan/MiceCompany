﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MiceCompany
{
    public class BuffMoveSpeed : MonoBehaviour, IBuffEffect
    {
        public BuffEffectType EffectType;
        public float Value = 0;

        void IBuffEffect.ApplyTo(Mice m)
        {
            //Debug.Log("MoveSpeed applied to " + m.name);

            if (EffectType == BuffEffectType.DirectValue && Value >= 0)
                m.MoveSpeed = Value;
            else if(EffectType == BuffEffectType.Sum && Value != 0)
                m.MoveSpeed = Mathf.Clamp(m.MoveSpeed + Value, 0,10000);
            else if(EffectType == BuffEffectType.Multiply && Value != 1)
                m.MoveSpeed = Mathf.Clamp(m.MoveSpeed * Value, 0, 10000);
        }

        void IBuffEffect.UnApplyFrom(Mice m)
        {
            //Debug.Log("MoveSpeed Unapplied to " + m.name);

            if (EffectType == BuffEffectType.DirectValue && Value >= 0)
                m.MoveSpeed = Common.Data.PreLoadedAssets.Instance.MousePrefabs.Find(toFind => toFind.Name == m.Name).MoveSpeed;
            else if (EffectType == BuffEffectType.Sum && Value != 0)
                m.MoveSpeed = Mathf.Clamp(m.MoveSpeed - Value, 0, 10000);
            else if (EffectType == BuffEffectType.Multiply && Value != 1)
                m.MoveSpeed = Value != 0 ?
                    Mathf.Clamp(m.MoveSpeed / Value, 0, 10000) :
                    m.MoveSpeed = Common.Data.PreLoadedAssets.Instance.MousePrefabs.Find(toFind => toFind.Name == m.Name).MoveSpeed;
        }
    }
}
