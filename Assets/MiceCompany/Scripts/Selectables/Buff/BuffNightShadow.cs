﻿using Common.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MiceCompany
{
    public class BuffNightShadow : MonoBehaviour
    {
        [SerializeField]
        bool nightModeOnDefault = true;
        [SerializeField]
        float nightModeLumenDefault = 0.5f;

        void Start()
        {
            nightModeOnDefault = GameManager.Instance.IsNightMode;
            nightModeLumenDefault = GameManager.Instance.LightCamera.backgroundColor.r;

            GameManager.Instance.StartCoroutine(GettingDark());
        }

        void OnDestroy()
        {
            GameManager.Instance.StartCoroutine(GettingLight());
        }

        IEnumerator GettingLight()
        {
            for (float i = 0; i <= 1.01f; i += 0.05f)
            {
                yield return new WaitForSeconds(0.05f);
                if (nightModeOnDefault)
                {
                    GameManager.Instance.SetLightSystem(true, Mathf.Lerp(nightModeLumenDefault, 0, i));
                    //Debug.Log("L" + GameManager.Instance.IsNightMode + GameManager.Instance.LightCamera.backgroundColor.r.ToString());
                }
            }

            if(!nightModeOnDefault)
                GameManager.Instance.SetLightSystem(false);
        }

        IEnumerator GettingDark()
        {
            for (float i = 0; i <= 1.01f; i += 0.05f)
            {

                    GameManager.Instance.SetLightSystem(true, Mathf.Lerp(0, nightModeLumenDefault, i));
                    //Debug.Log("D" + GameManager.Instance.IsNightMode + GameManager.Instance.LightCamera.backgroundColor.r.ToString());
                yield return new WaitForSeconds(0.05f);
            }

            GameManager.Instance.SetLightSystem(true);
        }
    }
}
