﻿using Common.PathFinding;
using Common.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MiceCompany
{
    public class PlayerMiceMovementController : MiceMovementController
    {
        public float MoveSpeedInterestingMultiplier = 1.2f;
        public float MoveSpeedNoCheeseMultiplier = 0.5f;
        public float MoveSpeedWoundedMultiplier = 0.7f;



        public override void DecideRotation()
        {
            var makesBlind = LinkedMice.AroundInterestings.FindAll(i => i.MakesBlind);
            // Kör edici varsa
            if (makesBlind.Count > 0)
            {
                isFollowingCheese = false;
                // Kör edici derece dikkat çeken olduğunda sadece kör edicilere olan ortak yön alınır
                Vector2 AllInterestingVectors = Common.Tools.Math.Vector3Sum(makesBlind.ConvertAll(e =>
                           (e.transform.position - transform.position) * e.Power
                        ).ToArray()).normalized;

                // Smelling power a göre daha keskşn bir yön belirliyor
                Angle ToCheese = Angle.Random(
                    (Angle)AllInterestingVectors - (LinkedMice.SmellingPower - 1) * 114, //114 = 360 / PI
                    (Angle)AllInterestingVectors + (LinkedMice.SmellingPower - 1) * 114);

                TargetAngle = ToCheese;
            }
            // Kör edici yok ama Interesting varsa
            else
            {
                // Kör edici derece dikkat çeken olmadığında peynir olan yol ile interestingsin ortalaması alınır
                Vector2 AllInterestingVectors = Common.Tools.Math.Vector3Sum(LinkedMice.AroundInterestings.ConvertAll(e =>
                       (e.transform.position - transform.position) * e.Power
                    ).ToArray()).normalized;

                var ToCheeseWay = PathFinder.Instance.FindDirectionToTarget(transform.position);
                isFollowingCheese = ToCheeseWay != null;
                // Peynirin kokusunu alabiliyosa
                if (ToCheeseWay != null)
                {
                    // Interestinglerle Peynirin yönünü birleştiriyor
                    Vector2 SumOfTwo = Common.Tools.Math.Vector3Sum(AllInterestingVectors, (Vector2)(Angle)Directions.Angles[ToCheeseWay.Value]).normalized;

                    // Smelling power a göre daha keskşn bir yön belirliyor
                    Angle ToCheese = Angle.Random(
                        (Angle)SumOfTwo - (LinkedMice.SmellingPower - 1) * 114, //114 = 360 / PI
                        (Angle)SumOfTwo + (LinkedMice.SmellingPower - 1) * 114);

                    TargetAngle = ToCheese;

                    //Debug.Log("=>" + AllEffectivesVector + ":" + ToCheese + ":" + sumoftwo+":"+ ToCheeseWay.Value +":"+ TargetAngle );
                }
                // En az 1 tane interestign varsa
                else if(AllInterestingVectors != Vector2.zero)
                {
                    TargetAngle = AllInterestingVectors;
                }
                // Ne peynir nede interesting yoksa
                else
                {
                    TargetAngle = Angle.Random();
                }
            }
        }

        public override void DecideSpeed()
        {
            // Fare yaralıysa
            if (LinkedMice.Health == 1 && LinkedMice.MaxHealth != 1)
            {
                CurrentMoveSpeed = LinkedMice.MoveSpeed * MoveSpeedWoundedMultiplier;
                return;
            }

            // Kör edici Interesting varsa
            if (LinkedMice.AroundInterestings.Count > 0 && LinkedMice.AroundInterestings.Find(i => i.MakesBlind) != null)
            {
                CurrentMoveSpeed = LinkedMice.MoveSpeed * MoveSpeedInterestingMultiplier;
            }
            // Kör edici Interesting yok ama Interesting varsa
            else if (LinkedMice.AroundInterestings.Count > 0)
            {
                CurrentMoveSpeed = LinkedMice.MoveSpeed;
            }
            // Interesting yoksa
            else
            {
                var ToCheeseWay = PathFinder.Instance.FindDirectionToTarget(transform.position);
                if (ToCheeseWay != null)
                {
                    CurrentMoveSpeed = LinkedMice.MoveSpeed;
                }
                else
                {
                    CurrentMoveSpeed = LinkedMice.MoveSpeed * MoveSpeedNoCheeseMultiplier;
                }
            }
        }

    }
}
