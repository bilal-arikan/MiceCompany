﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common.Tools;
using Common.PathFinding;
using Common.Managers;
using MiceCompany;
using Common.Data;

namespace MiceCompany
{
    public enum DeathStyle
    {
        Standart,
        Blowed,
        Drowned,
        Poisoned,
        UnVisible
    }

    public enum MiceState
    {
        Normal,
        Confused,
        FallInLove,
        Scared,
        SearchCheese,
        SawGhost,
    }

    public enum MiceTeam
    {
        DarkSide = -1,
        Zombie = 0,
        Team1 = 1,
        Team2 = 2,
        Team3 = 3,
        Team4 = 4
    }
}

//[RequireComponent(typeof(Rigidbody2D),typeof(CircleCollider2D),typeof(SpriteRenderer))]
public class Mice : Item {

    [Header("Mice Properties:")]

    public MiceTeam Team  = MiceTeam.Team1;
    public List<MiceTeam> FriendTeams = new List<MiceTeam>();
    public List<MiceTeam> EnemyTeams = new List<MiceTeam>() { 
        MiceTeam.DarkSide,
        MiceTeam.Zombie
    };
    [Space(20)]
    [NonSerialized]
    public int MaxHealth = 10;
    public int Health = 3;

    public float MoveSpeed = 20;
    public float RotateSpeed = 4f;
    [Range(0, 1)]
    public float SmellingPower = 0.5f;
    public float MinThinkTime = 2;
    public float MaxThinkTime = 5;
    //[Range(0,1)]
    //public float DecideSpeed = 0.5f;
    public float VisionLength
    {
        get
        {
            if (coll == null)
                return 0;
            else
                return coll.radius;
        }
        set
        {
            if(coll != null)
                coll.radius = value;
        }
    }
    [Space(20)]
    public int PlayMiceSoundMod = 6;
    public int ShowMiceStateMod = 12;
    public bool IsThinking = false;
    public float LastDamageTime;
    [Space(20)]
    public List<Interesting> AroundInterestings = new List<Interesting>();
    public List<Damaging> AroundDamagings = new List<Damaging>();
    public List<Mice> AroundMices = new List<Mice>();

    [NonSerialized]
    public MiceMovementController MovementController;
    [NonSerialized]
    public MiceSenseController SenseController;
    [NonSerialized]
    public HealthBarController HealthBarController;
    [NonSerialized]
    public Rigidbody2D Rigid2D;

    protected SpriteRenderer rend;
    protected Selectable selectable;
    protected AudioSource audi;
    protected CircleCollider2D coll;
    protected int brainFunctionCount = 1;
    protected float NextThinkTime;

    // Use this for initialization
    protected virtual void Start()
    {
        rend = GetComponent<SpriteRenderer>();
        selectable = GetComponent<Selectable>();
        audi = GetComponent<AudioSource>();
        coll = GetComponent<CircleCollider2D>();
        Rigid2D = GetComponent<Rigidbody2D>();
        MovementController = GetComponent<MiceMovementController>();
        SenseController = GetComponent<MiceSenseController>();
        HealthBarController = GetComponentInChildren<HealthBarController>();

        EventManager.TriggerObjectEvent(E.MiceBorn, this);

        HealthBarController.SetHealth(Health);

        NextThinkTime = Time.timeSinceLevelLoad;

        StartCoroutine(ShowEffects());
    }

    protected virtual void Update()
    {
        // Belirli Aralıklarla Düşünüyor ve Düşünmüyor
        if (Time.timeSinceLevelLoad > NextThinkTime)
        {
            BrainFunctions();
        }

    }


    protected IEnumerator ShowEffects()
    {
        while (true)
        {
            if (rend.color != Color.white)
                rend.color = new Color(
                    Mathf.Clamp01(rend.color.r + 0.1f),
                    Mathf.Clamp01(rend.color.g + 0.1f),
                    Mathf.Clamp01(rend.color.b + 0.1f),
                    rend.color.a);


            yield return new WaitForSeconds(0.2f);
        }
    }

    protected void BrainFunctions()
    {
        // Fare Sesi çıkar
        if (audi != null && brainFunctionCount % PlayMiceSoundMod == 0)
        {
            audi.Play();
        }

        IsThinking = !IsThinking;

        brainFunctionCount++;
        if (brainFunctionCount > 100)
            brainFunctionCount = 1;

        // Düşünüyorsa Bekleme süresinin yarısını bekle
        NextThinkTime += 
            !IsThinking ? 
            UnityEngine.Random.Range(MinThinkTime, MaxThinkTime) * 2:
            UnityEngine.Random.Range(MinThinkTime, MaxThinkTime) / 2;
        
    }


    /// <summary>
    /// Returns is Alive
    /// </summary>
    /// <returns></returns>
    public virtual bool AddToHealth(int value, DeathStyle style = DeathStyle.Standart)
    {
        Health += value;
        Health = Mathf.Clamp(Health, 0, MaxHealth);

        if (value < 0)
        {
            LastDamageTime = Time.timeSinceLevelLoad;

            if (audi != null)
                audi.Play();
            if (MovementController != null)
                MovementController.NewRotation = true;

            // Hasar Alırsa Durup Düşünür sonra hareket eder
            IsThinking = true;
            NextThinkTime = Time.timeSinceLevelLoad + UnityEngine.Random.Range(MinThinkTime, MaxThinkTime);
        }

        if (Health <= 0)
        {
            Death(style);
        }
        else
        {
            HealthBarController.SetHealth(Health);
        }

        if (Health > 0 && value < 0)
            rend.color = Color.gray;
        if(Health == 1)
            SetOutlined(true, Color.red, 15);

        return Health > 0;
    }


    //-------------------- ITEM ---------------------------------------
    public virtual void Death(DeathStyle style = DeathStyle.Standart)
    {
        if(style== DeathStyle.Drowned)
            Instantiate(PreLoadedAssets.DeathPrefabDrowned, transform.position, new Quaternion(),GameManager.Instance.MiceRoot);
        else if (style == DeathStyle.Blowed)
            Instantiate(PreLoadedAssets.DeathPrefabBlowed, transform.position, new Quaternion(), GameManager.Instance.MiceRoot);
        else if (style == DeathStyle.Poisoned)
            Instantiate(PreLoadedAssets.DeathPrefabStandart, transform.position, transform.rotation, GameManager.Instance.MiceRoot);
        else if(style == DeathStyle.Standart)
            Instantiate(PreLoadedAssets.DeathPrefabStandart, transform.position, transform.rotation, GameManager.Instance.MiceRoot);

        Destroy(gameObject);
        EventManager.TriggerObjectEvent(E.MiceDeath, PreLoadedAssets.Instance.MousePrefabs.Find(m => m.Name == Name));
    }


}
