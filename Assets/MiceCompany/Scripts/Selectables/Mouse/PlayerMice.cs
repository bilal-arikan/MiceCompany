﻿using Common.Data;
using Common.Managers;
using MiceCompany;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class PlayerMice : Mice
{
    protected override void Start()
    {
        base.Start();
    }


    //-------------------- MICE ---------------------------------------
    public void Arrive()
    {
        GameManager.Instance.PlayerMicesInTheMap.Remove(this);
        Destroy(gameObject);
        EventManager.TriggerObjectEvent(E.MiceArrived, PreLoadedAssets.Instance.MousePrefabs.Find(m => m.Name == Name));
    }

    protected override void OnDestroy()
    {
        GameManager.Instance.PlayerMicesInTheMap.Remove(this);
        base.OnDestroy();
    }

    //-------------------- ITEM ---------------------------------------
    public override void Death(DeathStyle style = DeathStyle.Standart)
    {
        GameManager.Instance.PlayerMicesInTheMap.Remove(this);
        base.Death(style);
    }

}
