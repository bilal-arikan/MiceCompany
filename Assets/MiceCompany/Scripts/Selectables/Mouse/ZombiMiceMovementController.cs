﻿using Common.PathFinding;
using Common.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MiceCompany
{
    public class ZombiMiceMovementController : MiceMovementController
    {
        public PlayerMice HuntingMice;
        public bool CanStayAwayDamagings = true;

        public float MoveSpeedSeePlayerMiceMultiplier = 1.5f;
        public float MoveSpeedWoundedMultiplier = 0.7f;


        public override void Decide()
        {
            DetectNearestPlayerMice();
            base.Decide();
        }

        public PlayerMice DetectNearestPlayerMice()
        {
            // Döngü esnasında AroundObjectse yeni Objeler eklenebiliyor
            Mice[] aroundCopy = new Mice[LinkedMice.AroundMices.Count];
            LinkedMice.AroundMices.CopyTo(aroundCopy);

            var mices = aroundCopy.FindAll(m => m != null && m.Team > 0);

            Transform t = transform.Closest(mices.ConvertAll(m => m.transform));

            if (t != null)
                HuntingMice = t.gameObject.GetCachedComponent<PlayerMice>();

            return HuntingMice;
        }

        public override void DecideRotation()
        {
            // Görünürde PlayerMice varsa körlemesine onu hedefle
            if (HuntingMice != null)
            {
                TargetAngle = (Vector2)(HuntingMice.transform.position - transform.position).normalized;
            }
            // PlayerMice yoksa
            // Damaginglerden uzak duracak şekilde yön belirle
            else if(CanStayAwayDamagings && LinkedMice.AroundDamagings.Count > 0)
            {
                // Kör edici derece dikkat çeken olduğunda sadece kör edicilere olan ortak yön alınır
                Vector2 AllEffectivesVector = Common.Tools.Math.Vector3Sum(LinkedMice.AroundDamagings.ConvertAll(e =>
                    // Damaginglerin aksi yönde ilerler       
                    -(e.transform.position - transform.position)
                    ).ToArray()).normalized;
                TargetAngle = AllEffectivesVector;
            }
            else
            {
                TargetAngle = Angle.Random();
            }
        }

        public override void DecideSpeed()
        {
            // Fare yaralıysa
            if (LinkedMice.Health == 1 && LinkedMice.MaxHealth != 1)
            {
                CurrentMoveSpeed = LinkedMice.MoveSpeed * MoveSpeedWoundedMultiplier;
            }
            // PlayerMice varsa
            else if (HuntingMice != null)
            {
                CurrentMoveSpeed = LinkedMice.MoveSpeed * MoveSpeedSeePlayerMiceMultiplier;
            }
            else
            {
                CurrentMoveSpeed = LinkedMice.MoveSpeed;
            }
        }

    }
}
