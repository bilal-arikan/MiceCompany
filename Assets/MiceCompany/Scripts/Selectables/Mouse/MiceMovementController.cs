﻿using Common.PathFinding;
using Common.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MiceCompany
{
    public abstract class MiceMovementController : MonoBehaviour
    {
        protected Mice LinkedMice;
        protected Rigidbody2D rb2D;

        public Angle TargetAngle;
        public Angle CurrentAngle
        {
            get
            {
                return transform.rotation.eulerAngles.z;
            }
            set
            {
                transform.rotation = Quaternion.Euler(0, 0, value);
            }
        }

        public float CurrentMoveSpeed = 20;
        public bool isFollowingCheese = false;

        public bool NewRotation = false;
        public bool NewSpeed = false;

        public float CoStepSeconds = 0.5f;
        [NonSerialized]
        public float NextCallTime;

        protected virtual void Awake()
        {
            rb2D = GetComponent<Rigidbody2D>();
            LinkedMice = GetComponent<Mice>();

            TargetAngle = CurrentAngle;
            CurrentMoveSpeed = LinkedMice.MoveSpeed;
        }

        protected virtual void OnEnable()
        {
            NextCallTime = Time.timeSinceLevelLoad;
        }

        private void Update()
        {
            // Belli aralıklarla Hız ve Rotayı Hesaplar
            // yada NewRotation yada NewSpeed i true yaparak o framde girer
            // örnek: Hasar aldığında
            if (NewRotation || NewSpeed || Time.timeSinceLevelLoad > NextCallTime)
            {
                Decide();

                NewRotation = false;
                NewSpeed = false;
                NextCallTime += CoStepSeconds;
            }
        }

        public void FixedUpdate()
        {
            // TargetAngle a Lerp ile yaklaşıyor
            Angle n = Angle.Lerp(transform.rotation.eulerAngles.z, TargetAngle, LinkedMice.RotateSpeed * Time.fixedDeltaTime);
            transform.rotation = Quaternion.Euler(0, 0, n);
            Vector2 posDelta = (Angle)CurrentAngle;
            
            // Dönmeyi Herzaman İlerlemeyi düşünmüyorken yapar
            if (!LinkedMice.IsThinking)
            {
                // Hareket ediyor
                rb2D.MovePosition((Vector2)transform.position + posDelta * Time.fixedDeltaTime * CurrentMoveSpeed);
            }
        }

        public virtual void Decide()
        {
            if (NewRotation && !NewSpeed)
            {
                DecideRotation();
            }
            else if (NewSpeed && !NewRotation)
            {
                DecideSpeed();
            }
            else
            {
                DecideRotation();
                DecideSpeed();
            }

        }


        public abstract void DecideSpeed();

        public abstract void DecideRotation();


        private void OnDrawGizmos()
        {
            if(LinkedMice != null)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawLine(transform.position, transform.position + ((Vector3)(Vector2)TargetAngle).normalized * LinkedMice.VisionLength);
            }
        }

    }
}
