﻿using Common.PathFinding;
using Common.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MiceCompany
{
    public class MiceSenseController : MonoBehaviour
    {
        private Mice LinkedMice;
        private CircleCollider2D MiceCollider;
        private float MiceColliderOutsideMagnitude = 0;

        public List<GameObject> AroundObjects = new List<GameObject>();
        public LayerMask LookRayLayerMask;

        public bool DetectInterestings = true;
        public bool DetectDamagings = true;
        public bool DetectMices = true;

        public float CoStepSeconds = 0.1f;
        [NonSerialized]
        public float NextCallTime;

        private void Awake()
        {
            LinkedMice = GetComponent<Mice>();
            MiceCollider = GetComponent<CircleCollider2D>();

            MiceColliderOutsideMagnitude = MiceCollider.bounds.size.magnitude/2 + 0.001f;
        }

        protected virtual void OnEnable()
        {
            NextCallTime = Time.timeSinceLevelLoad;
        }

        private void Update()
        {
            // Belli aralıklarla Görünür nesneleri Hesaplar
            if (Time.timeSinceLevelLoad > NextCallTime)
            {
                DetectVisibleObjects();

                NextCallTime += CoStepSeconds;
            }
        }

        public void DetectVisibleObjects()
        {
            // Döngü esnasında AroundObjectse yeni Objeler eklenebiliyor
            GameObject[] aroundCopy = new GameObject[AroundObjects.Count];
            AroundObjects.CopyTo(aroundCopy);

            LinkedMice.AroundInterestings.Clear();
            LinkedMice.AroundDamagings.Clear();
            LinkedMice.AroundMices.Clear();
            foreach (var obj in aroundCopy)
            {
                if (obj == null)
                {
                    AroundObjects.Remove(obj);
                    continue;
                }

                // Göremiyorsa sıradakine geç
                if (!MiceCollider.CanSee(obj, MiceColliderOutsideMagnitude, LookRayLayerMask.value))
                {
                    //Debug.Log(LinkedMice.name + " CanNotSee " + obj.name);
                    continue;
                }

                //görebiliuorsa Componentlere bak
                if (DetectInterestings)
                {
                    Interesting interest = obj.GetCachedComponent<Interesting>();
                    if (interest != null)
                    {
                        LinkedMice.AroundInterestings.Add(interest);
                    }
                }
                if (DetectDamagings)
                {
                    Damaging damage = obj.GetCachedComponent<Damaging>();
                    if (damage != null)
                    {
                        LinkedMice.AroundDamagings.Add(damage);
                    }
                }
                if (DetectMices)
                {
                    Mice mice = obj.GetCachedComponent<Mice>();
                    if (mice != null)
                    {
                        LinkedMice.AroundMices.Add(mice);
                    }
                }
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            //Debug.Log("Sense1 "+collision.gameObject.name);
            if(!AroundObjects.Contains(collision.gameObject))
                AroundObjects.Add(collision.gameObject);
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            //Debug.Log("Sense2 " + collision.gameObject.name);
            AroundObjects.Remove(collision.gameObject);
            LinkedMice.AroundInterestings.RemoveAll(i => i.gameObject == collision.gameObject);
            LinkedMice.AroundDamagings.RemoveAll(i => i.gameObject == collision.gameObject);
            LinkedMice.AroundMices.RemoveAll(i => i.gameObject == collision.gameObject);
        }

        private void OnDrawGizmos()
        {
            if (LinkedMice == null)
                return;

            Gizmos.color = CColor.yellow;
            foreach (var e in LinkedMice.AroundInterestings)
                if (e != null)
                    Gizmos.DrawLine(transform.position + (e.transform.position - transform.position).normalized * MiceColliderOutsideMagnitude, e.transform.position);
            Gizmos.color = CColor.red;
            foreach (var e in LinkedMice.AroundDamagings)
                if (e != null)
                    Gizmos.DrawLine(transform.position + (e.transform.position - transform.position).normalized * MiceColliderOutsideMagnitude, e.transform.position);

            //RayTracin başlayacağı dış çeper
            /*if (MiceColliderOutsideMagnitude > 0)
                    Gizmos.DrawWireSphere(transform.position, MiceColliderOutsideMagnitude);*/
        }
    }
}
