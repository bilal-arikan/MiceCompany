﻿using Common.Data;
using Common.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MiceCompany
{
    public enum MiceFeeling
    {
        Normal,
        FallInLove,
        TakeDamage,
        Scared,
        ShowEnemy,
        FollowingCheese,
        Confused,
    }

    public class MiceFeelingController : MonoBehaviour
    {
        protected Mice LinkedMice;

        //public MiceFeeling Currentfeeling = MiceFeeling.Normal;

        public float MinCoStepSeconds = 4f;
        public float MaxCoStepSeconds = 6f;
        [NonSerialized]
        public float NextCallTime;

        public bool FallInLove = false;
        public bool TakeDamage = false;
        public bool Scared = false;
        public bool ShowEnemy = false;
        public bool FollowingCheese = false;
        public bool Confused = false;

        private void Awake()
        {
            LinkedMice = GetComponent<Mice>();
        }

        private void OnEnable()
        {
            NextCallTime = Time.timeSinceLevelLoad;
        }

        private void Update()
        {
            // Belli aralıklarla Hisleri gösterir
            if (Time.timeSinceLevelLoad > NextCallTime)
            {
                DetectFeeling();
                ShowFeeling();

                NextCallTime += UnityEngine.Random.Range(MinCoStepSeconds,MaxCoStepSeconds);
            }
        }

        public void DetectFeeling()
        {
            // En son aldığında bu yana 5 saniyeden fazla geçmediyse
            TakeDamage = LinkedMice.LastDamageTime > 0 && Time.timeSinceLevelLoad - LinkedMice.LastDamageTime < 5;
            // Işık değeri çok düşükse
            Scared = GameManager.Instance.LightValue < 0.1f;
            // Gördüğü farelerden en az 1 inin takımı düşman takımdansa
            ShowEnemy = LinkedMice.AroundMices.Count > 0 &&
                LinkedMice.AroundMices.Exists(m => LinkedMice.EnemyTeams.Contains(m.Team));
            FollowingCheese = LinkedMice.MovementController.isFollowingCheese;
            // Peynir kokusu yoksa ve Interesting yoksa
            Confused = !LinkedMice.MovementController.isFollowingCheese &&
                (LinkedMice.SenseController.DetectInterestings && LinkedMice.AroundInterestings.Count > 0);
        }

        public void ShowFeeling()
        {
            if(FallInLove)
            {
                Instantiate(PreLoadedAssets.Instance.Feelings[0], transform);
            }
            else if (TakeDamage)
            {
                Instantiate(PreLoadedAssets.Instance.Feelings[1], transform);
            }
            else if (Scared)
            {
                Instantiate(PreLoadedAssets.Instance.Feelings[2], transform);
            }
            else if (ShowEnemy)
            {
                Instantiate(PreLoadedAssets.Instance.Feelings[3], transform);
            }
            else if (FollowingCheese)
            {
                Instantiate(PreLoadedAssets.Instance.Feelings[4], transform);
            }
            else if (Confused)
            {
                Instantiate(PreLoadedAssets.Instance.Feelings[5], transform);
            }
            else
            {
                Instantiate(PreLoadedAssets.Instance.Feelings[6], transform);
            }
        }
    }
}
