﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common.Tools;
using Common.PathFinding;
using Common.Managers;
using MiceCompany;

[RequireComponent(typeof(Rigidbody2D),typeof(CircleCollider2D),typeof(SpriteRenderer))]
public class ZombieInteractionController : MonoBehaviour
{
    protected Mice LinkedMice;
    public int DamageAmount = 1;
    public float DamageForce = 5000;

    private void Awake()
    {
        LinkedMice = GetComponent<Mice>();
    }

    protected virtual void OnCollisionEnter2D(Collision2D other)
    {
        PlayerMice mouse = other.gameObject.GetCachedComponent<PlayerMice>();
        if (mouse != null)
        {
            var pose = mouse.transform.position;
            var rot = mouse.transform.rotation;

            bool alive = mouse.AddToHealth(-DamageAmount);
            if(!alive)
                NewZombie(pose,rot);
            else
                mouse.Rigid2D.AddForce((mouse.transform.position - transform.position).normalized * DamageForce);
        }
    }


    public Mice ConvertToAZombie(PlayerMice m)
    {
        Vector3 lastPose = m.transform.position;
        Quaternion lastRote = m.transform.rotation;

        m.Death(DeathStyle.UnVisible);

        var o = Instantiate(gameObject, lastPose, lastRote, GameManager.Instance.MiceRoot);
        o.name = name;
        //Debug.LogError("NewZombie " + o.name);

        return o.GetComponent<Mice>();
    }

    public Mice NewZombie(Vector3 pose, Quaternion rot)
    {
        var o = Instantiate(gameObject, pose, rot, GameManager.Instance.MiceRoot);
        o.name = name;
        //Debug.LogError("NewZombie " +o.name);

        return o.GetComponent<Mice>();
    }
}
