﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MiceCompany
{
    [ExecuteInEditMode]
    public class HealthBarController : MonoBehaviour
    {
        public SpriteRenderer HealtBarRenderer;
#if UNITY_EDITOR
        public Mice LinkedMice;
#endif

        private void Awake()
        {
            HealtBarRenderer = GetComponent<SpriteRenderer>();
#if UNITY_EDITOR
            LinkedMice = GetComponentInParent<Mice>();

            if (LinkedMice != null)
                SetHealth(LinkedMice.Health);
#endif
        }

        public void SetHealth(int count)
        {
            HealtBarRenderer.size = new Vector2(count * 6, 6);
        }
    }
}
