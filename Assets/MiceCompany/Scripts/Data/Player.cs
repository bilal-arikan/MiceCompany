﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Data;
using MiceCompany;
using Common.Tools;

namespace Common.Data
{
    public partial class Player
    {
        [Space(10)]
        public int MaxOwnedMiceCount = 5;
        public int MaxOwnedBuffCount = 1;
        //public List<int> OwnedMouses = new List<int>();
        public List<int> OwnedBuffs = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    }


}