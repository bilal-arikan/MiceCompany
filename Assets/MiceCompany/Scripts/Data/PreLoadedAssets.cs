﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Tools;
using MiceCompany;

namespace Common.Data
{
    /// <summary>
    /// For MiceCompany
    /// </summary>
    public partial class PreLoadedAssets : Singleton<PreLoadedAssets>
    {
        public List<Mice> MousePrefabs = new List<Mice>();
        public List<Buff> BuffPrefabs = new List<Buff>();
        public List<Placable> PlacablePrefabs = new List<Placable>();
        public List<Lootable> LootablePrefabs = new List<Lootable>();
        public List<GameObject> Feelings = new List<GameObject>();

        public static GameObject InterestingPositive
        {
            get
            {
                return PreLoadedAssets.Instance.GameObjects[0];
            }
        }
        public static GameObject InterestingNegative
        {
            get
            {
                return PreLoadedAssets.Instance.GameObjects[1];
            }
        }

        public static GameObject DeathPrefabStandart
        {
            get
            {
                return PreLoadedAssets.Instance.GameObjects[3];
            }
        }
        public static GameObject DeathPrefabDrowned
        {
            get
            {
                return PreLoadedAssets.Instance.GameObjects[4];
            }
        }
        public static GameObject DeathPrefabBlowed
        {
            get
            {
                return PreLoadedAssets.Instance.GameObjects[5];
            }
        }

        public static GameObject DeathPrefabPoisoned
        {
            get
            {
                return PreLoadedAssets.Instance.GameObjects[6];
            }
        }
    }
}
