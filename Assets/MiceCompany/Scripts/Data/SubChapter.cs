﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public partial class SubChapter : ScriptableObject
    {
        public List<int> DisallowedMices = new List<int>();
        public List<int> DisallowedBuffs = new List<int>();
        public List<int> DisallowedPlacables = new List<int>();

        public List<int> GivenMices = new List<int>();
        public List<int> GivenBuffs = new List<int>();
        public List<int> GivenPlacables = new List<int>();

        public int MaxUsableMiceCount = 0;
        public int MaxUsableBuffCount = 0;
        public int MaxUsablePlacableCount = 0;

        public float ToWinSeconds = 0;
        public int ToWinArrive = -1;
        public int ToWinNotDeath = -1;

        [Space(20)]

        [Newtonsoft.Json.JsonIgnore, NonSerialized]
        public SubChSelectButton LinkedButton;

        public bool RemoveMiceObstacle = false;
    }
}
