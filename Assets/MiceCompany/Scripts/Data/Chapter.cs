﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Data
{
    public partial class Chapter : ScriptableObject
    {
        [Newtonsoft.Json.JsonIgnore, NonSerialized]
        public ChView LinkedChView;

        [Newtonsoft.Json.JsonIgnore, NonSerialized]
        public SubChView LinkedSubChView;
    }
}
