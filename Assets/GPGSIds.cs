// <copyright file="GPGSIds.cs" company="Google Inc.">
// Copyright (C) 2015 Google Inc. All Rights Reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//    limitations under the License.
// </copyright>

///
/// This file is automatically generated DO NOT EDIT!
///
/// These are the constants defined in the Play Games Console for Game Services
/// Resources.
///


public static class GPGSIds
{
        public const string achievement_unlock_4_chapter = "CgkI86LD7bMHEAIQDQ"; // <GPGSID>
        public const string leaderboard_chapter04 = "CgkI86LD7bMHEAIQBQ"; // <GPGSID>
        public const string achievement_first_place = "CgkI86LD7bMHEAIQEw"; // <GPGSID>
        public const string leaderboard_chapter03 = "CgkI86LD7bMHEAIQBA"; // <GPGSID>
        public const string achievement_play_3_days = "CgkI86LD7bMHEAIQDw"; // <GPGSID>
        public const string achievement_win_3_stars = "CgkI86LD7bMHEAIQBw"; // <GPGSID>
        public const string achievement_zombie_apocalypse = "CgkI86LD7bMHEAIQEQ"; // <GPGSID>
        public const string achievement_earn_10_video_gift = "CgkI86LD7bMHEAIQFA"; // <GPGSID>
        public const string leaderboard_main = "CgkI86LD7bMHEAIQHQ"; // <GPGSID>
        public const string achievement_arrived_red_mice = "CgkI86LD7bMHEAIQCg"; // <GPGSID>
        public const string achievement_play_7_days = "CgkI86LD7bMHEAIQEA"; // <GPGSID>
        public const string achievement_complete_the_game = "CgkI86LD7bMHEAIQFw"; // <GPGSID>
        public const string achievement_call_reaper = "CgkI86LD7bMHEAIQFg"; // <GPGSID>
        public const string achievement_unlock_2_chapter = "CgkI86LD7bMHEAIQCw"; // <GPGSID>
        public const string leaderboard_chapter01 = "CgkI86LD7bMHEAIQAg"; // <GPGSID>
        public const string achievement_unlock_5_chapter = "CgkI86LD7bMHEAIQDg"; // <GPGSID>
        public const string achievement_arrived_yellow_mice = "CgkI86LD7bMHEAIQCQ"; // <GPGSID>
        public const string achievement_unlock_3_chapter = "CgkI86LD7bMHEAIQDA"; // <GPGSID>
        public const string achievement_genius_mice = "CgkI86LD7bMHEAIQCA"; // <GPGSID>
        public const string achievement_first_play = "CgkI86LD7bMHEAIQAA"; // <GPGSID>
        public const string achievement_share_on_facebook = "CgkI86LD7bMHEAIQEg"; // <GPGSID>
        public const string achievement_3_new_friends = "CgkI86LD7bMHEAIQFQ"; // <GPGSID>
        public const string leaderboard_chapter05 = "CgkI86LD7bMHEAIQBg"; // <GPGSID>
        public const string leaderboard_chapter02 = "CgkI86LD7bMHEAIQAw"; // <GPGSID>

}

