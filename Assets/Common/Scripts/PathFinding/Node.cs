﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Common.Tools;

namespace Common.PathFinding
{
    public class Node
    {
        public bool Accesible = true;

        //Grid coordinates
        public int X;
        public int Y;

        //world position
        public Vector2 Position;
        public CGrid grid;

        public Node ToTargetNode = null;
        public Direction ToTargetDirection;

        /// <summary>
        /// Right, TopRight, Top, TopLeft, Left,  BottomLeft, Bottom, BottomRight
        /// </summary>
        public Node[] Around = new Node[8];
        public Direction[] AroundDirections = new Direction[8];
        public Node TopLeft
        {
            get
            {
                return grid.GetNode(X - 1, Y + 1);
            }
        }
        public Node Top
        {
            get
            {
                return grid.GetNode(X, Y + 1);
            }
        }
        public Node TopRight
        {
            get
            {
                return grid.GetNode(X + 1, Y + 1);
            }
        }
        public Node Right
        {
            get
            {
                return grid.GetNode(X + 1, Y);
            }
        }
        public Node BottomRight
        {
            get
            {
                return grid.GetNode(X + 1, Y - 1);
            }
        }
        public Node Bottom
        {
            get
            {
                return grid.GetNode(X, Y - 1);
            }
        }
        public Node BottomLeft
        {
            get
            {
                return grid.GetNode(X - 1, Y - 1);
            }
        }
        public Node Left
        {
            get
            {
                return grid.GetNode(X - 1, Y);
            }
        }

        public Node(int x, int y, Vector2 pos, CGrid g)
        {
            grid = g;
            Position = pos;
            X = x;
            Y = y;
        }

        public void Initialize()
        {
            // Kenarlardakiler ilk 4, çaprazdakiler son 4
            Around[0] = Right;
            Around[4] = TopRight;
            Around[1] = Top;
            Around[5] = TopLeft;
            Around[2] = Left;
            Around[6] = BottomLeft;
            Around[3] = Bottom;
            Around[7] = BottomRight;
            AroundDirections[0] = Direction.Right;
            AroundDirections[4] = Direction.TopRight;
            AroundDirections[1] = Direction.Top;
            AroundDirections[5] = Direction.TopLeft;
            AroundDirections[2] = Direction.Left;
            AroundDirections[6] = Direction.BottomLeft;
            AroundDirections[3] = Direction.Bottom;
            AroundDirections[7] = Direction.BottomRight;
            /*AroundDirections[0] = PathWay.Reverse(Direction.Right);
            AroundDirections[4] = PathWay.Reverse(Direction.TopRight);
            AroundDirections[1] = PathWay.Reverse(Direction.Top);
            AroundDirections[5] = PathWay.Reverse(Direction.TopLeft);
            AroundDirections[2] = PathWay.Reverse(Direction.Left);
            AroundDirections[6] = PathWay.Reverse(Direction.BottomLeft);
            AroundDirections[3] = PathWay.Reverse(Direction.Bottom);
            AroundDirections[7] = PathWay.Reverse(Direction.BottomRight);*/
        }

        public override string ToString()
        {
            return "N[" + X + ":" + Y + "]";
        }

    }
}
