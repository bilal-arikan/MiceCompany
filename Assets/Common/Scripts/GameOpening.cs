﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Common.Tools;
using Common.Managers;

public class GameOpening : Singleton<GameOpening> {

    public bool ContinueWithPressAny = false;
    public Scrollbar loadingBar;
    public Text pressAnyKey;
    public Text versionText;
    public List<int> ScenesToLoad = new List<int>() { 1 };

    private List<AsyncOperation> AOps = new List<AsyncOperation>();

    protected override void Awake()
    {
        //Screen.autorotateToLandscapeLeft = true;
        //Screen.autorotateToLandscapeRight = true;
        Screen.orientation = ScreenOrientation.Landscape;
        loadingBar.size = 0.0f;
        pressAnyKey.text = ". . .";
        versionText.text = Application.version;

        base.Awake();
    }

    public void Start()
    {
        StartCoroutine(AsynchronousLoad(ScenesToLoad));
    }

    IEnumerator AsynchronousLoad(List<int> scenes,int logOnScreenIndex = 0)
    {
        yield return null;
        AOps.Clear();

        // Her scene sırayla yüklenir
        for (int i = 0 ; i < scenes.Count; i++)
        {
            AsyncOperation ao = SceneManager.LoadSceneAsync(scenes[i], LoadSceneMode.Additive);
            ao.allowSceneActivation = false;
            AOps.Add(ao);

            while(ao.progress < 0.89)
            {
                float progress = ((ao.progress / 0.9f) / scenes.Count) + (i / (float)scenes.Count);
                //Debug.Log("Pr(" + i + "): " + progress);

                pressAnyKey.text = (int)(progress * 100) + "";
                loadingBar.size = progress;
                yield return new WaitForEndOfFrame();

            }
            Debug.Log("Pr "+i+" Yüklendi");

            yield return new WaitForEndOfFrame();

        }

        //Hepsi yüklendikten sonra hepsi aktifleştirilir
        for (int i=0; i<AOps.Count; i++) 
        {
            AOps.ForEach(a => { a.allowSceneActivation = true; });
        }

    }

}
