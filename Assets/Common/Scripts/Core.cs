﻿using Common.Managers;
using Common.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using System.Net;
using System.Net.Sockets;

namespace Common
{
    [ExecuteInEditMode]
    public class Core : Singleton<Core>
    {
        public float RequestTimeout = 5;
        public int CachedObjectCount = 0;

        [SerializeField]
        protected Queue<Action> _pending = new Queue<Action>();

        public List<string> AvailableNamespaces = new List<string>() { "Common","MiceCompany" };

        public List<string> AllTypeNames = new List<string>();

        public List<E> EventsToCallGarbageCollector = new List<E>()
        {
            E.AppStarted,
            E.GameFinished,
            E.GameStarted,
            E.GameOnPreparing,
            E.LevelLoadingStarted,
            E.LevelLoadingCompleted,
        };


        protected override void Awake()
        {
            base.Awake();

            if (Application.isPlaying)
                StartCoroutine(CoUpdate());

            var all = Assembly.GetExecutingAssembly().GetTypes().Where(t =>
                t.IsSubclassOf(typeof(Component)) 
                &&
                !t.IsSubclassOf(typeof(MarkLight.View))
                &&
                (
                    string.IsNullOrEmpty(t.Namespace) 
                    ||
                    AvailableNamespaces.Any(s => t.Namespace.StartsWith(s))
                )
                );
            AllTypeNames.Clear();
            AllTypeNames.AddRange(all.ToList().ConvertAll(t => t.FullName));


            if (Application.isPlaying)
                // Belirli eventlerde GB otomatik çağırılıyor
                EventsToCallGarbageCollector.ForEach((e) =>
                {
                   EventManager.StartListening(e, System.GC.Collect);
                });
        }


        IEnumerator CoUpdate()
        {
            while (true)
            {
                CachedObjectCount = Caching.Components.Count;

                lock (_pending)
                {
                    while (_pending.Count != 0) _pending.Dequeue().Invoke();
                }

                // GarbageColletion Performansına katkı sağlaması için AMA
                // System.GC.Collect() fonksiyonunu daha spesific yerlerde çağırmak lazım
                /*if (Time.frameCount % 30 == 0)
                {
                    System.GC.Collect();
                }*/

                yield return new WaitForEndOfFrame();
            }
        }

        /// <summary>
        /// Run action on Main Thread
        /// </summary>
        /// <param name="a"></param>
        public static void Invoke(Action a)
        {
            lock (Instance._pending) { Instance._pending.Enqueue(a); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="adress"></param>
        /// <param name="getParams"></param>
        /// <param name="callback"></param>
        public static void Request(string adress, Dictionary<string,string> postParams, Action<WWW> callback)
        {
            Invoke(() => { Instance.StartCoroutine(SendRequestCo(adress, postParams, callback)); });
            
        }



        static IEnumerator SendRequestCo(
            string adress, 
            Dictionary<string, string> postData, 
            Action<WWW> callback)
        {
            if (string.IsNullOrEmpty(adress))
            {
                callback(null);
                yield break;
            }


            float timeout = Time.time + Instance.RequestTimeout;

            WWW www;
            if(postData != null)
            {
                var form = new WWWForm();
                foreach (KeyValuePair<string, string> field in postData)
                {
                    form.AddField(field.Key, field.Value);
                }
                www = new WWW(adress ,form);
            }
            else
            {
                www = new WWW(adress );
            }

            while (!www.isDone)
            {
                if (Time.time > timeout)
                {
                    callback(null);
                    yield break;
                }
                yield return new WaitForEndOfFrame();
            }
            callback(www);
        }


        public static void DownloadPhoto(string url, Action<Sprite> result)
        {
            Core.Request(url, null, (www) =>
            {
                if (www != null && www.texture != null)
                {
                    var sprite = Sprite.Create(
                        www.texture,
                        new Rect(0, 0, www.texture.width, www.texture.height),
                        new Vector2(www.texture.width / 2, www.texture.height / 2));

                    result(sprite);
                }
                else
                    result(null);

            });
        }

        public static DateTime GetNetworkTime()
        {
            try
            {
                const string ntpServer = "pool.ntp.org"; // time.nist.gov
                var ntpData = new byte[48];
                ntpData[0] = 0x1B; //LeapIndicator = 0 (no warning), VersionNum = 3 (IPv4 only), Mode = 3 (Client Mode)

                var addresses = Dns.GetHostEntry(ntpServer).AddressList;
                var ipEndPoint = new IPEndPoint(addresses[0], 123);
                var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                socket.Connect(ipEndPoint);
                socket.Send(ntpData);
                socket.Receive(ntpData);
                socket.Close();

                ulong intPart = (ulong)ntpData[40] << 24 | (ulong)ntpData[41] << 16 | (ulong)ntpData[42] << 8 | (ulong)ntpData[43];
                ulong fractPart = (ulong)ntpData[44] << 24 | (ulong)ntpData[45] << 16 | (ulong)ntpData[46] << 8 | (ulong)ntpData[47];

                var milliseconds = (intPart * 1000) + ((fractPart * 1000) / 0x100000000L);
                var networkDateTime = (new DateTime(1900, 1, 1)).AddMilliseconds((long)milliseconds);

                return networkDateTime;
            }
            catch (Exception ex)
            {
                Debug.LogWarning("Sunucu Zaman bilgileri alınamadı !!! " + ex.GetType());
                return DateTime.Now;
            }
        }
    }
}
