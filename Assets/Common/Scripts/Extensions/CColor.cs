﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class CColor
{
    //http://avatar.se/molscript/doc/colour_names.html
    public static Color aliceblue = new Color(0.941176f, 0.972549f, 1f);
    public static Color antiquewhite = new Color(0.980392f, 0.921569f, 0.843137f);
    public static Color aquamarine = new Color(0.498039f, 1f, 0.831373f);
    public static Color azure = new Color(0.941176f, 1f, 1f);
    public static Color beige = new Color(0.960784f, 0.960784f, 0.862745f);
    public static Color bisque = new Color(1f, 0.894118f, 0.768627f);
    public static Color black = new Color(0, 0, 0f);
    public static Color blanchedalmond = new Color(1f, 0.921569f, 0.803922f);
    public static Color blue = new Color(0f, 0f, 1f);
    public static Color blueviolet = new Color(0.541176f, 0.168627f, 0.886275f);
    public static Color brown = new Color(0.647059f, 0.164706f, 0.164706f);
    public static Color burlywood = new Color(0.870588f, 0.721569f, 0.529412f);
    public static Color cadetblue = new Color(0.372549f, 0.619608f, 0.627451f);
    public static Color chartreuse = new Color(0.498039f, 1f, 0f);
    public static Color chocolate = new Color(0.823529f, 0.411765f, 0.117647f);
    public static Color coral = new Color(1f, 0.498039f, 0.313725f);
    public static Color cornflowerblue = new Color(0.392157f, 0.584314f, 0.929412f);
    public static Color cornsilk = new Color(1f, 0.972549f, 0.862745f);
    public static Color crimson = new Color(0.862745f, 0.0784314f, 0.235294f);
    public static Color cyan = new Color(0f, 1f, 1f);
    public static Color darkblue = new Color(0f, 0f, 0.545098f);
    public static Color darkcyan = new Color(0f, 0.545098f, 0.545098f);
    public static Color darkgoldenrod = new Color(0.721569f, 0.52549f, 0.0431373f);
    public static Color darkgray = new Color(0.662745f, 0.662745f, 0.662745f);
    public static Color darkgreen = new Color(0f, 0.392157f, 0f);
    public static Color darkgrey = new Color(0.662745f, 0.662745f, 0.662745f);
    public static Color darkkhaki = new Color(0.741176f, 0.717647f, 0.419608f);
    public static Color darkmagenta = new Color(0.545098f, 0f, 0.545098f);
    public static Color darkolivegreen = new Color(0.333333f, 0.419608f, 0.184314f);
    public static Color darkorange = new Color(1f, 0.54902f, 0f);
    public static Color darkorchid = new Color(0.6f, 0.196078f, 0.8f);
    public static Color darkred = new Color(0.545098f, 0f, 0f);
    public static Color darksalmon = new Color(0.913725f, 0.588235f, 0.478431f);
    public static Color darkseagreen = new Color(0.560784f, 0.737255f, 0.560784f);
    public static Color darkslateblue = new Color(0.282353f, 0.239216f, 0.545098f);
    public static Color darkslategray = new Color(0.184314f, 0.309804f, 0.309804f);
    public static Color darkslategrey = new Color(0.184314f, 0.309804f, 0.309804f);
    public static Color darkturquoise = new Color(0f, 0.807843f, 0.819608f);
    public static Color darkviolet = new Color(0.580392f, 0f, 0.827451f);
    public static Color deeppink = new Color(1f, 0.0784314f, 0.576471f);
    public static Color deepskyblue = new Color(0f, 0.74902f, 1f);
    public static Color dimgray = new Color(0.411765f, 0.411765f, 0.411765f);
    public static Color dimgrey = new Color(0.411765f, 0.411765f, 0.411765f);
    public static Color dodgerblue = new Color(0.117647f, 0.564706f, 1f);
    public static Color firebrick = new Color(0.698039f, 0.133333f, 0.133333f);
    public static Color floralwhite = new Color(1f, 0.980392f, 0.941176f);
    public static Color forestgreen = new Color(0.133333f, 0.545098f, 0.133333f);
    public static Color gainsboro = new Color(0.862745f, 0.862745f, 0.862745f);
    public static Color ghostwhite = new Color(0.972549f, 0.972549f, 1f);
    public static Color gold = new Color(1f, 0.843137f, 0f);
    public static Color goldenrod = new Color(0.854902f, 0.647059f, 0.12549f);
    public static Color gray = new Color(0.745098f, 0.745098f, 0.745098f);
    public static Color green = new Color(0f, 1f, 0f);
    public static Color greenyellow = new Color(0.678431f, 1f, 0.184314f);
    public static Color grey = new Color(0.745098f, 0.745098f, 0.745098f);
    public static Color honeydew = new Color(0.941176f, 1f, 0.941176f);
    public static Color hotpink = new Color(1f, 0.411765f, 0.705882f);
    public static Color indianred = new Color(0.803922f, 0.360784f, 0.360784f);
    public static Color indigo = new Color(0.294118f, 0f, 0.509804f);
    public static Color ivory = new Color(1f, 1f, 0.941176f);
    public static Color khaki = new Color(0.941176f, 0.901961f, 0.54902f);
    public static Color lavender = new Color(0.901961f, 0.901961f, 0.980392f);
    public static Color lavenderblush = new Color(1f, 0.941176f, 0.960784f);
    public static Color lawngreen = new Color(0.486275f, 0.988235f, 0f);
    public static Color lemonchiffon = new Color(1f, 0.980392f, 0.803922f);
    public static Color lightblue = new Color(0.678431f, 0.847059f, 0.901961f);
    public static Color lightcoral = new Color(0.941176f, 0.501961f, 0.501961f);
    public static Color lightcyan = new Color(0.878431f, 1f, 1f);
    public static Color lightgoldenrod = new Color(0.933333f, 0.866667f, 0.509804f);
    public static Color lightgoldenrodyellow = new Color(0.980392f, 0.980392f, 0.823529f);
    public static Color lightgray = new Color(0.827451f, 0.827451f, 0.827451f);
    public static Color lightgreen = new Color(0.564706f, 0.933333f, 0.564706f);
    public static Color lightgrey = new Color(0.827451f, 0.827451f, 0.827451f);
    public static Color lightpink = new Color(1f, 0.713725f, 0.756863f);
    public static Color lightsalmon = new Color(1f, 0.627451f, 0.478431f);
    public static Color lightseagreen = new Color(0.12549f, 0.698039f, 0.666667f);
    public static Color lightskyblue = new Color(0.529412f, 0.807843f, 0.980392f);
    public static Color lightslateblue = new Color(0.517647f, 0.439216f, 1f);
    public static Color lightslategray = new Color(0.466667f, 0.533333f, 0.6f);
    public static Color lightslategrey = new Color(0.466667f, 0.533333f, 0.6f);
    public static Color lightsteelblue = new Color(0.690196f, 0.768627f, 0.870588f);
    public static Color lightyellow = new Color(1f, 1f, 0.878431f);
    public static Color limegreen = new Color(0.196078f, 0.803922f, 0.196078f);
    public static Color linen = new Color(0.980392f, 0.941176f, 0.901961f);
    public static Color magenta = new Color(1f, 0f, 1f);
    public static Color maroon = new Color(0.690196f, 0.188235f, 0.376471f);
    public static Color mediumaquamarine = new Color(0.4f, 0.803922f, 0.666667f);
    public static Color mediumblue = new Color(0f, 0f, 0.803922f);
    public static Color mediumorchid = new Color(0.729412f, 0.333333f, 0.827451f);
    public static Color mediumpurple = new Color(0.576471f, 0.439216f, 0.858824f);
    public static Color mediumseagreen = new Color(0.235294f, 0.701961f, 0.443137f);
    public static Color mediumslateblue = new Color(0.482353f, 0.407843f, 0.933333f);
    public static Color mediumspringgreen = new Color(0f, 0.980392f, 0.603922f);
    public static Color mediumturquoise = new Color(0.282353f, 0.819608f, 0.8f);
    public static Color mediumvioletred = new Color(0.780392f, 0.0823529f, 0.521569f);
    public static Color midnightblue = new Color(0.0980392f, 0.0980392f, 0.439216f);
    public static Color mintcream = new Color(0.960784f, 1f, 0.980392f);
    public static Color mistyrose = new Color(1f, 0.894118f, 0.882353f);
    public static Color moccasin = new Color(1f, 0.894118f, 0.709804f);
    public static Color navajowhite = new Color(1f, 0.870588f, 0.678431f);
    public static Color navy = new Color(0f, 0f, 0.501961f);
    public static Color navyblue = new Color(0f, 0f, 0.501961f);
    public static Color oldlace = new Color(0.992157f, 0.960784f, 0.901961f);
    public static Color olivedrab = new Color(0.419608f, 0.556863f, 0.137255f);
    public static Color orange = new Color(1f, 0.647059f, 0f);
    public static Color orangered = new Color(1f, 0.270588f, 0f);
    public static Color orchid = new Color(0.854902f, 0.439216f, 0.839216f);
    public static Color palegoldenrod = new Color(0.933333f, 0.909804f, 0.666667f);
    public static Color palegreen = new Color(0.596078f, 0.984314f, 0.596078f);
    public static Color paleturquoise = new Color(0.686275f, 0.933333f, 0.933333f);
    public static Color palevioletred = new Color(0.858824f, 0.439216f, 0.576471f);
    public static Color papayawhip = new Color(1f, 0.937255f, 0.835294f);
    public static Color peachpuff = new Color(1f, 0.854902f, 0.72549f);
    public static Color peru = new Color(0.803922f, 0.521569f, 0.247059f);
    public static Color pink = new Color(1f, 0.752941f, 0.796078f);
    public static Color plum = new Color(0.866667f, 0.627451f, 0.866667f);
    public static Color powderblue = new Color(0.690196f, 0.878431f, 0.901961f);
    public static Color purple = new Color(0.627451f, 0.12549f, 0.941176f);
    public static Color red = new Color(1f, 0f, 0f);
    public static Color rosybrown = new Color(0.737255f, 0.560784f, 0.560784f);
    public static Color royalblue = new Color(0.254902f, 0.411765f, 0.882353f);
    public static Color saddlebrown = new Color(0.545098f, 0.270588f, 0.0745098f);
    public static Color salmon = new Color(0.980392f, 0.501961f, 0.447059f);
    public static Color sandybrown = new Color(0.956863f, 0.643137f, 0.376471f);
    public static Color seagreen = new Color(0.180392f, 0.545098f, 0.341176f);
    public static Color seashell = new Color(1f, 0.960784f, 0.933333f);
    public static Color sgibeet = new Color(0.556863f, 0.219608f, 0.556863f);
    public static Color sgibrightgray = new Color(0.772549f, 0.756863f, 0.666667f);
    public static Color sgibrightgrey = new Color(0.772549f, 0.756863f, 0.666667f);
    public static Color sgichartreuse = new Color(0.443137f, 0.776471f, 0.443137f);
    public static Color sgidarkgray = new Color(0.333333f, 0.333333f, 0.333333f);
    public static Color sgidarkgrey = new Color(0.333333f, 0.333333f, 0.333333f);
    public static Color sgilightblue = new Color(0.490196f, 0.619608f, 0.752941f);
    public static Color sgilightgray = new Color(0.666667f, 0.666667f, 0.666667f);
    public static Color sgilightgrey = new Color(0.666667f, 0.666667f, 0.666667f);
    public static Color sgimediumgray = new Color(0.517647f, 0.517647f, 0.517647f);
    public static Color sgimediumgrey = new Color(0.517647f, 0.517647f, 0.517647f);
    public static Color sgiolivedrab = new Color(0.556863f, 0.556863f, 0.219608f);
    public static Color sgisalmon = new Color(0.776471f, 0.443137f, 0.443137f);
    public static Color sgislateblue = new Color(0.443137f, 0.443137f, 0.776471f);
    public static Color sgiteal = new Color(0.219608f, 0.556863f, 0.556863f);
    public static Color sgiverydarkgray = new Color(0.156863f, 0.156863f, 0.156863f);
    public static Color sgiverydarkgrey = new Color(0.156863f, 0.156863f, 0.156863f);
    public static Color sgiverylightgray = new Color(0.839216f, 0.839216f, 0.839216f);
    public static Color sgiverylightgrey = new Color(0.839216f, 0.839216f, 0.839216f);
    public static Color sienna = new Color(0.627451f, 0.321569f, 0.176471f);
    public static Color skyblue = new Color(0.529412f, 0.807843f, 0.921569f);
    public static Color slateblue = new Color(0.415686f, 0.352941f, 0.803922f);
    public static Color slategray = new Color(0.439216f, 0.501961f, 0.564706f);
    public static Color slategrey = new Color(0.439216f, 0.501961f, 0.564706f);
    public static Color snow = new Color(1f, 0.980392f, 0.980392f);
    public static Color springgreen = new Color(0f, 1f, 0.498039f);
    public static Color steelblue = new Color(0.27451f, 0.509804f, 0.705882f);
    public static Color tan = new Color(0.823529f, 0.705882f, 0.54902f);
    public static Color thistle = new Color(0.847059f, 0.74902f, 0.847059f);
    public static Color tomato = new Color(1f, 0.388235f, 0.278431f);
    public static Color turquoise = new Color(0.25098f, 0.878431f, 0.815686f);
    public static Color violet = new Color(0.933333f, 0.509804f, 0.933333f);
    public static Color violetred = new Color(0.815686f, 0.12549f, 0.564706f);
    public static Color wheat = new Color(0.960784f, 0.870588f, 0.701961f);
    public static Color white = new Color(1f, 1f, 1f);
    public static Color whitesmoke = new Color(0.960784f, 0.960784f, 0.960784f);
    public static Color yellow = new Color(1f, 1f, 0f);
    public static Color yellowgreen = new Color(0.603922f, 0.803922f, 0.196078f);

}
