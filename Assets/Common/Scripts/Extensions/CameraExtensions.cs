﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Tools
{
    public static class CameraExtensions
    {

        public static Bounds OrthographicBounds(this Camera camera)
        {
            float screenAspect = (float)Screen.width / (float)Screen.height;
            float cameraHeight = camera.orthographicSize * 2;
            Bounds bounds = new Bounds(
                camera.transform.position,
                new Vector3(cameraHeight * screenAspect, cameraHeight, 0));
            return bounds;
        }

        public static Vector4 RightTopLeftBottom(this Camera camera)
        {
            Bounds b = camera.OrthographicBounds();
            Vector4 v = new Vector4(
                (b.center + b.extents).x,
                (b.center + b.extents).y,
                (b.center - b.extents).x,
                (b.center - b.extents).y);
            return v;
        }
    }
}
