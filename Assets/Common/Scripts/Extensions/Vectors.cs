﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Swizzling.cs" company="Nick Prühs">
//   Copyright (c) Nick Prühs. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

    using UnityEngine;

public static class Vectors
{
    #region Public Methods and Operators

    public static Vector2 WithX(this Vector2 v, float newX)
    {
        return new Vector2(newX, v.y);
    }

    public static Vector3 WithX(this Vector3 v, float newX)
    {
        return new Vector3(newX, v.y, v.z);
    }

    public static Vector2 WithY(this Vector2 v, float newY)
    {
        return new Vector2(v.x, newY);
    }

    public static Vector3 WithY(this Vector3 v, float newY)
    {
        return new Vector3(v.x, newY, v.z);
    }

    public static Vector3 WithZ(this Vector3 v, float newZ)
    {
        return new Vector3(v.x, v.y, newZ);
    }

    public static Vector2 XY(this Vector3 v)
    {
        return new Vector2(v.x, v.y);
    }

    public static Vector2 XZ(this Vector3 v)
    {
        return new Vector2(v.x, v.z);
    }

    public static Vector2 YZ(this Vector3 v)
    {
        return new Vector2(v.y, v.z);
    }

    public static Vector2 Closest(this Vector2 v, Vector2[] arounds)
    {
        if(arounds.Length == 0)
        {
            Debug.LogError("arounds.Length must be greater than Zero(0)");
            return Vector2.zero;
        }

        Vector2 closest = arounds[0];
        float magnitude = (arounds[0] - v).magnitude;
        // 0. indexi default olarak aldık
        for (int i=1; i< arounds.Length; i++)
        {
            float temp = (arounds[i] - v).magnitude;
            if (temp < magnitude)
            {
                closest = arounds[i];
                magnitude = temp;
            }
        }
        return closest;
    }

    public static Vector3 Closest(this Vector3 v, Vector3[] arounds)
    {
        if (arounds.Length == 0)
        {
            Debug.LogError("arounds.Length must be greater than Zero(0)");
            return Vector3.zero;
        }

        Vector3 closest = arounds[0];
        float magnitude = (arounds[0] - v).magnitude;
        // 0. indexi default olarak aldık
        for (int i = 1; i < arounds.Length; i++)
        {
            float temp = (arounds[i] - v).magnitude;
            if (temp < magnitude)
            {
                closest = arounds[i];
                magnitude = temp;
            }
        }
        return closest;
    }

    #endregion
}
