﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class Collision2Ds
{
    public static T GetComponent<T>(this Collision2D coll) where T : Component
    {
        return coll.gameObject.GetCachedComponent<T>();
    }

    public static Component GetComponent(this Collision2D coll, Type t)
    {
        return coll.gameObject.GetCachedComponent(t);
    }
}
