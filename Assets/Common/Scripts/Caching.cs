﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class Caching
{
    // All Cached References
    public static Dictionary<GameObject, Dictionary<Type, Component>> Components = new Dictionary<GameObject, Dictionary<Type, Component>>();

    public static bool Log = false;
    /* Example Using
    protected virtual void Awake()
    {
        if (!gameObject.IsCached())
            gameObject.CacheComponents();
    }
    protected virtual void OnDestroy()
    {
        gameObject.UnCacheComponents();
    }
    */

    /// <summary>
    /// 
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static bool IsCached(this GameObject obj)
    {
        return Components.ContainsKey(obj);
    }

    /// <summary>
    /// if already cached returms false
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static bool CacheComponents(this GameObject obj)
    {
        Components.Remove(obj);
        Components.Add(obj, new Dictionary<Type, Component>());

        Component[] objs = obj.GetComponents<Component>();
        for (int i = 0; i < objs.Length; ++i)
        {
            Components[obj].Add(objs[i].GetType(), objs[i]);
        }

        if(Log)
            Debug.Log(obj.name + " Cached:" + objs.Length);
        return true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static bool UnCacheComponents(this GameObject obj)
    {
        return Components.Remove(obj);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static T GetCachedComponent<T>(this GameObject obj, bool ifNullUseDefaultMethod = false) where T : Component
    {
        if (!obj.IsCached())
        {
            if(Log)
                Debug.LogWarning(obj.name + " GameObject not Cached " + typeof(T));
            return obj.GetComponent<T>();
        }

        Component temp = null;
        // type ı Dictionaride ara (Item aranıyorken Mice ı da bulacak şekilde)
        for(int i = 0; i < Caching.Components[obj].Keys.Count; i++)
        {
            //Aranan type sa veya Alt typeı ise
            if (typeof(T) == Caching.Components[obj].Keys.ElementAt(i) ||
                Caching.Components[obj].Keys.ElementAt(i).IsSubclassOf(typeof(T)))
            {
                temp = Caching.Components[obj].Values.ElementAt(i);
                break;
            }
        }

        if(ifNullUseDefaultMethod && temp == null)
        {
            if(Log)
                Debug.LogWarning(obj.name + " Component not Cached " + typeof(T));
            temp = obj.GetComponent<T>();

            // GetComponentlede bulunamadıysa Component yoktur. Ama varsa Cacheda ekle
            /*if(temp != null)
                Caching.Components[obj].Add(temp.GetType(), temp);*/
        }

        return (T)temp;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="t"></param>
    /// <returns></returns>
    public static Component GetCachedComponent(this GameObject obj,Type t, bool ifNullUseDefaultMethod = false)
    {
        if (!obj.IsCached())
        {
            if(Log)
                Debug.LogWarning(obj.name + " GameObject not Cached " + t);
            return obj.GetComponent(t);
        }

        Component temp = null;
        // type ı Dictionaride ara (Item aranıyorken Mice ı da bulacak şekilde)
        for (int i = 0; i < Caching.Components[obj].Keys.Count; i++)
        {
            //Aranan type sa veya Alt typeı ise
            if (t == Caching.Components[obj].Keys.ElementAt(i) ||
                Caching.Components[obj].Keys.ElementAt(i).IsSubclassOf(t))
            {
                temp = Caching.Components[obj].Values.ElementAt(i);
                break;
            }
        }

        if (ifNullUseDefaultMethod && temp == null)
        {
            if(Log)
                Debug.LogWarning(obj.name + " Component not Cached " + t);
            temp = obj.GetComponent(t);

            // GetComponentlede bulunamadıysa Component yoktur. Ama varsa Cacheda ekle
            /*if (temp != null)
                Caching.Components[obj].Add(temp.GetType(), temp);*/
        }

        return temp;
    }
}
