﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using MarkLight.Views.UI;
using MarkLight;
using MarkLight.Views;

public class WarningView : ShowableView
{
    Action answer;
    public _string TextToShow;

    public void Show(string text, Action answer)
    {
        this.answer = answer;
        TextToShow.Value = text;
        base.Show();
    }
    
    void ResultYes()
    {
        base.Hide();
        if (answer != null)
            answer.Invoke();
        answer = null;
    }

}