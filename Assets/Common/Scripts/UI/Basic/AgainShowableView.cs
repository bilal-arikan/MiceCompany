﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MarkLight.Views.UI;
using MarkLight.Views;
using MarkLight;

public abstract class AgainShowableView : UIView
{
    public _string TextToShow;
    public ViewAnimation AnimationShow;
    public ViewAnimation AnimationHide;
    protected bool currentlyShowing = false;
    protected bool onlyOne = true;
    protected List<string> MoreTexts = new List<string>();

    protected virtual void Show(string text, bool callForNext = false)
    {
        if(!callForNext)
            MoreTexts.Add(text);

        if (!currentlyShowing)
        {
            currentlyShowing = true;
            if(TextToShow !=null)
                TextToShow.Value = text;
            IsVisible.Value = true;

            if (AnimationShow != null)
                AnimationShow.StartAnimation();
        }
    }

    protected virtual void Hide()
    {
        if (AnimationHide != null)
        {
            AnimationHide.StartAnimation();
        } 
        else
        {
            OnHideCompleted();
        }
    }

    protected virtual void AnimationShowCompleted()
    {

    }

    protected virtual void AnimationHideCompleted()
    {
        OnHideCompleted();
    }

    protected virtual void OnHideCompleted()
    {
        IsVisible.Value = false;
        currentlyShowing = false;

        MoreTexts.RemoveAt(0);

        if (MoreTexts.Count > 0)
            Show(MoreTexts[0],true);
    }
}
