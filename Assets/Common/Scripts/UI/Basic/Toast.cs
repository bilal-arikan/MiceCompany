﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using MarkLight.Views.UI;
using UnityEngine;
using MarkLight.Views;
using MarkLight;

public class Toast : AgainShowableView
{
    public Image ToastImage;
    public float defaultTimeToShow = 3;

    public void Show(string text)
    {
        base.Show(text,false);
        StartCoroutine(WaitCoroutine(defaultTimeToShow));
    }

    float i;
    private IEnumerator WaitCoroutine(float wait)
    {
        for (i=10; i>0;i--)
        {
            yield return new WaitForSecondsRealtime(wait/10);
        }
        Hide();
    }

    protected override void OnHideCompleted()
    {
        IsVisible.Value = false;
        currentlyShowing = false;

        MoreTexts.RemoveAt(0);

        if (MoreTexts.Count > 0)
        {
            Show(MoreTexts[0], true);
            StartCoroutine(WaitCoroutine(defaultTimeToShow));
        }
    }
}

