﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using MarkLight.Views.UI;
using MarkLight;
using MarkLight.Views;

public class AreYouSureView : ShowableView
{
    Action<bool> answer;
    bool currentAnswer;
    public _string TextToShow;

    public void Show(string text, Action<bool> answer)
    {
        base.Show();
        TextToShow.Value = text;
        this.answer = answer;
    }

    void ResultYes()
    {
        currentAnswer = true;
        Hide();
        if (answer != null)
            answer.Invoke(currentAnswer);
        answer = null;
    }

    void ResultNo()
    {
        currentAnswer = false;
        Hide();
        if (answer != null)
            answer.Invoke(currentAnswer);
        answer = null;
    }

}

