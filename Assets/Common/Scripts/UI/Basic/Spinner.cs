﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using MarkLight.Views.UI;
using MarkLight.Views;
using UnityEngine;

public class Spinner : UIView
{

    ViewAnimation SpinnerRotate;

    public void Show(float failedTime = 0)
    {
        IsVisible.Value = true;
        //SpinnerRotate.StartAnimation();
        if(failedTime > 0)
            StartCoroutine(SpinnerFailed(failedTime));
    }

    public void Hide()
    {
        IsVisible.Value = false;
        //SpinnerRotate.StopAnimation();
    }

    IEnumerator SpinnerFailed(float time)
    {
        if(time > -1)
        {
            yield return new WaitForSecondsRealtime(time);
            Hide();
        }
    }
}
