﻿using System;
using System.Collections.Generic;
using UnityEngine;
using MarkLight;
using MarkLight.Views;
using MarkLight.Views.UI;
using Common.Managers;

public class SceneUI : SingletonView<SceneUI>
{

    public ViewSwitcher MainViewSwitcher;
    public ViewSwitcher SubViewSwitcher;
    public Dictionary<Type, UIView> MainViews = new Dictionary<Type, UIView>();
    public Dictionary<Type, UIView> SubViews = new Dictionary<Type, UIView>();


    public Toast Toast;
    public Spinner LoadingSpinner;
    public WarningView Warning;
    public AreYouSureView AreYouSure;
    //public ColorSelector ColorS;
    //public WelcomeView Welcome;


    public override void Initialize()
    {

        // Store MainViews
        MainViewSwitcher.ForEachChild<UIView>(view =>
        {
            //Debug.Log("xXx");
            if (!MainViews.ContainsKey(view.GetType()))
            {
                MainViews.Add(view.GetType(), view);
            }
        }, false);

        SubViewSwitcher.ForEachChild<UIView>(view =>
        {
            //Debug.Log("xXx");
            if (!SubViews.ContainsKey(view.GetType()))
            {
                SubViews.Add(view.GetType(), view);
            }
        }, false);

        base.Initialize();
    }


    protected override void Awake()
    {
        base.Awake();
        UIManager.Instance.Scene = this;
    }

}
