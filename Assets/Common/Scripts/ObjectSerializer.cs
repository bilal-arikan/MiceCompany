﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using Common.Managers;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Common.Tools
{
    public static class ObjectSerializer
    {
        static JsonSerializerSettings sett = new JsonSerializerSettings() {
            ContractResolver = new DictionaryAsArrayResolver(),
            Converters = { new JsonGenericDictionaryOrArrayConverter() },
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore,
        };

        /// <summary>
        /// Parse to Json string file
        /// </summary>
        /// <param name="pretty"></param>
        /// <returns></returns>
        public static string ToJson(object o, bool pretty = false)
        {
            if (o == null)
            {
                Debug.LogWarning("ToJson: Object NULL");
                return "{}";
            }

            if (o.GetType().IsPrimitive || o.GetType() == typeof(string))
                return o.ToString();
            else
                return JsonConvert.SerializeObject(o, pretty ? Formatting.Indented : Formatting.None,sett);
        }

        /// <summary>
        /// Returns T typed object
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T FromJson<T>(string json)
        {
            if (string.IsNullOrEmpty(json))
            {
                Debug.LogWarning("FromJson: Json NULL");
                return default(T);
            }

            try
            {
                if (typeof(T) == typeof(string))
                    return (T)Convert.ChangeType(json, typeof(T));
                else if (typeof(T) == typeof(bool))
                    return (T)Convert.ChangeType(bool.Parse(json), typeof(T));
                else if (typeof(T).IsPrimitive)
                    return (T)Convert.ChangeType(json, typeof(T));
                else
                    return JsonConvert.DeserializeObject<T>(json, sett);
                //return (T)JsonUtility.FromJson(json, typeof(T));
            }
            catch (System.Exception ex)
            {
                Debug.LogError("FromJson Error: " + ex + " : " + typeof(T) + " : " + ex.Message);
                Debug.LogError(JsonToPretty(json));
                return default(T);
            }
        }

        public static string JsonToPretty(string json)
        {
            return Newtonsoft.Json.Linq.JToken.Parse(json).ToString();
        }
        /// <summary>
        /// forSerialize Must be class object
        /// forSerialize can contains only object Arrays
        /// </summary>
        /// <param name="forSerialize"></param>
        /// <returns></returns>
        public static Dictionary<string, object> ToDict(object forSerialize)
        {
            Dictionary<string, object> dicToReturn = new Dictionary<string, object>();

            if (forSerialize.GetType().IsPrimitive || forSerialize.GetType() == typeof(string))
            {
                throw new UnityException("ToDict: object doğrudan Primitive veya string olamaz");
            }
            else
            {
                FieldInfo[] fieldinfo = forSerialize.GetType().GetFields();
                foreach (FieldInfo f in fieldinfo)
                {
                    //int, bool ,float vs.
                    if (f.FieldType.IsPrimitive || f.FieldType == typeof(string))
                    {
                        Debug.Log("#" + f.FieldType + "#" + f.Name + "#" + f.GetValue(forSerialize));
                        dicToReturn.Add(f.Name, f.GetValue(forSerialize));
                    }
                    // Arrayler
                    else if (f.FieldType.IsArray || f.FieldType.IsGenericType)
                    {
                        Debug.Log("#" + f.FieldType + "#" + f.Name + "#" + f.GetValue(forSerialize));

                        // Array elemanları Cast edilemiyo buna bi çözüm bulunmalı !!!!!!!
                        /*var method = f.FieldType.GetMethod("Cast",BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreReturn |BindingFlags.InvokeMethod);
                        var genericMethod = method.MakeGenericMethod(typeof(object));
                        if(genericMethod != null)
                        {
                            Debug.Log("Not Null: " + genericMethod + method.ToString());
                        }
                        else
                        {
                            Debug.Log("Null: " + method.ToString());
                        }
                        var generic = (IEnumerable<object>)genericMethod.Invoke(f.GetValue(forSerialize),new object[0]);*/
                        var generic = (IEnumerable<object>)f.GetValue(forSerialize);

                        dicToReturn.Add(f.Name, ArrayToDict(generic));

                    }
                    // objectler
                    else
                    {
                        object o = f.GetValue(forSerialize);
                        if (o != null)
                        {
                            Debug.Log("#" + f.FieldType);
                            dicToReturn.Add(f.Name, ToDict(o));
                        }
                        else
                        {
                            dicToReturn.Add(f.Name, null);
                        }
                    }
                }
            }
            return dicToReturn;
        }

        public static Dictionary<string, object> ArrayToDict(IEnumerable<object> array)
        {
            Dictionary<string, object> dicToReturn = new Dictionary<string, object>();
            for (int i = 0; i < array.Count<object>(); i++)
            {
                object o = array.ElementAt(i);
                // int, bool, float Array elemanları
                if (o.GetType().IsPrimitive)
                {
                    Debug.Log("#" + o.GetType() + " (" + i + ") #" + o);
                    dicToReturn.Add(i.ToString(), o);
                }
                //object Array elemanları
                else
                {
                    dicToReturn.Add(i.ToString(), ToDict(o));
                }
            }
            return dicToReturn;
        }

    }
}
//*/