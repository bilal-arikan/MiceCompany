﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
using UnityEngine;
using MarkLight;
using Common.Tools;
using Common.Managers;
using MarkLight.Views.UI;

namespace Common.Managers
{
    public partial class UIManager : Singleton<UIManager>
    {
        [ReadOnly]
        public SceneUI Scene;
        [ReadOnly]
        public View LastMainView;
        [ReadOnly]
        public View MainMenuView;

        public bool ShowSplashScreens = true;

        public List<Sprite> LoadingViewSprites = new List<Sprite>();

        protected override void Awake()
        {

            base.Awake();
            Init();
        }

        public virtual void Init()
        {
            EventManager.StartListeningObjectEvent(E.LevelLoadingProgressed, LevelLoadingProgressed);
            EventManager.StartListeningObjectEvent(E.LevelLoadingStarted, LevelLoadingStarted);
            // For Debug
            CConsole.ActionsNoArg.Add("spinner", () =>
            {
                UIManager.Instance.SetWaitingtState(!Scene.LoadingSpinner.IsActive.Value);
            });
        }

        /// <summary>
        /// Change Main Screen
        /// </summary>
        /// <param name="t"></param>
        public void ChangeMainView(Type t)
        {
            Scene.SubViewSwitcher.SwitchTo(0, false);

            LastMainView = Scene.MainViewSwitcher.ActiveView;
            if (t == null)
            {
                Scene.MainViewSwitcher.SwitchTo(0, false);
            }
            else
            {
                Scene.MainViewSwitcher.SwitchTo(UIManager.Instance.Scene.MainViews[t], false);
            }
        }

        /// <summary>
        /// Change Main Screen
        /// </summary>
        /// <param name="t"></param>
        public void ChangeSubView(Type t)
        {
            if (t == null)
            {
                Scene.SubViewSwitcher.SwitchTo(UIManager.Instance.Scene.SubViews[typeof(Region)]);
            }
            else
            {
                Scene.SubViewSwitcher.SwitchTo(UIManager.Instance.Scene.SubViews[t]);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="text"></param>
        /// <param name="accept">Answer Result</param>
        public void ShowAskToUser(string text, Action<bool> accept)
        {
            Scene.AreYouSure.Show(text, accept);
        }

        public void ShowWarning(string text, Action ok = null)
        {
            Scene.Warning.Show(text, ok);
        }

        public void ShowToast(string text, float time = 3)
        {
            Debug.Log(text);
            if (Scene != null)
            {
                Common.Core.Invoke(() => {
                    Scene.Toast.Show(text);
                });
            }
            else
                Debug.Log("Scene Null");

        }


        public void ShowToastGameJolt(string text, Sprite s = null)
        {
#if GameJolt
            if (s == null)
                GameJolt.UI.Manager.Instance.QueueNotification(text);
            else
                GameJolt.UI.Manager.Instance.QueueNotification(text, s);
#endif
        }


        public void SetWaitingtState(bool show)
        {
            if (show)
            {
                Scene.LoadingSpinner.Show();
                Debug.Log("Spinner On");
            }
            else
            {
                Scene.LoadingSpinner.Hide();
                Debug.Log("Spinner Off");
            }
        }

        /*public void ShowColorSelector(Action<Color> colorSelected, Action<bool> answer)
        {
            Scene.ColorS.Show(colorSelected, answer);
        }*/

        void LevelLoadingStarted(object index)
        {
            LoadingView.Instance.MainSprite.Value = new SpriteAsset("Loading", LoadingViewSprites.GetRandom());

            ChangeSubView(null);
            ChangeMainView(typeof(LoadingView));
        }

        void LevelLoadingProgressed(object percent)
        {
            if ((float)percent == 100)
            {
                ChangeMainView(typeof(GameView));
            }

            LoadingView.Instance.Percent.Value = "% " + (int)((float)percent * 100);
        }
    }
}