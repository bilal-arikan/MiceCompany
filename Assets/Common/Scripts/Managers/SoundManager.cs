﻿//***********************************************************************//
// Bu kodu başka yerden aldım
//***********************************************************************//
using UnityEngine;
using System.Collections;
using Common.Tools;

namespace Common.Managers
{	
	/// <summary>
	/// This persistent singleton handles sound playing
	/// </summary>
	public partial class SoundManager : Singleton<SoundManager>
	{	
		/// true if the music is enabled	
		public bool MusicOn
        {
            get
            {
                if (backgroundMusic != null)
                    return backgroundMusic.volume > 0;
                else
                    return false;
            }
            set
            {
                if(backgroundMusic != null)
                {
                    if (value)
                        backgroundMusic.volume = MusicVolume;
                    else
                        backgroundMusic.volume = 0;
                }
            }
        }
		/// true if the sound fx are enabled
		public bool SfxOn
        {
            get
            {
                return AudioListener.volume > 0;
            }
            set
            {
                if (value)
                    AudioListener.volume = SfxVolume;
                else
                    AudioListener.volume = 0;
            }
        }


        /// the music volume
        [SerializeField,ReadOnlyPlaying]
        [Range(0,1)]
		float musicVolume=0.3f;
        public float MusicVolume
        {
            get
            {
                return musicVolume;
            }
            set
            {
                musicVolume = value;
                if (backgroundMusic != null)
                    backgroundMusic.volume = value;
            }
        }


        /// the sound fx volume
        [SerializeField,ReadOnlyPlaying]
        [Range(0,1)]
		float sfxVolume=1f;
        public float SfxVolume
        {
            get
            {
                return sfxVolume;
            }
            set
            {
                sfxVolume = value;
                AudioListener.volume = value;
            }
        }

        [SerializeField]
        protected AudioSource backgroundMusic;

        public AudioClip WinAudio;
        public AudioClip LostAudio;

        protected override void Awake()
        {
            base.Awake();
            AudioListener.volume = SfxVolume;
            if (backgroundMusic != null)
                backgroundMusic.volume = MusicVolume;

            // Bölüm Kazanılırsa
            EventManager.StartListening(E.PlayerWon, () =>
            {
                SoundManager.Instance.PlaySound(WinAudio);
            });
            // Bölüm Kaybedilirse
            EventManager.StartListening(E.PlayerLost, () =>
            {
                SoundManager.Instance.PlaySound(LostAudio);
            });
        }

        /// <summary>
        /// Plays a background music.
        /// Only one background music can be active at a time.
        /// </summary>
        /// <param name="Clip">Your audio clip.</param>
        public virtual void PlayBackgroundMusic(AudioSource Music)
		{
			// if the music's been turned off, we do nothing and exit
			if (!MusicOn)
				return;
			// if we already had a background music playing, we stop it
			if (backgroundMusic!=null)
				backgroundMusic.Stop();
			// we set the background music clip
			backgroundMusic=Music;
			// we set the music's volume
			backgroundMusic.volume=MusicVolume;
			// we set the loop setting to true, the music will loop forever
			backgroundMusic.loop=true;
			// we start playing the background music
			backgroundMusic.Play();		
		}	

		public virtual AudioSource GetBackgroundMusic()
		{
			return backgroundMusic;
		}
		
		/// <summary>
		/// Plays a sound
		/// </summary>
		/// <returns>An audiosource</returns>
		/// <param name="Sfx">The sound clip you want to play.</param>
		/// <param name="Location">The location of the sound.</param>
		/// <param name="Volume">The volume of the sound.</param>
		public virtual void PlaySound(AudioClip Sfx, Vector3 Location)
		{
			if (!SfxOn || Sfx == null)
				return;

            AudioSource.PlayClipAtPoint(Sfx, Location);

			// we create a temporary game object to host our audio source
			/*GameObject temporaryAudioHost = new GameObject("TempAudio");
			// we set the temp audio's position
			temporaryAudioHost.transform.position = Location;
			// we add an audio source to that host
			AudioSource audioSource = temporaryAudioHost.AddComponent<AudioSource>() as AudioSource; 
			// we set that audio source clip to the one in paramaters
			audioSource.clip = Sfx; 
			// we set the audio source volume to the one in parameters
			audioSource.volume = SfxVolume;
			// we start playing the sound
			audioSource.Play(); 
			// we destroy the host after the clip has played
			Destroy(temporaryAudioHost, Sfx.length);
			// we return the audiosource reference*/
		}

        /// <summary>
        /// Plays a sound on Camera position
        /// </summary>
        /// <returns>An audiosource</returns>
        /// <param name="Sfx">The sound clip you want to play.</param>
        /// <param name="Location">The location of the sound.</param>
        /// <param name="Volume">The volume of the sound.</param>
        public virtual void PlaySound(AudioClip Sfx)
        {
            if (!SfxOn || Sfx == null)
                return;

            AudioSource.PlayClipAtPoint(Sfx, Camera.main.transform.position);

            // we create a temporary game object to host our audio source
            /*GameObject temporaryAudioHost = new GameObject("TempAudio");
			// we set the temp audio's position
			temporaryAudioHost.transform.position = Location;
			// we add an audio source to that host
			AudioSource audioSource = temporaryAudioHost.AddComponent<AudioSource>() as AudioSource; 
			// we set that audio source clip to the one in paramaters
			audioSource.clip = Sfx; 
			// we set the audio source volume to the one in parameters
			audioSource.volume = SfxVolume;
			// we start playing the sound
			audioSource.Play(); 
			// we destroy the host after the clip has played
			Destroy(temporaryAudioHost, Sfx.length);
			// we return the audiosource reference*/
        }


    }
}