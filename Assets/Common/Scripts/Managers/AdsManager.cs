﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
#if Admob
using GoogleMobileAds;
using GoogleMobileAds.Api;
#endif
using UnityEngine;
using Common.Tools;
using System.Collections;

namespace Common.Managers
{
    public partial class AdsManager : Singleton<AdsManager>
    {
        public bool InTesting = false;
        public string AdmobAppId = "ca-app-pub-5742130512939823~8349211623";
        public Action<AdType, EventType> FullDebugAction;
#if Admob
        public AdPosition BannerShowPosition = AdPosition.Bottom;
#endif
        protected bool IsInited
        {
            get
            {
#if Admob
                return (bannerView != null && interstitialView != null && rewardView != null );
#else
                return false;
#endif
            }
        }
        protected string DeviceID
        {
            get
            {
                return SystemInfo.deviceUniqueIdentifier;
            }
        }

        [SerializeField]
        protected string UnitIDBanner = "";//"ca-app-pub-5742130512939823/6896451898";
        [SerializeField]
        protected string UnitIDInterstitial = "";//"ca-app-pub-5742130512939823/7005182970";
        [SerializeField]
        protected string UnitIDRewardVideo = "";//"ca-app-pub-5742130512939823/7144574347";

        public enum AdType
        {
            Banner,
            Interstitial,
            Reward
        }
        public enum EventType
        {
            Loaded,
            FailedToLoad, // Called when an ad request failed to load.
            Opening, // Called when an ad is clicked.
            Closed, // Called when the user returned from the app after an ad click.
            LeavingApp, // Called when the ad click caused the user to leave the application.
            Started,
            Rewarded
        }

#if Admob
        protected BannerView bannerView;
        protected InterstitialAd interstitialView;
        protected RewardBasedVideoAd rewardView;

        protected AdFailedToLoadEventArgs lastErrorBanner;
        protected AdFailedToLoadEventArgs lastErrorInters;
        protected AdFailedToLoadEventArgs lastErrorReward;
        protected AdRequest request;
        protected AdRequest.Builder builder;

        protected EventHandler<EventArgs> _started;
        protected EventHandler<Reward> _rewarded;

        void Start()
        {
            Init(InTesting);
        }

        public virtual void Init(bool inTesting)
        {
            MobileAds.Initialize(AdmobAppId);

            InTesting = inTesting;
            if (InTesting)
            {
                builder = new AdRequest.Builder()
                    .AddTestDevice(AdRequest.TestDeviceSimulator)
                    .AddTestDevice(DeviceID);
            }
            else
            {
                builder = new AdRequest.Builder();
            }
            request = builder.Build();


            InitBanner(AdSize.Banner, BannerShowPosition);
            InitInterstitial();
            InitReward();
        }


        public virtual void InitBanner(AdSize size, AdPosition pos)
        {
            //----------------------------------------------------------------------------------------------------------
            // Create a 320x50 banner at the top of the screen.
            if (bannerView != null)
                bannerView.Destroy();
            bannerView = new BannerView(UnitIDBanner, size, pos);
            /*bannerView.OnAdLoaded += delegate { OnAdEvent(AdType.Banner, EventType.Loaded); };
            bannerView.OnAdFailedToLoad += delegate (object o, AdFailedToLoadEventArgs e)
            {
                lastErrorBanner = e;
                OnAdEvent(AdType.Banner, EventType.FailedToLoad);
            };
            bannerView.OnAdOpening += delegate { OnAdEvent(AdType.Banner, EventType.Opening); };
            bannerView.OnAdClosed += delegate { OnAdEvent(AdType.Banner, EventType.Closed); };
            bannerView.OnAdLeavingApplication += delegate { OnAdEvent(AdType.Banner, EventType.LeavingApp); };*/

            bannerView.Hide();
            bannerView.LoadAd(request);
        }

        public virtual void InitInterstitial()
        {
            //----------------------------------------------------------------------------------------------------------
            if (interstitialView != null)
                interstitialView.Destroy();
            interstitialView = new InterstitialAd(UnitIDInterstitial);
            /*interstitialView.OnAdLoaded += delegate { OnAdEvent(AdType.Interstitial, EventType.Loaded); };
            interstitialView.OnAdFailedToLoad += delegate (object o, AdFailedToLoadEventArgs e)
            {
                lastErrorInters = e;
                OnAdEvent(AdType.Interstitial, EventType.FailedToLoad);
            };
            interstitialView.OnAdOpening += delegate { OnAdEvent(AdType.Interstitial, EventType.Opening); };
            interstitialView.OnAdClosed += delegate { OnAdEvent(AdType.Interstitial, EventType.Closed); };
            interstitialView.OnAdLeavingApplication += delegate { OnAdEvent(AdType.Interstitial, EventType.LeavingApp); };*/

            interstitialView.LoadAd(request);
        }

        public virtual void InitReward()
        {
            //----------------------------------------------------------------------------------------------------------
            if (rewardView != RewardBasedVideoAd.Instance)
            {
                rewardView = RewardBasedVideoAd.Instance;
                /*rewardView.OnAdLoaded += delegate { OnAdEvent(AdType.Reward, EventType.Loaded); };
                rewardView.OnAdFailedToLoad += delegate (object o, AdFailedToLoadEventArgs e)
                {
                    lastErrorReward = e;
                    OnAdEvent(AdType.Reward, EventType.FailedToLoad);
                };
                rewardView.OnAdOpening += delegate { OnAdEvent(AdType.Reward, EventType.Opening); };
                rewardView.OnAdClosed += delegate { OnAdEvent(AdType.Reward, EventType.Closed); };
                rewardView.OnAdLeavingApplication += delegate { OnAdEvent(AdType.Reward, EventType.LeavingApp); };
                rewardView.OnAdRewarded += delegate { OnAdEvent(AdType.Reward, EventType.Rewarded); };
                rewardView.OnAdStarted += delegate { OnAdEvent(AdType.Reward, EventType.Started); };*/

                rewardView.LoadAd(request, UnitIDRewardVideo);
            }
            else
            {
                Debug.LogError("Reward Couldnt Initialized !!!");
            }

        }
#endif


        public virtual void TryShowBanner()
        {
#if Admob
            if (!enabled)
            {
                Debug.Log("BannerShow: AdsManager Disabled");
                return;
            }

            if (bannerView != null)
            {
                bannerView.Show();
                StartCoroutine(LoadCo(AdType.Banner));
            }
            else
            {
                Debug.LogError("BannerShow NULL");
            }
#endif
        }

        public virtual bool TryShowInterstitial(Action<bool> success = null)
        {
            if (!enabled)
            {
                Debug.Log("InterstitialShow: AdsManager Disabled");
                if (success != null)
                    success(false);
                return false;
            }
#if Admob
            if (interstitialView != null)
            {
                if (interstitialView.IsLoaded())
                {
                    interstitialView.Show();

                    StartCoroutine(LoadCo(AdType.Interstitial));

                    if (success != null)
                        success(true);
                    return true;
                }
                else
                {
                    //interstitialView.LoadAd(request);
                    Debug.LogWarning("Intestitial Not Loaded, Please Use InitInterstitial()");
                    interstitialView.LoadAd(request);
                }
            }
            else
            {
                Debug.LogError("InterstitialShow NULL");
            }
#endif
            if (success != null)
                success(false);
            return false;
        }

        public virtual bool TryShowReward(Action<EventArgs> started, Action<int> rewarded)
        {
#if UNITY_EDITOR && Admob
            started.Invoke(new EventArgs() { });
            rewarded.Invoke(10);
            return true;
#elif Admob
            if (rewardView != null)
            {
                if (_started != null)
                    rewardView.OnAdStarted -= _started;
                if (_rewarded != null)
                    rewardView.OnAdRewarded -= _rewarded;

                _started = delegate (object o, EventArgs e) { 
                    started.Invoke(e); 
                };
                _rewarded = delegate (object o, Reward e) {
                    rewarded.Invoke((int)e.Amount);
                    StartCoroutine(LoadCo(AdType.Reward));
                }; ;

                if (_started != null)
                    rewardView.OnAdStarted += _started;
                if (_rewarded != null)
                    rewardView.OnAdRewarded += _rewarded;

                if (rewardView.IsLoaded())
                {
                    rewardView.Show();
                    return true;
                }
                else
                {
                    Debug.LogWarning("Reward Not Loaded, Please Use InitReward()");
                    rewardView.LoadAd(request, UnitIDRewardVideo);
                    return false;
                }
            }
            else
            {
                Debug.LogError("RewardShow NULL");
                InitReward();
                return false;
            }
#else
            return false;
#endif
        }


        public virtual void HideBanner()
        {
#if Admob
            if (bannerView != null)
            {
                bannerView.Hide();
            }
            else
            {
                Debug.LogError("BannerHide NULL");
            }
#endif
        }

#if Admob
        IEnumerator LoadCo(AdType type)
        {
            yield return new WaitForSeconds(3);

            if(type == AdType.Banner)
            {
                bannerView.LoadAd(request);
            }
            else if(type == AdType.Interstitial)
            {
                interstitialView.LoadAd(request);
            }
            else if(type == AdType.Reward)
            {
                rewardView.LoadAd(request, UnitIDRewardVideo);
            }
        }

        /// <summary>
        /// Every action Calls this method
        /// </summary>
        /// <param name="adType"></param>
        /// <param name="eventType"></param>
        protected virtual void OnAdEvent(AdType adType, EventType eventType)
        {
            if (FullDebugAction != null)
                FullDebugAction.Invoke(adType, eventType);

            if (adType == AdType.Banner)
            {
                if (eventType == EventType.FailedToLoad)
                {
                    Debug.LogError("BannerError: "+ lastErrorBanner != null ? lastErrorBanner.Message : "");
                }
            }
            else if (adType != AdType.Interstitial)
            {
                if (eventType == EventType.FailedToLoad)
                {
                    Debug.LogError("InterstitialError: " + lastErrorInters != null ? lastErrorInters.Message : "");
                }
            }
            else if (adType != AdType.Reward)
            {
                if (eventType == EventType.FailedToLoad)
                {
                    Debug.LogError("RewardError: " + lastErrorReward != null ? lastErrorReward.Message : "");
                }
            }
        }
#endif

    }
}
//*/
