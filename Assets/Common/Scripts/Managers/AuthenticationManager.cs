﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
#if FirebaseAuthentication
using Firebase.Auth;
#endif
using Common.Tools;
using Common.Data;
using System.Collections;

namespace Common.Managers
{
    public partial class AuthenticationManager : Singleton<AuthenticationManager>
    {
        public enum SignMethod
        {
            Email, Anonym, GameJolt, Google, Facebook, Twitter, Github, Register
        }

        public enum MergeMethod
        {
            DontMerge, MainIsNew, MainIsOld
        }

        [SerializeField]
        public string GoogleClientToken = "254438068595-mrvnkdgn2kr4nhu5pd6e90j3nlvejkho.apps.googleusercontent.com";
        public string GoogleIdToken
        {
            get
            {
#if GooglePlayGames
                return GooglePlayGames.PlayGamesPlatform.Instance.GetIdToken();
#else
                return "";
#endif
            }
        }
        public string FacebookAccessToken
        {
            get
            {
#if Facebook
                return Facebook.Unity.AccessToken.CurrentAccessToken.TokenString;
#else
                return "";
#endif
            }
        }
        [SerializeField]
        public string TwitterIdToken;
        [SerializeField]
        public string TwitterSecret;
        [SerializeField]
        public string GithubIdToken;

        public bool UseEmailVerificationForRegister = false;
        public bool AutoAnonymLogin = true;
        public bool AutoSignOut = false;
        public static string CurrentUserId
        {
            get
            {
#if FirebaseAuthentication
                return FirebaseAuth.DefaultInstance.CurrentUser != null ? FirebaseAuth.DefaultInstance.CurrentUser.UserId : "null";
#else
                return "null";
#endif
            }
        }

        public static bool UserIsValid
        {
            get
            {
#if FirebaseAuthentication && UNITY_EDITOR
                return CurrentUser != null;
#elif FirebaseAuthentication
                return CurrentUser != null && !CurrentUser.IsAnonymous;
#else
                return false;
#endif
            }
        }
        public static bool IsInitialized
        {
            get
            {
#if FirebaseAuthentication
                return FirebaseAuth.DefaultInstance != null;
#else
                return false;
#endif
            }
        }

#if FirebaseAuthentication
        public static FirebaseUser CurrentUser
        {
            get
            {
                return FirebaseAuth.DefaultInstance.CurrentUser;
            }
        }

        public void Start()
        {
            Init();

        }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="signAnonym">If signed in Automatically sign anonymous</param>
        public virtual void Init()
        {
            if(AutoSignOut && FirebaseAuth.DefaultInstance.CurrentUser != null)
            {
                FirebaseAuth.DefaultInstance.SignOut();
            }
            else if (FirebaseAuth.DefaultInstance.CurrentUser != null)
            {
                EventManager.TriggerObjectEvent(E.FirebaseSignIn, CurrentUser.UserId);
                foreach(var data in CurrentUser.ProviderData)
                {
                    Debug.LogWarning("Firebase Sağlayıcıları:" + data.ProviderId);
                    if (data.ProviderId == "google.com")
                    {
                        PlayerManager.CurrentProfile = Profile.Create(data);
                        break;
                    }
                    else if (data.ProviderId == "facebook.com")
                    {
                        PlayerManager.CurrentProfile = Profile.Create(data);
                        break;
                    }
                }
                Debug.Log("Firebase User Already Signed In " + FirebaseAuth.DefaultInstance.CurrentUser.UserId);
            }
            else if (AutoAnonymLogin)
            {
                SignIn(SignMethod.Anonym, null, null, delegate { Debug.Log("Anonym SignedIn"); });
            }

            // Kullanıcı giriş ve çıkışını yakalar
            /*FirebaseAuth.DefaultInstance.StateChanged += (object sender, System.EventArgs eventArgs) =>
            {
                if (FirebaseAuth.DefaultInstance.CurrentUser != null)
                {
                    Debug.Log("Auth In Event:" + E.FirebaseSignIn);
                    EventManager.TriggerObjectEvent(E.FirebaseSignIn, CurrentUser.UserId);
                }
                else
                {
                    Debug.Log("Auth Out Event:" + E.FirebaseSignOut);
                    EventManager.TriggerObjectEvent(E.FirebaseSignOut, CurrentUser.UserId);
                }
            };*/
        }

        /// <summary>
        /// Register Async
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="passwordAgain"></param>
        /// <param name="onCompleted">Called when process completed</param>
        public virtual Credential Register(string email, string password,  Action<AuthResult> completed , bool signInIfExist = true)
        {
            if (completed == null)
                throw new UnityException("Register: completed Action is NULL !");

            OnAuthStarted(SignMethod.Register);

            var cred = EmailAuthProvider.GetCredential(email, password);

            //Hesap oluşturur
            FirebaseAuth.DefaultInstance.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(t =>
            {
                AuthResult isSuccess = OnAuthFinished(t, SignMethod.Register);
                if (isSuccess == AuthResult.EmailAlreadyInUse && signInIfExist)
                {
                    // Kullanıcı mevcutsa giriş yap
                    SignIn(SignMethod.Email,email, password, completed);
                }
                else
                {
                    if (isSuccess == AuthResult.Success)
                        EventManager.TriggerObjectEvent(E.FirebaseSignIn, t.Result.UserId, true);
                    completed.Invoke(isSuccess);
                }
            });
            return cred;
        }

        /// <summary>
        /// SignIn Async
        /// </summary>
        /// <param name="method"></param>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="completed">Called when process completed</param>
        /// <returns>Returns credential (maybe to Merge Users)</returns>
        public virtual Credential SignIn(SignMethod method, string email, string password, Action<AuthResult> completed = null)
        {
            OnAuthStarted(method);

            // Storage Credential
            Credential cred = null;
            switch (method)
            {
                case SignMethod.Email:
                    cred = EmailAuthProvider.GetCredential(email, password);
                    break;
                case SignMethod.Google:
                    cred = GoogleAuthProvider.GetCredential(GoogleIdToken, GoogleClientToken);
                    break;
                case SignMethod.Facebook:
                    cred = FacebookAuthProvider.GetCredential(FacebookAccessToken);
                    break;
                case SignMethod.Twitter:
                    cred = TwitterAuthProvider.GetCredential(TwitterIdToken, TwitterSecret);
                    break;
                case SignMethod.Github:
                    cred = GitHubAuthProvider.GetCredential(GithubIdToken);
                    break;
            }


            switch (method)
            {
                //////////////////////////////////////////////////////////////////////////
                case SignMethod.Email:
                    FirebaseAuth.DefaultInstance.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
                        {
                            AuthResult isSuccess = OnAuthFinished(task, method);
                            if(isSuccess == AuthResult.Success)
                                EventManager.TriggerObjectEvent(E.FirebaseSignIn, task.Result.UserId);
                            if (completed != null)
                                completed.Invoke(isSuccess);
                        });

                    break;
                //////////////////////////////////////////////////////////////////////////
                case SignMethod.Anonym:
                    FirebaseAuth.DefaultInstance.SignInAnonymouslyAsync().ContinueWith(task =>
                    {
                        AuthResult isSuccess = OnAuthFinished(task, method);
                        if (isSuccess == AuthResult.Success)
                            EventManager.TriggerObjectEvent(E.FirebaseSignIn, task.Result.UserId);
                        if (completed != null)
                            completed.Invoke(AuthResult.Success);

                    });
                    break;
                //////////////////////////////////////////////////////////////////////////
                case SignMethod.Google:
                    if (cred != null)
                        FirebaseAuth.DefaultInstance.SignInWithCredentialAsync(cred).ContinueWith(task =>
                        {
                            /*task.Result.TokenAsync(false).ContinueWith((s) => {
                                CConsole.Log("Token:" + s.Result);
                            });*/
                            foreach (var d in task.Result.ProviderData)
                                CConsole.Log("PD: " + d.Email + " : " + d.DisplayName + " : " + d.ProviderId + " : " + d.PhotoUrl);

                            AuthResult isSuccess = OnAuthFinished(task, method);
                            if (isSuccess == AuthResult.Success)
                                EventManager.TriggerObjectEvent(E.FirebaseSignIn, task.Result.UserId);
                            if (completed != null)
                                completed.Invoke(isSuccess);
                        });
                    break;
                //////////////////////////////////////////////////////////////////////////
                case SignMethod.Facebook:
                    if (cred != null)
                        FirebaseAuth.DefaultInstance.SignInWithCredentialAsync(cred).ContinueWith(task =>
                        {
                            foreach (var d in task.Result.ProviderData)
                                CConsole.Log("PD: " + d.Email + " : " + d.DisplayName + " : " + d.ProviderId + " : " + d.PhotoUrl);

                            AuthResult isSuccess = OnAuthFinished(task, method);
                            if (isSuccess == AuthResult.Success)
                                EventManager.TriggerObjectEvent(E.FirebaseSignIn, task.Result.UserId);
                            if (completed != null)
                                completed.Invoke(isSuccess);
                        });
                    break;
                //////////////////////////////////////////////////////////////////////////
                case SignMethod.Twitter:
                    if (cred != null)
                        FirebaseAuth.DefaultInstance.SignInWithCredentialAsync(cred).ContinueWith(task =>
                        {
                            AuthResult isSuccess = OnAuthFinished(task, method);
                            if (isSuccess == AuthResult.Success)
                                EventManager.TriggerObjectEvent(E.FirebaseSignIn, task.Result.UserId);
                            if (completed != null)
                                completed.Invoke(isSuccess);
                        });
                    break;
                //////////////////////////////////////////////////////////////////////////
                case SignMethod.Github:
                    if(cred != null)
                        FirebaseAuth.DefaultInstance.SignInWithCredentialAsync(cred).ContinueWith(task =>
                        {
                            AuthResult isSuccess = OnAuthFinished(task, method);
                            if (isSuccess == AuthResult.Success)
                                EventManager.TriggerObjectEvent(E.FirebaseSignIn, task.Result.UserId);
                            if (completed != null)
                                completed.Invoke(isSuccess);
                        });
                    break;
                //////////////////////////////////////////////////////////////////////////
            }

            return cred;
        }

        public virtual void SignOut()
        {
            string id = CurrentUser != null ? CurrentUser.UserId : "null";
            FirebaseAuth.DefaultInstance.SignOut();
            EventManager.TriggerObjectEvent(E.FirebaseSignOut, id);
        }

        /// <summary>
        /// called when a sign method called
        /// Herhangi Kayıt veya Giriş işlemi başladığında bu fonksiyon çağırılır
        /// </summary>
        /// <param name="job"></param>
        protected virtual void OnAuthStarted(SignMethod job)
        {
            // Yükleniyor pencerisini gösterir
            //UIManager.Instance.SetWaitingtState(true);
        }

        /// <summary>
        /// called when any SignIn or Register process finished
        /// Herhangi Kayıt veya Giriş işlemi bittiğinde bu fonksiyon çağırılır
        /// </summary>
        /// <param name="task"></param>
        /// <param name="method"></param>
        protected virtual AuthResult OnAuthFinished(Task<FirebaseUser> task, SignMethod method)
        {
            // Yülkleniyor ekranını kapatır
            //UIManager.Instance.SetWaitingtState(false);

                    
            if (task.IsCanceled)
            {
                Debug.LogError(method + ": Cancelled");
                return AuthResult.ServerCanceled;
            }
            if (task.IsFaulted)
            {
                string error = "";
                //error += method.ToString() + task.Exception + "\n";
                foreach (Firebase.FirebaseException e in task.Exception.InnerExceptions)
                {
                    error += e.Message + "_" + e.ErrorCode + "#" + e.Data;
                    foreach (object i in e.Data.Keys)
                    {
                        error += " (" + e.Data[i] + ")";
                    }
                }
                Debug.LogError(method + ": " + error);
                return (AuthResult)((Firebase.FirebaseException)task.Exception.InnerExceptions[0]).ErrorCode;
            }

            if (task.IsCompleted)
            {
                switch (method)
                {
                    //////////////////////////////////////////////////////////////////////////
                    case SignMethod.Email:
                        if (UseEmailVerificationForRegister && !task.Result.IsEmailVerified)
                        {
                            FirebaseAuth.DefaultInstance.SignOut();
                            return AuthResult.EmailNeedsVerification;
                        }
                        break;
                    //////////////////////////////////////////////////////////////////////////
                    case SignMethod.Anonym:
                        break;
                    //////////////////////////////////////////////////////////////////////////
                    case SignMethod.Register:
                        if (UseEmailVerificationForRegister)
                        {
                            SendVerificationMail(task.Result,null);
                            return AuthResult.EmailNeedsVerification;
                        }
                        break;
                    //////////////////////////////////////////////////////////////////////////
                    case SignMethod.Facebook:
                    case SignMethod.Google:
                    case SignMethod.Twitter:
                    case SignMethod.Github:
                        break;
                }
            }

            //CConsole.Log("Firebase SignIn Succesful:" + CurrentUser.UserId + " : "+ CurrentUser.ProviderId, Color.green);

            return AuthResult.Success;
        }



        /// <summary>
        /// Onaylama epostası gönderir (eposta biraz geç geliyo)
        /// </summary>
        /// <param name="user"></param>
        protected virtual void SendVerificationMail(FirebaseUser user, Action<bool> completed)
        {
            Debug.Log("Verification Mail Sending...");

            user.SendEmailVerificationAsync().ContinueWith(task =>
            {
                if (completed != null) //completed null sa gerisine bakmanın anlamı yok
                {
                    if (task.IsCanceled)
                    {
                        completed.Invoke(false);
                    }
                    else if (task.IsFaulted)
                    {
                        Debug.LogError("SendEmailVerificationAsync error: " + task.Exception);
                        if (task.Exception.InnerExceptions.Count > 0)
                            Debug.Log(task.Exception.InnerExceptions[0].Message);
                        completed.Invoke(false);

                    }
                    else
                    {
                        Debug.Log("Check Emails to verifiy");
                        completed.Invoke(true);
                    }
                }
            });
        }

        /// <summary>
        /// Merg user infos to a Standart User
        /// Mesela Anonim şekilde oyun ilerledikten sonra Yeni bi hesap açtıysa 2 hesabı birleştirmek için
        /// </summary>
        /// <param name="newUser"></param>
        protected virtual void MergeUser(FirebaseUser oldUser, Credential toMerg, Action<bool> completed, bool IsFirstUserMain)
        {
            if (toMerg == null && completed != null)
                completed.Invoke(false);

            string firstId = CurrentUser.UserId;
            

            if (IsFirstUserMain)
            {
                oldUser.LinkWithCredentialAsync(toMerg).ContinueWith(t =>
                {
                    if (completed != null)
                    {
                        if (t.IsCanceled || t.IsFaulted)
                        {
                            completed.Invoke(false);
                        }
                        else
                        {
                            Debug.Log("Merge1: " + firstId + " : " + toMerg.Provider + " : " + t.Result.UserId);
                            completed.Invoke(true);
                        }
                    }
                });
            }
            else
            {
                oldUser.LinkAndRetrieveDataWithCredentialAsync(toMerg).ContinueWith(t =>
                {
                    if (completed != null)
                    {
                        if (t.IsCanceled || t.IsFaulted)
                        {
                            completed.Invoke(false);
                        }
                        else
                        {
                            Debug.Log("Merge2: " + firstId + " : " + toMerg.Provider + " : " + t.Result.User.UserId + " : "+ ObjectSerializer.ToJson(t.Result.Info));
                            completed.Invoke(true);
                        }
                    }
                });
            }

        }

        /// <summary>
        /// Kullanıcının DisplayName ve PhotoUrl bilgisini güncelleyebilirisin
        /// </summary>
        /// <param name="name"></param>
        /// <param name="surname"></param>
        /// <param name="completed"></param>
        public virtual void UpdateProfile(string displayName, string photoUrl, Action<bool> completed)
        {
            UserProfile profile = new UserProfile();
            profile.DisplayName = displayName;
            if (!string.IsNullOrEmpty(photoUrl))
                profile.PhotoUrl = new Uri(photoUrl);


            FirebaseAuth.DefaultInstance.CurrentUser.UpdateUserProfileAsync(profile).ContinueWith((t) => {
                if(completed != null)
                {
                    if (t.IsCanceled || t.IsFaulted)
                        completed.Invoke(false);
                    else
                        completed.Invoke(true);
                }
            });
        }

        /// <summary>
        /// Kullanıcının şifresini değiştirmek için
        /// </summary>
        /// <param name="pass"></param>
        /// <param name="passAgain"></param>
        /// <param name="completed"></param>
        public virtual void UpdatePassword(string pass, string passAgain, Action<bool> completed)
        {

            FirebaseAuth.DefaultInstance.CurrentUser.UpdatePasswordAsync(pass).ContinueWith((t) =>
            {
                if (completed != null)
                {
                    if (t.IsCanceled || t.IsFaulted)
                        completed.Invoke(false);
                    else
                        completed.Invoke(true);
                }
            });

        }
#endif
        /////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Also Using for client side Email and Password verification
        /// Giriş veya Kayıt yapmaya çalıştığında, Sonucu(Başarılı mı? sorun mu çıktı? bilgisi) verir
        /// </summary>
        public enum AuthResult
        {
            Success = 0,
            EmailInvalid = 1,
            EmailNeedsVerification = 2,
            PasswordInvalid = 3,
            PasswordTwoNotEqual = 4,
            PasswordSmall = 5,
            PasswordNoLettersAndNumbers = 6,
            ServerCanceled = 7,
            WrongPassword = 12,
            UserNotFound = 14,
            WeakPassword = 23,
            EmailAlreadyInUse = 8,
            CredentialNull = 77,
            Unknown = 100
        }
    }
}
//*/
/*
 * 1("ERROR_INVALID_CUSTOM_TOKEN", "The custom token format is incorrect. Please check the documentation."));
    2("ERROR_CUSTOM_TOKEN_MISMATCH", "The custom token corresponds to a different audience."));
    3("ERROR_INVALID_CREDENTIAL", "The supplied auth credential is malformed or has expired."));
    4("ERROR_INVALID_EMAIL", "The email address is badly formatted."));
    5-12("ERROR_WRONG_PASSWORD", "The password is invalid or the user does not have a password."));
    6("ERROR_USER_MISMATCH", "The supplied credentials do not correspond to the previously signed in user."));
    7("ERROR_REQUIRES_RECENT_LOGIN", "This operation is sensitive and requires recent authentication. Log in again before retrying this request."));
    8("ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL", "An account already exists with the same email address but different sign-in credentials. Sign in using a provider associated with this email address."));
    9-8("ERROR_EMAIL_ALREADY_IN_USE", "The email address is already in use by another account."));
    10("ERROR_CREDENTIAL_ALREADY_IN_USE", "This credential is already associated with a different user account."));
    11("ERROR_USER_DISABLED", "The user account has been disabled by an administrator."));
    12("ERROR_USER_TOKEN_EXPIRED", "The user\'s credential is no longer valid. The user must sign in again."));
    13-14("ERROR_USER_NOT_FOUND", "There is no user record corresponding to this identifier. The user may have been deleted."));
    14("ERROR_INVALID_USER_TOKEN", "The user\'s credential is no longer valid. The user must sign in again."));
    15("ERROR_OPERATION_NOT_ALLOWED", "This operation is not allowed. You must enable this service in the console."));
    16-23("ERROR_WEAK_PASSWORD", "The given password is invalid."));
*/