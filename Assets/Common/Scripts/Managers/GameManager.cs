﻿//***********************************************************************//
// Bu kodu başka yerden aldım :D 
//***********************************************************************//
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Common.Tools;
using Common.Data;
using System;

namespace Common.Managers
{

    /// <summary>
    /// The game manager is a persistent singleton that handles points and time
    /// </summary>
    public partial class GameManager : Singleton<GameManager> 
    {
        /// the current time scale
        [ReadOnly]
		public float TimeScale = 1f;

        /// the various states the game can be in
        public enum GameStatus { MainMenu,BeforeGameStart, GameInProgress, Paused, GameOver, LifeLost };

        /// the current status of the game
        [SerializeField,ReadOnly]
        protected GameStatus _status;
        public GameStatus Status
        {
            get
            {
                return _status;
            }
            protected set
            {
                _status = value;
            }
        }

        public delegate void GameManagerInspectorRedraw();
        // Declare the event to which editor code will hook itself.
        public event GameManagerInspectorRedraw GameManagerInspectorNeedRedraw;


        // storage
        protected float _savedTimeScale;
        protected GameStatus _statusBeforePause;


        /// <summary>
        /// Sets the status. Status can be accessed by other classes to check if the game is paused, starting, etc
        /// </summary>
        /// <param name="newStatus">New status.</param>
        public virtual void SetStatus(GameStatus newStatus)
        {
            E eventToTriggered = E.Unknown;

            if (newStatus == GameStatus.BeforeGameStart )
            {
                eventToTriggered = E.GameOnPreparing;
            }
            else if (newStatus == GameStatus.GameInProgress && Status == GameStatus.BeforeGameStart)
            {
                eventToTriggered = E.GameStarted;
            }
            else if (newStatus == GameStatus.GameOver)
            {
                eventToTriggered = E.GameFinished;
            }
            else if (newStatus == GameStatus.Paused)
            {
                eventToTriggered = E.GamePaused;
            }
            else if (newStatus == GameStatus.GameInProgress && Status == GameStatus.Paused)
            {
                eventToTriggered = E.GameUnPaused;
            }

            Status = newStatus;
            if (GameManagerInspectorNeedRedraw != null) { GameManagerInspectorNeedRedraw(); }

            if(eventToTriggered != E.Unknown)
                EventManager.TriggerEvent(eventToTriggered);
        }

        public virtual void SetTimeScale(float newTimeScale)
        {
            _savedTimeScale = Time.timeScale;
            TimeScale = newTimeScale;
            Time.timeScale = newTimeScale;
        }

        public virtual void ResetTimeScale()
        {
            Time.timeScale = _savedTimeScale;
        }
        
        public virtual void Pause()
        {
            // if time is not already stopped		
            if (Status != GameStatus.Paused)
            {
                SetTimeScale(0.0f);
                _statusBeforePause = Status;
                SetStatus(GameStatus.Paused);
                EventManager.TriggerEvent(E.GamePaused);
            }
        }
        
        public virtual void UnPause()
        {
            ResetTimeScale();
            SetStatus(_statusBeforePause);
            EventManager.TriggerEvent(E.GameUnPaused);
        }
    }
}