﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using Common.Tools;
#if GooglePlayGames
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#endif
#if GameJolt
using GameJolt.API;
using GameJolt.API.Objects;
using System.Linq;
using Common.Data;
#endif
#if Facebook
using Facebook.Unity;
#endif


namespace Common.Managers
{
    public partial class SocialManager : Singleton<SocialManager>
    {
       public enum Network
        {
            GooglePlay,
            GameJolt,
            Facebook,
            Firebase
        }

        public List<String> FacebookReadPermissions = new List<string>();
        public List<String> FacebookPublishPermissions = new List<string>();
        public string FacebookQuery = "/me?fields=id,name,email,picture,friends";
        public bool GooglePlayLogEnabled = false;
        [Tooltip("iOS App ID (number), example: 1122334455")]
        public string iOSAppID = "";

        public bool IsSignedInGooglePlay
        {
            get
            {
#if GooglePlayGames
                return (PlayGamesPlatform.Instance != null && PlayGamesPlatform.Instance.IsAuthenticated());
#else
                return false;
#endif
            }
        }
        public bool IsSignedInGameJolt
        {
            get
            {
#if GameJolt
                return Manager.Instance != null && Manager.Instance.CurrentUser != null;
#else
                return false;
#endif
            }
        }
        public bool IsSignedInFacebook
        {
            get
            {
#if Facebook
                return FB.IsInitialized && FB.IsLoggedIn;
#else
                return false;
#endif
            }
        }
        public string FB_API_Url
        {
            get
            {
#if Facebook
                return "http://graph.facebook.com/" + Facebook.Unity.AccessToken.CurrentAccessToken.UserId;
#else
                return "";
#endif
            }
        }

        void Start()
        {
            StartCoroutine(Init());
        }

        public IEnumerator Init()
        {
            yield return null;
#if FirebaseAuthentication
            int tryCount = 0;
            while (!AuthenticationManager.IsInitialized && tryCount < 10)
            {
                tryCount++;
                Debug.LogWarning("FirebaseAuth Null " + tryCount);
                yield return new WaitForSecondsRealtime(1);
            }
#endif
#if GooglePlayGames
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
                // requests the email address of the player be available. Will bring up a prompt for consent.
                .RequestEmail()
                // requests a server auth code be generated so it can be passed to an
                //  associated back end server application and exchanged for an OAuth token.
                //.RequestServerAuthCode(false)
                // requests an ID token be generated.  This OAuth token can be used to
                //  identify the player to other services such as Firebase.
                .RequestIdToken()
                .Build();

            PlayGamesPlatform.InitializeInstance(config);
            // recommended for debugging:
            PlayGamesPlatform.DebugLogEnabled = GooglePlayLogEnabled;
            // Activate the Google Play Games platform
            PlayGamesPlatform.Activate();
#endif
#if GameJolt
            // Gets GameJolt Achievemetns.  Trophy = Achievement
            Trophies.Get((Trophy[] trophies) =>
            {
                if (trophies != null)
                {
                    foreach (var trophy in trophies)
                    {
                        Debug.Log(string.Format("> {0} - {1} - {2} - {3}Unlocked", trophy.Title, trophy.ID, trophy.Difficulty, trophy.Unlocked ? "" : "Not "));
                    }
                    Debug.Log(string.Format("Found {0} trophies.", trophies.Length));
                }
            });
#endif
#if Facebook
            FB.Init( ()=> {
                if (FB.IsInitialized)
                {
                    FB.ActivateApp();
                    if (FB.IsLoggedIn)
                    {
                        //PlayerManager.CurrentPlayer.PhotoUrl = FB_API_Url + "/picture";
                        OnConnected(Network.Facebook, (s) => { });
                    }
                }
                else
                    Debug.LogWarning("SocialInit: Facebook Not Initialized");
            } ,
            (isGameShown)=>
            {
                if (!isGameShown)
                    // Pause the game - we will need to hide
                    Time.timeScale = 0;
                else
                    // Resume the game - we're getting focus again
                    Time.timeScale = 1;
            });
#endif
            Firebase.Messaging.FirebaseMessaging.TokenReceived += delegate (object sender, Firebase.Messaging.TokenReceivedEventArgs token)
            {
                Debug.Log("Firebase:Received Registration Token: " + token.Token);
                EventManager.TriggerObjectEvent(E.ReceivedToken, token.Token);
            };
            Firebase.Messaging.FirebaseMessaging.MessageReceived += delegate (object sender, Firebase.Messaging.MessageReceivedEventArgs e)
            {
                Debug.Log("Firebase:Received a new message from: " + e.Message.RawData);
                EventManager.TriggerObjectEvent(E.ReceivedToken, e.Message);
            };
        }

        /// <summary>
        /// Connect to a Social Network
        /// </summary>
        /// <param name="socialNetwork"></param>
        /// <param name="completed"></param>
        public void Connect(Network socialNetwork, Action<bool> completed)
        {
#if GooglePlayGames
            if (socialNetwork == Network.GooglePlay)
            {
                if (!IsSignedInGooglePlay)
                {
                    PlayGamesPlatform.Instance.Authenticate((s)=> {
                        if (s)
                        {
                            OnConnected(Network.GooglePlay, completed);
                        }
                        else
                        {
                            if (completed != null)
                                completed(false);
                            Debug.LogError("PlayGamesPlatform Connection Failed");
                        }

                    });
                }
                else if (completed != null)
                    completed(true);
            }
#endif
#if GameJolt
            if (socialNetwork == Network.GameJolt)
            {
                if(Manager.Instance.CurrentUser == null || !Manager.Instance.CurrentUser.IsAuthenticated)
                {
                    GameJolt.UI.Manager.Instance.ShowSignIn(
                      (bool signInSuccess) =>
                      {
                      },
                      (bool userFetchedSuccess) =>
                      {
                          if(userFetchedSuccess)
                            OnConnected(Network.GameJolt, completed);
                          else if (completed != null)
                              completed(false);
                      });
                }
                else if (completed != null)
                    completed(true);
            }
#endif
#if Facebook
            if (socialNetwork == Network.Facebook)
            {
                if (!FB.IsInitialized)
                {
                    completed(false);
                    Debug.LogError("Facebook is not initialized");
                }


                if (!FB.IsLoggedIn)
                {
                    FB.LogInWithReadPermissions(FacebookReadPermissions, (r) =>
                    {
                        if (!r.Cancelled && r.Error == null)
                        {
                            OnConnected(Network.Facebook, completed);
                            //Debug.Log("FB_Read: Success " + FB.ClientToken + " : " + Facebook.Unity.AccessToken.urrentAccessToken.UserId);
                        }
                        else
                        {   
                            if(!r.Cancelled)
                                Debug.LogError("FB_Read Failed: " + r.Error);

                            if (completed != null)
                                completed(false);
                        }


                        /*FB.LogInWithPublishPermissions(FacebookPublishPermissions, (p) =>
                        {
                            if (!p.Cancelled && p.Error == null)
                            {
                                completed(true);
                                return;
                            }
                            else
                                Debug.LogError("FB_Publish:" + p.Error);

                            completed(false);

                        });*/

                    });


                }
                else if (completed != null)
                    completed(true);

            }
#endif
        }
        /// <summary>
        /// Disconnect
        /// </summary>
        /// <param name="socialNetwork"></param>
        public void Disconnect(Network socialNetwork)
        {
#if GooglePlayGames
            if (socialNetwork == Network.GooglePlay)
            {
                if (IsSignedInGooglePlay)
                {
                    PlayGamesPlatform.Instance.SignOut();
                }
            }
#endif
#if GameJolt
            if (socialNetwork == Network.GameJolt)
            {
                if (IsSignedInGameJolt)
                {
                    Manager.Instance.CurrentUser.SignOut();
                }
            }
#endif
#if Facebook
            if (socialNetwork == Network.Facebook)
            {
                if (IsSignedInFacebook)
                {
                    FB.LogOut();
                }
            }
#endif
#if FirebaseAuthentication
            AuthenticationManager.Instance.SignOut();
#endif

        }

        /// <summary>
        /// OnConnected Successed
        /// </summary>
        /// <param name="socialNetwork"></param>
        /// <param name="completed"></param>
        private void OnConnected(Network socialNetwork, Action<bool> completed)
        {
            // Diğer Ağlardan çıkış yap
            if (socialNetwork == Network.GooglePlay)
            {
                Disconnect(Network.Facebook);
                Disconnect(Network.GameJolt);
            }
            else if (socialNetwork == Network.GameJolt)
            {
                Disconnect(Network.GooglePlay);
                Disconnect(Network.Facebook);
            }
            else if (socialNetwork == Network.Facebook)
            {
                Disconnect(Network.GooglePlay);
                Disconnect(Network.GameJolt);
            }


#if GooglePlayGames
            if (socialNetwork == Network.GooglePlay)
            {
                //GooglePlay Profil bilgileri kaydedilir
                PlayerManager.CurrentProfile = Profile.Create((PlayGamesLocalUser)PlayGamesPlatform.Instance.localUser);
                EventManager.TriggerObjectEvent(E.PlayerProfileChanged, PlayerManager.CurrentProfile);

                completed(true);
#if FirebaseAuthentication
                AuthenticationManager.Instance.SignIn(AuthenticationManager.SignMethod.Google, null, null, (result) =>
                {
                    completed(result == AuthenticationManager.AuthResult.Success);

                    Debug.Log("Firebase Google SignedIn " + result);
                });
#endif
            }
#endif
#if GameJolt
            if (socialNetwork == Network.GameJolt)
            {
                //GameJolt Profil bilgileri kaydedilir
                PlayerManager.CurrentProfile = Profile.Create(Manager.Instance.CurrentUser);
                EventManager.TriggerObjectEvent(E.PlayerProfileChanged, PlayerManager.CurrentProfile);

                completed(true);

#if FirebaseAuthentication
                // Fireabse e kayıt ol
                AuthenticationManager.Instance.Register(
                    Manager.Instance.CurrentUser.Name.ToLower() + "@gamejolt.com",
                    Manager.Instance.CurrentUser.Name.ToLower(), (resultR) =>
                {

                    completed(resultR == AuthenticationManager.AuthResult.Success);

                    Debug.Log("Firebase GameJolt Registered " + " : " + resultR.ToString());
                },true);
#endif
            }
#endif
#if Facebook
                if (socialNetwork == Network.Facebook)
            {
                FB.API(FacebookQuery, HttpMethod.GET, (callback) =>
                {
                    if (callback.Error != null)
                    {
                        Debug.LogError("FB Api Error: " + callback.Error);
                        completed(false);
                        return;
                    }

                    completed(true);
#if FirebaseAuthentication
                    // Facebook Profil bilgileri kaydedilir
                    PlayerManager.CurrentProfile = Profile.Create(callback);
                    EventManager.TriggerObjectEvent(E.PlayerProfileChanged, PlayerManager.CurrentProfile);

                    AuthenticationManager.Instance.SignIn(AuthenticationManager.SignMethod.Facebook, null, null, (result) =>
                    {
                        completed(result == AuthenticationManager.AuthResult.Success);

                        Debug.Log("Firebase Facebook SignedIn " + result);
                    });
#endif

                });

            }
#endif
                }

        public void SetAchievement(Common.Data.Achievement ach, double value = 100d)
        {
#if GooglePlayGames
            if (IsSignedInGooglePlay && !string.IsNullOrEmpty(ach.GooglePlayId))
            {
                PlayGamesPlatform.Instance.ReportProgress(ach.GooglePlayId, value, (bool success) =>
                {
                     //UIManager.Instance.ShowToast(ach.Name + " achieved(GP) " + success);
                });
            }
#endif
#if GameJolt
            if (IsSignedInGameJolt)
            {
                Trophies.Unlock(ach.GameJoltId, (bool success) =>
                {
                    //UIManager.Instance.ShowToast(ach.Name + " achieved(GJ) " + success);
                });
            }
#endif
            /*Social.ReportProgress(GPGSIds.achievement_firstplay, 100.0f, (bool success) =>
            {
                UIManager.Instance.ShowToast("Ach: firstplay " + success);
                // handle success or failure
            });*/
        }

        public void SetScore(Chapter ch, int totalChScore, string metadata = "")
        {

#if GooglePlayGames
            if (IsSignedInGooglePlay)
            {
                if(!String.IsNullOrEmpty(metadata))
                    PlayGamesPlatform.Instance.ReportScore(totalChScore, ch.GooglePlayScoreTableId, metadata, (bool success) =>
                    {
                        CConsole.Log(ch.Index.ToString() + success);
                    });
                else
                    PlayGamesPlatform.Instance.ReportScore(totalChScore, ch.GooglePlayScoreTableId, (bool success) =>
                    {
                        CConsole.Log(ch.Index.ToString() + success);
                    });
            }
#endif
#if GameJolt
            if (IsSignedInGameJolt)
            {
                Score s = new Score(totalChScore, ch.Index.ToString(), PlayerManager.CurrentProfile.DisplayName, metadata);
                GameJolt.API.Scores.Add(s, ch.GameJoltScoreTableId,  (bool success) =>
                {
                    CConsole.Log(string.Format("Score Add {0}.", success ? "Successful" : "Failed"));
                });
            }
#endif
#if Facebook
            if (IsSignedInFacebook)
            {
                var scoreData = new Dictionary<string, string>() { { ch.Index.ToString(), totalChScore.ToString() } };

                FB.API("/me/scores", HttpMethod.POST, 
                    (r) => {
                        if(!r.Cancelled && r.Error == null)
                            Debug.Log("FB_SetScore Success");
                        else
                            Debug.Log("FB_SetScore Failed: "+r.Error);
                    }, 
                    scoreData);
            }
#endif
        }

        /// <summary>
        /// </summary>
        public void ShowAchievements(Network socialNetwork)
        {
#if GooglePlayGames
            if (socialNetwork == Network.GooglePlay)
            {
                if (IsSignedInGooglePlay)
                {
                    GooglePlayGames.PlayGamesPlatform.Instance.ShowAchievementsUI();
                }
            }
#endif
#if GameJolt
            if (socialNetwork == Network.GameJolt)
            {
                if (IsSignedInGameJolt)
                {
                    GameJolt.UI.Manager.Instance.ShowTrophies();
                }
            }
#endif
#if Facebook
            if (socialNetwork == Network.Facebook)
            {

            }
#endif
        }

        /// <summary>
        /// </summary>
        public void ShowLeaderboard(Network socialNetwork)
        {
#if GooglePlayGames
            if (socialNetwork == Network.GooglePlay)
            {
                if (IsSignedInGooglePlay)
                {
                    GooglePlayGames.PlayGamesPlatform.Instance.ShowLeaderboardUI();
                }
            }
#endif
#if GameJolt
            if (socialNetwork == Network.GameJolt)
            {
                if (IsSignedInGameJolt)
                {
                    GameJolt.UI.Manager.Instance.ShowLeaderboards();
                }
            }
#endif
#if Facebook
            if (socialNetwork == Network.Facebook)
            {

            }
#endif
        }


        public void SendInvite(string title, string msg, string imageUrl, string actionText, string deepLink, Action<int> sendedInvites = null)
        {
            Firebase.Invites.Invite invite = new Firebase.Invites.Invite()
            {
                TitleText = title,
                CustomImageUrl = new Uri(imageUrl),
                MessageText = msg,
                CallToActionText = actionText,
                //GoogleAnalyticsTrackingId = "", //sonra bakılacak
                DeepLinkUrl = new Uri(deepLink)
            };

            // Invite Result
            Firebase.Invites.FirebaseInvites.SendInviteAsync(invite).ContinueWith((s) =>
            {
                if (s.IsCanceled)
                {
                    Debug.Log("Invitation canceled.");
                }
                else if (s.IsFaulted)
                {
                    Debug.LogError("Invitation encountered an error:\n" + s.Exception.ToString());
                }
                else if (s.IsCompleted)
                {
                    Firebase.Invites.SendInviteResult sendResult = s.Result;
                    int count = (sendResult.InvitationIds as List<string>).Count;
                    Debug.Log("SendInvite: " + count + " invites sent successfully.");

                    foreach (string id in sendResult.InvitationIds)
                    {
                        Debug.Log("SendInvite: Invite code: " + id);
                    }

                    if (sendedInvites != null)
                        sendedInvites(count);
                }
            });
        }

        public void TakeAScreenshot(Action<Texture> callback)
        {
            if (callback == null)
                return;

            StartCoroutine(TakeAScreenshotCo(callback));
        }
        IEnumerator TakeAScreenshotCo(Action<Texture> callback)
        {
            yield return new WaitForEndOfFrame();

            var width = Screen.width;
            var height = Screen.height;
            var tex = new Texture2D(width, height, TextureFormat.RGB24, false);
            // Read screen contents into the texture
            tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
            tex.Apply();
            callback(tex);

            /*byte[] screenshot = tex.EncodeToPNG();
            var wwwForm = new WWWForm();
            wwwForm.AddBinaryData("image", screenshot, "Screenshot.png");

            FB.API("me/photos", Facebook.HttpMethod.POST, APICallback, wwwForm);*/
        }


        public void ShareText(string subject,string body, string shareVia = "")
        {
            //execute the below lines if being run on a Android device
#if UNITY_ANDROID
            //Reference of AndroidJavaClass class for intent
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            //Reference of AndroidJavaObject class for intent
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
            //call setAction method of the Intent object created
            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            //set the type of sharing that is happening
            intentObject.Call<AndroidJavaObject>("setType", "text/plain");
            //add data to be passed to the other activity i.e., the data to be sent
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), subject);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), body);
            //get the current activity
            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

            if(string.IsNullOrEmpty(shareVia))
            {
                //start the activity by sending the intent data
                currentActivity.Call("startActivity", intentObject);
            }
            else
            {
                AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, shareVia);
                //start the activity by sending the intent data
                currentActivity.Call("startActivity", jChooser);
            }
#endif

        }

        public void OpenMarketPage()
        {
#if UNITY_IOS
            string url;
            string iOSRatingURI = "itms://itunes.apple.com/us/app/apple-store/{0}?mt=8";

            if (!string.IsNullOrEmpty (iOSAppID)) {
                url = iOSRatingURI.Replace("{0}",iOSAppID);
            }
            else {
                Debug.LogWarning ("Please set iOSAppID variable");
            }
            Application.OpenURL(url);
#elif UNITY_ANDROID
            string url;
            string AndroidRatingURI = "market://details?id={0}";
            url = AndroidRatingURI.Replace("{0}", Application.identifier);
            Application.OpenURL(url);
#endif
            //http://play.google.com/store/apps/details?id=
            //Application.OpenURL("market://details?id=com.bilalarikan.micecompany");
        }
    }
}
