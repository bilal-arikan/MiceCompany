﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
using Firebase.Analytics;
using UnityEngine.Analytics;
using Common.Tools;
using UnityEngine;
using Common.Data;

namespace Common.Managers
{
    public partial class AnalyticManager :Singleton<AnalyticManager>
    {
        protected bool _Enabled = false;
        public bool Enabled
        {
            set
            {
                FirebaseAnalytics.SetAnalyticsCollectionEnabled(value);
                UnityEngine.Analytics.Analytics.enabled = value;
                _Enabled = value;
            }
            get
            {
                return _Enabled;
            }
        }

        StatisticData CurrentPlayStatistics;

        void Start()
        {
            Init(true);

            CurrentPlayStatistics = DatabaseManager.Local.Get<StatisticData>("TotalPlayStatistics");
            if (CurrentPlayStatistics == null)
                CurrentPlayStatistics = new StatisticData();

        }

        public virtual void Init(bool isEnabled)
        {
            Enabled = isEnabled;
            UnityEngine.Analytics.Analytics.deviceStatsEnabled = true;
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventAppOpen);

        }

        public void CalculateStatistics()
        {
            if (PlayerManager.CurrentPlayer == null)
                return;
            
            // Toplam oynanma
            CurrentPlayStatistics.PlayTime += Time.time;

            // Kazanılan yıldızların bütün yıldızlara oranı
            int earnedStars = 0;
            foreach (var passedSubChValue in PlayerManager.CurrentPlayer.PassedSubChapterScores.Values)
                earnedStars += passedSubChValue.Star;
            CurrentPlayStatistics.ChaptersCompletition = earnedStars / (Chapter.AllSubChCount*3);

            // Toplam Achievement bitirme oranı
            float totalAch = 0;
            float currentAch = 0;
            foreach (var ach in PlayerManager.CurrentPlayer.Achievements)
            {
                totalAch += 100;
                currentAch += ach.Value;
            }
            CurrentPlayStatistics.AchievementsCompletition = totalAch > 0 ? currentAch / totalAch : 0;

            // Oyun Dili
            CurrentPlayStatistics.Language = LanguageManager.CurrentLanguage;

            // Oyunun açılışından bu yana ortalama Ses yüksekliği
            CurrentPlayStatistics.AverageSfxValue =
                (CurrentPlayStatistics.AverageSfxValue * PlayerManager.CurrentPlayer.GameOpenedDays.Count + SoundManager.Instance.SfxVolume)
                / (PlayerManager.CurrentPlayer.GameOpenedDays.Count + 1);

            // Oyunun açılışından bu yana ortalama Müzik yüksekliği
            CurrentPlayStatistics.AverageMusicValue =
                (CurrentPlayStatistics.AverageMusicValue * PlayerManager.CurrentPlayer.GameOpenedDays.Count + SoundManager.Instance.MusicVolume)
                / (PlayerManager.CurrentPlayer.GameOpenedDays.Count + 1);

            // Birkez olsun Facebookla giriş yaptı
            if (SocialManager.Instance.IsSignedInFacebook && !CurrentPlayStatistics.AnyLogInFacebook)
                CurrentPlayStatistics.AnyLogInFacebook = true;
            // Birkez olsun GooglePlay giriş yaptı
            if (SocialManager.Instance.IsSignedInGooglePlay && !CurrentPlayStatistics.AnyLogInGooglePlay)
                CurrentPlayStatistics.AnyLogInGooglePlay = true;
            // Birkez olsun Facebookla giriş yaptı
            if (SocialManager.Instance.IsSignedInGameJolt && !CurrentPlayStatistics.AnyLogInGameJolt)
                CurrentPlayStatistics.AnyLogInGameJolt = true;

            Debug.Log("Statistics Calculated");
        }

        public virtual void ReportLoginState()
        {
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLogin);
        }

        void OnApplicationQuit()
        {
            CalculateStatistics();
            DatabaseManager.Local.Set("TotalPlayStatistics", CurrentPlayStatistics);
        }

    }

    [Serializable]
    public class StatisticData
    {
        public float PlayTime = 0;
        public float ChaptersCompletition = 0;
        public float AchievementsCompletition = 0;
        public int AverageRankingBetweenFriends = 0;
        public float ExploredPercentageOfTheGame = 0;

        public string Language = "en";
        public float AverageSfxValue = 0;
        public float AverageMusicValue = 0;
        public bool AnyLogInFacebook = false;
        public bool AnyLogInGooglePlay = false;
        public bool AnyLogInGameJolt = false;
    }
}
//*/
