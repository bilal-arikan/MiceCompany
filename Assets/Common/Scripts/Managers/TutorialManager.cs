﻿using Common.Data;
using Common.Tools;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Managers
{
    public class TutorialManager : Singleton<TutorialManager>
    {
        [ReadOnlyPlaying]
        public bool ShowTutorials = true;
        public bool ShowEvenWhatched = false;


        public Tutorial ActiveTutorial;

        public PointTutorialDict AllTutorials = new PointTutorialDict();

        private void Start()
        {
            if (ShowTutorials)
            {
                EventManager.StartListening(E.GameStarted, () =>
                {
                    if (PlayerManager.CurrentPlayer != null)
                    {
                        // ShowEvenWhatched ise her halikarda göster
                        if (ShowEvenWhatched || !PlayerManager.CurrentPlayer.WhatchedTutorials.Contains(LevelLoadManager.CurrentIndex))
                            Show(LevelLoadManager.CurrentIndex);
                    }
                });
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha0))
                Show(new Point());
            if (Input.GetKeyDown(KeyCode.Alpha1))
                Show(new Point(1,1));
            if (Input.GetKeyDown(KeyCode.Alpha2))
                Show(new Point(1,2));
        }

        public void Show(Point index)
        {
            if(ActiveTutorial != null)
            {
                Debug.LogWarning("Zaten Bir Tutorial Aktif: " + ActiveTutorial.name);
                return;
            }

            if (AllTutorials.ContainsKey(index))
                ActiveTutorial = Instantiate(AllTutorials[index].gameObject, transform).GetComponent<Tutorial>();

            if(ActiveTutorial != null)
            {
                // Tutorial gösterilirse oyun duraklatılır
                float def = Time.timeScale;
                GameManager.Instance.SetTimeScale(0);

                ActiveTutorial.Completed.AddListener(() =>
                {
                    PlayerManager.CurrentPlayer.WhatchedTutorials.Add(index);
                    Destroy(ActiveTutorial.gameObject);
                    // Tutorial bittiğinde oyun devam eder
                    GameManager.Instance.SetTimeScale(def);
                });
            }
        }
    }
}
