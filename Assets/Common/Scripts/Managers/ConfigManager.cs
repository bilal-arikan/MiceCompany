﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
using UnityEngine;
using Common.Tools;
using System.Collections.Specialized;
using System.Collections;
using Common.Data;
using Firebase.RemoteConfig;

namespace Common.Managers
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ConfigManager : Singleton<ConfigManager>
    {
        [ReadOnlyPlaying]
        public Firebase.LogLevel FirebaseLogLevel = Firebase.LogLevel.Info;
        protected bool _syncronized = false;
        public bool IsSyncronized
        {
            get
            {
                return _syncronized;
            }
        }

        [SerializeField,ReadOnly]
        long currentDayNumber;
        public static long CurrentDayNumber
        {
            get
            {
                if (Instance.currentDayNumber == 0)
                    return (long)((DateTime.Now - DateTime.MinValue).TotalDays);
                else
                    return Instance.currentDayNumber;
            }
            
        }

        [SerializeField,ReadOnly]
        bool availableNewVersion = false;
        public bool AvailableNewVersion
        {
            get
            {
                return availableNewVersion;
            }
        }

        [SerializeField,ReadOnly]
        private int currentVersion = 1;
        public static int CurrentVersion
        {
            get
            {
                return Instance.currentVersion;
            }
        }
        [SerializeField,ReadOnly]
        private int playstoreVersion = 1;
        public static int PlaystoreVersion
        {
            get
            {
                return Instance.playstoreVersion;
            }
        }

#if UNITY_EDITOR
        [SerializeField,ReadOnly]
        List<string> AllConfigs = new List<string>();
#endif

        void Start()
        {
            Firebase.FirebaseApp.LogLevel = FirebaseLogLevel;

            if (Application.internetReachability == NetworkReachability.NotReachable)
                Debug.LogWarning("No Internet Connection");

            currentDayNumber = (long)(Core.GetNetworkTime() - DateTime.MinValue).TotalDays;

            FetchConfigs();

            if (!int.TryParse(Application.version.Trim().Trim('.'), out currentVersion))
                currentVersion = 1;
            //Debug.Log("CurrentVersion: " + Application.version);

            EventManager.StartListening(E.ConfigsFetched, () =>
            {
                // Check new Version
                Common.Core.Invoke(() =>
                {
                    CheckNewVersion();
                    if (AvailableNewVersion)
                        UIManager.Instance.ShowToast("New Version Available !");
                });
            });

            CConsole.ActionsWithArg.Add("firebaseloglevel", (arg) =>
            {
                int result;
                if (int.TryParse(arg, out result))
                {
                    Firebase.FirebaseApp.LogLevel = (Firebase.LogLevel)result;
                    Debug.Log("Firebase LogLevel: " + Firebase.FirebaseApp.LogLevel);
                }
                else
                    CConsole.Log("Parameter Error ! exm:\"firebaseloglevel 2\"");
            });
        }


        /// <summary>
        /// Fetch configs from Firebase server
        /// if fails, use defaults values
        /// </summary>
        public virtual void FetchConfigs()
        {
            Debug.Log("Configs Fetching: started");
            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            {
                Firebase.DependencyStatus dependencyStatus = task.Result;
                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {

                    Firebase.RemoteConfig.FirebaseRemoteConfig.SetDefaults(DefaultConfigs);
                    Firebase.RemoteConfig.FirebaseRemoteConfig.FetchAsync().ContinueWith(OnFetchingConfigsFinished);
                }
                else
                {
                    Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
                }
            });

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        protected virtual void OnFetchingConfigsFinished(System.Threading.Tasks.Task task)
        {
            ConfigInfo info = FirebaseRemoteConfig.Info;
            switch (info.LastFetchStatus)
            {
                case LastFetchStatus.Success:
                    //////////////////////////////////////////////////////////////////////////
                    FirebaseRemoteConfig.ActivateFetched();
                    _syncronized = true;
                    Debug.Log(String.Format("Remote Configs loaded (last fetch time {0}) LastVersion:{1}.", info.FetchTime,googlestore_version));

#if UNITY_EDITOR
                    AllConfigs.Clear();
                    foreach (string s in DefaultConfigs.Keys)
                        AllConfigs.Add(GetConfigValue(s));
#endif
                    //////////////////////////////////////////////////////////////////////////
                    break;
                case LastFetchStatus.Failure:
                    switch (info.LastFetchFailureReason)
                    {
                        case FetchFailureReason.Error:
                            Debug.LogError("Fetch failed for Invalid Reason: " + task.Exception.Message);
                            break;
                        case FetchFailureReason.Throttled:
                            Debug.Log("Fetch throttled until " + info.ThrottledEndTime);
                            break;
                    }
                    break;
                case LastFetchStatus.Pending:
                    Debug.LogError("Latest Fetch call still pending.");
                    break;
            }

            EventManager.TriggerEvent(E.ConfigsFetched);

        }

        public void SetGraphicQuality(int quality)
        {
            Debug.Log("Set Quality " + quality);
            QualitySettings.SetQualityLevel(quality, true);
        }
        

        public void ReportCrash(string message, Exception e)
        {
            Debug.Log("Bug Reported:" +   BugReport.Create(message,e).ToString());
        }

        public bool CheckNewVersion()
        {
            playstoreVersion = int.Parse(googlestore_version);

            //Debug.Log("Version: " + playstoreVersion + googlestore_version + currentVersion);

            availableNewVersion = currentVersion < playstoreVersion;
            if (availableNewVersion)
                EventManager.TriggerObjectEvent(E.AvailableNewVersion, playstoreVersion);

            return availableNewVersion;
        }

        public void GetNewsAndReleaseNotes(Action<List<New>> newsResult, Action<List<string>> releaseResult)
        {
            List<New> news = ObjectSerializer.FromJson<List<New>>(news_json);
            List<string> releases = ObjectSerializer.FromJson<List<string>>(releases_json);

            newsResult(news);
            releaseResult(releases);
        }

        /// <summary>
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetConfigValue(string key)
        {
                return FirebaseRemoteConfig.GetValue(key).StringValue;
        }

        [Serializable]
        public class New
        {
            public string DeepUrl;
            public string ImageUrl;

            [Newtonsoft.Json.JsonIgnore]
            public Sprite Image;
            [Newtonsoft.Json.JsonIgnore]
            public MarkLight.SpriteAsset NewSpriteAsset
            {
                get
                {
                    return new MarkLight.SpriteAsset(DeepUrl, Image);
                }
                set
                {
                    Image = value.Sprite;
                }
            }
        }
    }
}
