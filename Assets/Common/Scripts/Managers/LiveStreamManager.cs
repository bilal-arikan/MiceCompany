﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
using UnityEngine;
#if GooglePlayGames
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Video;
#endif
using Common.Tools;

namespace Common.Managers
{
    public partial class LiveStreamManager : Singleton<LiveStreamManager>
    {
        /*protected VideoCapabilities _capabilities;
        public bool IsCameraSupport
        {
            get
            {
                if(_capabilities != null)
                {
                    return _capabilities.IsCameraSupported;
                }
                else{
                    return false;
                }
            }
        }
        public bool IsMicrophoneSupport
        {
            get
            {
                if (_capabilities != null)
                {
                    return _capabilities.IsMicSupported;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool IsStorageSupport
        {
            get
            {
                if (_capabilities != null)
                {
                    return _capabilities.IsWriteStorageSupported;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool IsLiveStreamSupport
        {
            get
            {
                if (_capabilities != null)
                {
                    return _capabilities.SupportsCaptureMode(VideoCaptureMode.Stream);
                }
                else
                {
                    return false;
                }
            }
        }
        protected VideoQualityLevel MaxQualitySupport
        {
            get
            {
                if (_capabilities != null)
                {
                    if (_capabilities.SupportsQualityLevel(VideoQualityLevel.FullHD))
                        return VideoQualityLevel.FullHD;
                    else if (_capabilities.SupportsQualityLevel(VideoQualityLevel.XHD))
                        return VideoQualityLevel.XHD;
                    else if (_capabilities.SupportsQualityLevel(VideoQualityLevel.HD))
                        return VideoQualityLevel.HD;
                    else if (_capabilities.SupportsQualityLevel(VideoQualityLevel.SD))
                        return VideoQualityLevel.SD;
                    else
                        return VideoQualityLevel.Unknown;
                }
                else
                {
                    return VideoQualityLevel.Unknown;
                }
            }
        }

        protected override void Awake()
        {
            base.Awake();
            if (Application.isPlaying)
                Init();
        }

        public virtual void Init()
        {
            PlayGamesPlatform.Instance.Video.GetCaptureCapabilities(
              (status, capabilities) =>
              {
                  bool isSuccess = CommonTypesUtil.StatusIsSuccess(status);
                  if (isSuccess)
                  {
                      _capabilities = capabilities;
                      Debug.Log("LiveStream: Capabilities Set: " + status.ToString());
                  }
                  else
                  {
                      Debug.Log("LiveStreamError: " + status.ToString());
                  }
              });
            PlayGamesPlatform.Instance.Video.RegisterCaptureOverlayStateChangedListener(new StateListener());
        }

        public virtual void ShowState()
        {
            PlayGamesPlatform.Instance.Video.GetCaptureState(
              (status, state) =>
              {
                  bool isSuccess = CommonTypesUtil.StatusIsSuccess(status);
                  if (isSuccess)
                  {
                      if (state.IsCapturing)
                      {
                          Debug.Log("Currently capturing to " + state.CaptureMode.ToString() + " in " +
                                    state.QualityLevel.ToString());
                      }
                      else
                      {
                          Debug.Log("Not currently capturing.");
                      }
                  }
                  else
                  {
                      Debug.Log("Error: " + status.ToString());
                  }
              });
        }

        public virtual void StartCapturing(bool isLiveStream )
        {
            if (PlayGamesPlatform.Instance.Video.IsCaptureSupported())
            {
                PlayGamesPlatform.Instance.Video.IsCaptureAvailable( isLiveStream ? VideoCaptureMode.Stream : VideoCaptureMode.File,
                  (status, isAvailable) =>
                  {
                      bool isSuccess = CommonTypesUtil.StatusIsSuccess(status);
                      if (isSuccess)
                      {
                          if (isAvailable)
                          {
                              PlayGamesPlatform.Instance.Video.ShowCaptureOverlay();
                          }
                          else
                          {
                              Debug.Log("Video capture is unavailable. Is the overlay already open?");
                          }
                      }
                      else
                      {
                          Debug.Log("Error: " + status.ToString());
                      }
                  });
            }
            else
            {
                Debug.Log("Not Supporting !");
            }
        }

        public virtual void StopCapturing()
        {

        }

        public virtual void PauseCapturing()
        {

        }

        /// <summary>
        /// CaptureOverlayStateListener
        /// </summary>
        protected class StateListener : CaptureOverlayStateListener
        {
            public void OnCaptureOverlayStateChanged(VideoCaptureOverlayState overlayState)
            {
                Debug.Log("Overlay State is now " + overlayState.ToString());
            }
        }*/

    }
}
//*/
