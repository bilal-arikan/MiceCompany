﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common.Tools;
using MarkLight;

namespace Common.Managers
{
    /// <summary>
    /// ISO 639-1
    /// </summary>
    public partial class LanguageManager : Singleton<LanguageManager>
    {
        [SerializeField,ReadOnlyPlaying]
        private string langID = "en";
        public static string CurrentLanguage
        {
            get
            {
                return Instance.langID;
            }
        }

        protected override void Awake()
        {
            base.Awake();
#if !UNITY_EDITOR
        Debug.Log("Available Languages: en,tr,ja,zh,ko,de,h,ru,es,ar");
#endif
            Common.CConsole.ActionsWithArg.Add("setlanguage", (s) => { LanguageManager.ChangeLanguage(s); });
        }

        /// <summary>
        /// </summary>
        /// <param name="id"></param>
        public static void ChangeLanguage(string id)
        {

            Instance.langID = id;
            ResourceDictionary.SetConfiguration(id);
            ResourceDictionary.NotifyObservers(); // update bindings
                                                  //DatabaseManager.Language = id;
        }

        public static string GetText(string textID)
        {
            bool hasValue;
            string text = ResourceDictionary.GetValue("Loc", textID, out hasValue);

            if (string.IsNullOrEmpty(text))
                return "";
            else
                return text;
        }

    }
}


public static partial class L
{
}