﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Common.Tools;
using Common.Data;
using Common;

namespace Common.Managers
{
    public partial class LevelLoadManager : Singleton<LevelLoadManager>
    {
        public IntChapterDict AllChapters = new IntChapterDict();

        public bool IsMapChanging = false;
        public float LoadCompleteDelay = 0;
        public float WaitToUnlockLevel= 20;

        [ReadOnly]
        public bool AppStarted = false;

        [SerializeField,ReadOnly]
        private bool IsUnlocking = false;

        /// <summary>
        /// Her bölüm yüklendiğinde bu Set edilir
        /// diğer değerlerde buna göre belirlenir
        /// </summary>
        [SerializeField,ReadOnly]
        private Point currentIndex = new Point();
        public static Point CurrentIndex
        {
            get
            {
                return Instance.currentIndex;
            }
            protected set
            {
                Instance.currentIndex = value;
            }
        }

        public static Chapter CurrentCh
        {
            get
            {
                if (CurrentIndex.X > 0)
                    return Instance.AllChapters[CurrentIndex.X];
                else
                    return null;
            }
        }
        public static SubChapter CurrentSubCh
        {
            get
            {
                if (CurrentIndex.X > 0 && CurrentIndex.Y > 0)
                    return Instance.AllChapters[CurrentIndex.X][CurrentIndex.Y];
                else
                    return null;
            }
        }
        public static string ActiveSubChKey
        {
            get
            {
                return LevelLoadManager.CurrentIndex.X.ToString("00") + LevelLoadManager.CurrentIndex.Y.ToString("00");
            }
        }
        public static string CurrentSubChName
        {
            get
            {
                return "Ch" + CurrentSubCh.Index.ToString();
            }
        }

        protected override void Awake()
        {
            base.Awake();

            CConsole.ActionsWithArg.Add("load", (s) => 
            {
                try
                {
                    string[] xy = { s.Trim().Substring(0, 2).Trim(), s.Trim().Substring(2, 2) } ;
                    int x = int.Parse(xy[0]);
                    int y = int.Parse(xy[1]);
                    ChangeChapter(new Point(x, y));
                    /*LoadLevel(new Point(x, y), (sc) =>
                    {
                        Debug.Log("Loading Completed");
                    });*/
                }
                catch
                {
                    CConsole.LogError("Wrong Format! example usage> \"load 0205\" (with 4 digit)");
                }

            });
            CConsole.ActionsNoArg.Add("scenes", () =>
            {
                string text = "";
                foreach(var s in AllChapters.Keys)
                    text += s + ", ";
                Debug.Log("Scenes: " + text);
            });
        }

        IEnumerator Start()
        {
            // Loading Ekranından gelindiyse o ekranı siler
            if ( SceneManager.GetSceneByBuildIndex(0).isLoaded)
            {
                SceneManager.UnloadSceneAsync(0);
            }

            // Başlangıçta yüklü bölümü tespit eder
            for (int i = 2; i < SceneManager.sceneCountInBuildSettings; i++)
                if (SceneManager.GetSceneByBuildIndex(i).isLoaded)
                {
                    Point index = Point.FromString(SceneManager.GetSceneByBuildIndex(i).name.Substring(2, 4));
                    Debug.Log("Active: " + index);
                    currentIndex = index;

                    SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(i));

                    goto End;
                }

            End:


            UIManager.Instance.InitChapters();

            yield return new WaitForEndOfFrame();

            EventManager.TriggerEvent(E.AppStarted);
            AppStarted = true;
            yield return new WaitForEndOfFrame();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toLoad">AllLevels Dictionary Key</param>
        /// <param name="loadFinished"></param>
        public void LoadLevel(Point toLoad, Action<Point> loadFinished)
        {
            // MainMenu ise
            if(toLoad.X == 0)
            {
                StartCoroutine(
                    AsynchronousLoad(
                        2,
                        toLoad,
                        loadFinished));
            }
            else if (toLoad.X > 0 && toLoad.Y > 0)// .Exists(k => toLoad.SceneBuildIndex == k.SceneBuildIndex))
            {
                Chapter toLoadCh = null;
                if(AllChapters.TryGetValue(toLoad.X,out toLoadCh))
                {
                    StartCoroutine(
                        AsynchronousLoad(
                            toLoadCh[toLoad.Y].SceneBuildIndex,
                            toLoad,
                            loadFinished));
                }
                else
                {
                    Debug.LogError("Level Bulunamadı!!! (" + toLoad + ")");
                }
                //Debug.Log(toLoad + " Loading...");
            }
            else
            {
                Debug.LogError("Level Index'i Geçerli değil: " + toLoad);
            }
        }


        IEnumerator AsynchronousLoad(int buildIndex, Point toLoad, Action<Point> loadFinished)
        {
            LoadingStarted(toLoad);

            //Application.backgroundLoadingPriority = ThreadPriority.High;

            AsyncOperation ao = SceneManager.LoadSceneAsync(buildIndex, LoadSceneMode.Additive);
            ao.allowSceneActivation = false;

            while (!ao.isDone)
            {
                // [0, 0.9] > [0, 1]
                float progress = Mathf.Clamp01(ao.progress / 0.9f);
                //Debug.Log("Loading progress: " + (progress * 100) + "%");
                EventManager.TriggerObjectEvent(E.LevelLoadingProgressed, progress);

                // Loading completed
                if (ao.progress >= 0.9f)
                {
#pragma warning disable 0618
                    // MainMenu yüklüyse Unload et
                    if (currentIndex.X == 0 && SceneManager.GetSceneByBuildIndex(2).isLoaded)
                        SceneManager.UnloadScene(2);
                    // Eski bölüm yüklüyse Unload et
                    else if (currentIndex.X != 0 && SceneManager.GetSceneByBuildIndex(AllChapters[currentIndex.X][currentIndex.Y].SceneBuildIndex).isLoaded)
                        SceneManager.UnloadScene(AllChapters[currentIndex.X][currentIndex.Y].SceneBuildIndex);
#pragma warning restore 0618

                    ao.allowSceneActivation = true;
                }

                yield return null;
            }

            // Kod while dan çıkıp buraya ulaştığında Scene yüklenmiştir
            yield return new WaitForSeconds(LoadCompleteDelay);

            SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(buildIndex));

            LoadingCompleted(toLoad);
            if (loadFinished != null)
                loadFinished.Invoke(toLoad );
        }


        /// <summary>
        /// Sets up all visual elements, fades from black at the start
        /// </summary>
        protected virtual void LoadingStarted(Point scene)
        {
            IsMapChanging = true;
            EventManager.TriggerObjectEvent(E.LevelLoadingStarted, scene);
            EventManager.TriggerObjectEvent(E.LevelLoadingProgressed, 0f);
        }

        /// <summary>
        /// Triggered when the actual loading is done, replaces the progress bar with the complete animation
        /// </summary>
        protected virtual void LoadingCompleted(Point scene)
        {
            //Debug.Log("NewScene: " + scene.Name);
            currentIndex = scene;
            IsMapChanging = false;

            EventManager.TriggerObjectEvent(E.LevelLoadingProgressed, 100f);
            EventManager.TriggerObjectEvent(E.LevelLoadingCompleted, scene);
        }

        /// <summary>
        /// Load a Chapter
        /// </summary>
        /// <param name="ch"></param>
        /// <param name="subch"></param>
        public void ChangeChapter(Point p)
        {
            Debug.Log("Ch" + p.X.ToString("00") + p.Y.ToString("00") + " Loading");
            if (p.X == 0)
                GameManager.Instance.SetStatus( GameManager.GameStatus.MainMenu);
            LevelLoadManager.Instance.LoadLevel(p, GameManager.Instance.OnChapterLoaded);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns>returns true if success</returns>
        public void TryUnlockSubChapter(SubChapter sch,Action<bool> success, Action<float> AdFailedTimerChanged = null)
        {
            //AdsManager Disablsa (RemoveAds itemini almakla)
            if (!AdsManager.Instance.enabled)
            {
                EventManager.TriggerObjectEvent(E.SubChapterUnlocked, sch);
                success(true);
                return;
            }

            // Revard Göstermeyi Dene
#if UNITY_EDITOR
            bool s = false;
#else
            bool s = AdsManager.Instance.TryShowReward(
                (started) =>
                {
                    Debug.Log("Unlocking > " + sch.IndexPoint.X + ":" + sch.IndexPoint.Y);

                }, (reward) =>
                {
                    EventManager.TriggerObjectEvent(E.SubChapterUnlocked, sch);
                    success(true);
                });

            // Revard Gösterilemezse Interstitial Göstermeyi dene
            if (!s)
                s = AdsManager.Instance.TryShowInterstitial(
                    (showed) =>
                    {
                        if (showed)
                        {
                            EventManager.TriggerObjectEvent(E.SubChapterUnlocked, sch);
                            success(true);
                        }
                    });
#endif


            // O da gösterilemezse 
            if (!s )
            {
                // Timerı takip edecek bir fonksiyon varsa ve Timer çalıştırılmadıysa Timer çalıştırılır
                if ( AdFailedTimerChanged != null && !IsUnlocking)
                {
                    IsUnlocking = true;
                    var t = Timer.StartTimer(WaitToUnlockLevel, (finishTime) => 
                    {
                        IsUnlocking = false;
                        EventManager.TriggerObjectEvent(E.SubChapterUnlocked, sch);
                        success(true);
                    },
                    (currentTime)=> 
                    {
                        // Kalan zamanı gösterir
                        AdFailedTimerChanged(WaitToUnlockLevel - currentTime);
                    },
                    // RealTime
                    true);
                    // Sonradan tespit edip silebilmek için
                    t.transform.parent = transform;
                }
                else
                {
                    success(false);
                }
            }

        }

        public void CancelUnlockSubChapter()
        {
            IsUnlocking = false;
            var t = GetComponentInChildren<Timer>();
            if(t != null)
            {
                Destroy(t.gameObject);
            }
        }
    }

}
