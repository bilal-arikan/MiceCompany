﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Common.Tools;
using Common.Data;

namespace Common.Managers
{
    public partial class PlayerManager: Singleton<PlayerManager>
    {

        [SerializeField]
        Player currentPlayer;
        public static Player CurrentPlayer
        {
            get { return Instance.currentPlayer; }
        }

        [SerializeField]
        Profile currentProfile;
        public static Profile CurrentProfile
        {
            get { return Instance.currentProfile; }
            set { Instance.currentProfile = value; }
        }

        [SerializeField,ReadOnly]
        private bool isFirstPlaying;
        public static bool IsFirstPlaying
        {
            get
            {
                return Instance.isFirstPlaying;
            }
        }

        public AEnumAchievementDict AllAchievements = new AEnumAchievementDict();

        public int[] LevelSteps = { 0, 15, 32, 65, 150, 285, 400 };

        [ReadOnly]
        public bool ChaptersInitialized = false;


        void OnApplicationQuit()
        {
            //DatabaseManager.Local.Set("Player", CurrentPlayer);
            //Debug.Log("Player Saved to Local");
            SavePlayerData();
        }

        /// <summary>
        /// Günlük girişten kazanılan hediye varmı kontrol eder
        /// </summary>
        protected void CheckDailyGifts()
        {
            if (ConfigManager.CurrentDayNumber == 0)
                Debug.LogWarning("Tarih bilgisi Sunucudan Alınamadı !!!");

            if( !CurrentPlayer.GameOpenedDays.Contains(ConfigManager.CurrentDayNumber) )
            {
                for(int i = 0; i < 7; i++)
                {
                    // kaydedilen günlerden öncesine bakılamaz
                    if((CurrentPlayer.GameOpenedDays.Count - i - 1 >= 0) &&
                    // Son kaydedilen gün, bugünün 1 öncesi mi?
                        (CurrentPlayer.GameOpenedDays.ElementAt(CurrentPlayer.GameOpenedDays.Count - i - 1) == ConfigManager.CurrentDayNumber - i - 1))
                    {
                    }
                    else
                    {
                        //(i+1). Günün hediyesi
                        EventManager.TriggerObjectEvent(E.DailyGift, i+1);
                        break;
                    }
                }

                // Hediyeler alındıktan sonra şu an ki gün kaydedilir
                CurrentPlayer.GameOpenedDays.Add(ConfigManager.CurrentDayNumber);
            }
            else
            {
                Debug.Log("Zaten Bugünün Hediyesini Aldın");
            }
        }

        public void LoadPlayerProfile()
        {
            currentProfile = new Profile();
            currentProfile.PhotoUrl = ConfigManager.invite_image;
            EventManager.TriggerObjectEvent(E.PlayerProfileChanged, currentProfile);
        }

        public void LoadPlayerData()
        {
            CConsole.Log("Player Data Loading...", CColor.cyan);

            // Localden Data yı alır
            var player = DatabaseManager.Local.Get<Player>("Players/" + PlayerManager.CurrentPlayer.ID);
            if (player != null)
            {
                currentPlayer = player;
                isFirstPlaying = false;
                CConsole.Log("Player Loaded: From Local", CColor.cyan);
            }
            // Ordanda alınamazsa yeni Player oluştur
            else
            {
                currentPlayer = new Player();
                isFirstPlaying = true;
                SavePlayerData();
                CConsole.Log("No Data (New Player Created)", CColor.cyan);
            }
            EventManager.TriggerObjectEvent(E.PlayerDataChanged, CurrentPlayer);
        }

        public void SavePlayerData()
        {

            // Her halukarda Locale kaydet
            DatabaseManager.Local.Set("Players/" + PlayerManager.CurrentPlayer.ID, CurrentPlayer);
            CConsole.Log("Player Saved to Local",CColor.cyan);
            EventManager.TriggerObjectEvent(E.PlayerDataSaved, CurrentPlayer);
        }

        public void SaveScore(PassedSubCh psc)
        {
            CurrentPlayer.AddPassedChapter(LevelLoadManager.CurrentIndex.X, LevelLoadManager.CurrentIndex.Y, psc);

            //ve Sunucuya kaydetme kodları olmalı
            //Ama şimdilik devre dışı
        }

        public void SetAchievement(Common.Data.Achievement ach, double value = 100d)
        {
            if(CurrentPlayer != null)
            {
                var existAch = CurrentPlayer.Achievements.First(a => a.GetHashCode() == ach.GetHashCode());
                if(existAch != null)
                {
                    existAch.Value = (float)value;
                }
                else
                {
                    ach.Value = (float)value;
                    CurrentPlayer.Achievements.Add(ach);
                }
                Debug.Log("Ach: " + ach.Name + " :" + value);
            }
        }
    }
}
