﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Tools;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Common.Managers
{
    public partial class InputManager : Singleton<InputManager>
    {
        public EventSystem eventSystem;
        public LayerMask PointerClickLayerMask;
        [SerializeField,ReadOnly]
        private Vector3 oldMousePoisition = Vector3.zero;
        [SerializeField, ReadOnly]
        private TouchPhase mousePhase = TouchPhase.Ended;
        [SerializeField, ReadOnly]
        private Touch? mouseTouch = null;
        private static bool IsPointerDownCatched = false;

        public static Vector2 PointerPosition
        {
            get
            {
                return Input.touchCount > 0 ? Input.GetTouch(0).position : (Instance.mouseTouch != null ? Instance.mouseTouch.Value.position : (Vector2)Input.mousePosition);
            }
        }
        public static Vector2 PointerDelta
        {
            get
            {
                if (Input.touchCount > 0)
                    return Input.GetTouch(0).deltaPosition;
                else if (Instance.mouseTouch != null)
                    return Instance.mouseTouch.Value.deltaPosition;
                else
                    return Vector2.zero;
            }
        }
        public static bool IsPointerDown
        {
            get
            {
                return Input.touchCount > 0 ? true : Instance.mouseTouch != null;
            }
        }
        public static float TwoPointerDistance
        {
            get
            {
                if (Input.touchCount > 1)
                    return Vector2.Distance(
                        Input.GetTouch(0).position,
                        Input.GetTouch(1).position);
                else
                    return 0;
            }
        }
        public static float TwoPointerDistanceDelta
        {
            get
            {
                if (Input.touchCount > 1)
                    return TwoPointerDistance - Vector2.Distance(
                        Input.GetTouch(0).position - Input.GetTouch(0).deltaPosition,
                        Input.GetTouch(1).position - Input.GetTouch(1).deltaPosition);
                else
                    return 0;
            }
        }
        [SerializeField, ReadOnly]
        private bool isClickedUI = false;
        public static bool IsClickedUI
        {
            get
            {
                return Instance.isClickedUI; // _IsClickedUI;
            }
        }

        private void Start()
        {
            //Debug.Log("Input:" + (1 << LayerMask.GetMask(LayersToRay.ToArray())) + ":" + LayerMask.GetMask(LayersToRay.ToArray()));
            //Input.simulateMouseWithTouches = true;
            eventSystem = EventSystem.current;
        }

        private void Update()
        {
            bool isClicked = false;
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                // IsClickedUI true
                if (IsPointerOverUIObject())
                    isClickedUI = true;

                mousePhase = TouchPhase.Began;
                oldMousePoisition = Vector3.zero;
                isClicked = true;
                IsPointerDownCatched = false;
            }
            else if (Input.GetKey(KeyCode.Mouse0))
            {
                if (Input.mousePosition - oldMousePoisition != Vector3.zero)
                    mousePhase = TouchPhase.Moved;
                else
                    mousePhase = TouchPhase.Stationary;
                oldMousePoisition = Input.mousePosition;
                isClicked = true;
                IsPointerDownCatched = true;
            }
            else if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                oldMousePoisition = Input.mousePosition;
                mousePhase = TouchPhase.Ended;
                isClicked = true;
                IsPointerDownCatched = true;
            }
            else
                IsPointerDownCatched = false;

            if (isClicked)
            {
                Touch fakeTouch = new Touch();
                fakeTouch.fingerId = 0;
                fakeTouch.position = Input.mousePosition;
                fakeTouch.deltaTime = Time.deltaTime;
                fakeTouch.deltaPosition = oldMousePoisition == Vector3.zero ?
                                                            Vector3.zero : Input.mousePosition - oldMousePoisition;
                fakeTouch.phase = mousePhase;
                fakeTouch.tapCount = 1;

                mouseTouch = fakeTouch;
            }
            else
            {
                mouseTouch = null;
            }



        }

        private void LateUpdate()
        {
            if(Input.GetKeyUp(KeyCode.Mouse0))
                isClickedUI = false;

            if (Input.touchCount > 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began && IsPointerOverUIObject())
                    isClickedUI = true;
                else if (Input.GetTouch(0).phase == TouchPhase.Ended)
                    isClickedUI = false;
            }
            else if (mouseTouch != null)
            {
                if (mouseTouch.Value.phase == TouchPhase.Began && IsPointerOverUIObject())
                    isClickedUI = true;
                else if (mouseTouch.Value.phase == TouchPhase.Ended)
                    isClickedUI = false;
            }
        }

        public static bool GetPointerDown()
        {
            return IsPointerDown && !IsPointerDownCatched;
        }

        public static RaycastHit2D ScreenRay2D()
        {
            if (!IsClickedUI)
            {
                Vector2 worldPos = Camera.main.ScreenToWorldPoint(PointerPosition);
                RaycastHit2D hit = Physics2D.Raycast(worldPos, Vector2.zero, Mathf.Infinity, Instance.PointerClickLayerMask.value);//0: Default Layer
                //Debug.Log("RAY:" + hit + hit.collider + hit.point);
                return hit;
            }
            else
            {
                return new RaycastHit2D();
            }
        }
        public static RaycastHit2D ScreenRay2D(Vector3 centerPos)
        {
            if (!IsClickedUI)
            {
                Vector2 worldPos = Camera.main.ScreenToWorldPoint(centerPos);
                RaycastHit2D hit = Physics2D.Raycast(worldPos, Vector2.zero, Mathf.Infinity, Instance.PointerClickLayerMask.value);
                //Debug.Log("RAY:" + hit + hit.collider + hit.point);
                return hit;
            }
            else
            {
                return new RaycastHit2D();
            }
        }


        public static RaycastHit2D[] ScreenRayAll2D()
        {
            if (!IsClickedUI)
            {
                Vector2 worldPos = Camera.main.ScreenToWorldPoint(PointerPosition);
                RaycastHit2D[] hit = Physics2D.RaycastAll(worldPos, Vector2.zero, Mathf.Infinity, Instance.PointerClickLayerMask.value);
                //Debug.Log("RAY:" + hit.Length + ":" + (1 << 0) + ":" + ~(1 << 0) + ":" + Instance.PointerClickLayerMask.value + ":" +(1 << Instance.PointerClickLayerMask.value));
                return hit;
            }
            else
            {
                return new RaycastHit2D[0];
            }
        }
        public static RaycastHit2D[] ScreenRayAll2D(Vector3 centerPos)
        {
            if (!IsClickedUI)
            {
                Vector2 worldPos = Camera.main.ScreenToWorldPoint(centerPos);
                RaycastHit2D[] hit = Physics2D.RaycastAll(worldPos, Vector2.zero,Mathf.Infinity, Instance.PointerClickLayerMask.value);
                //Debug.Log("RAY:" + hit + hit.collider + hit.point);
                return hit;
            }
            else
            {
                return new RaycastHit2D[0];
            }
        }


        public static Vector2 ScreenToWorld(Camera cam = null)
        {
            Vector2 worldPos = (cam == null ? Camera.main : cam).ScreenToWorldPoint(
                Input.touches.Length > 0 ? Input.GetTouch(0).position : (Vector2)Input.mousePosition);
            return worldPos;
        }

        /// <summary>
        /// Cast a ray to test if Input.mousePosition is over any UI object in EventSystem.current. This is a replacement
        /// for IsPointerOverGameObject() which does not work on Android in 4.6.0f3
        /// </summary>
        private bool IsPointerOverUIObject()
        {
            // Referencing this code for GraphicRaycaster https://gist.github.com/stramit/ead7ca1f432f3c0f181f
            // the ray cast appears to require only eventData.position.
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }
    }
}
