﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
using UnityEngine;
#if FirebaseDatabase
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
#endif
using Common.Tools;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine.Events;
using Common.Data;

namespace Common.Managers
{
    public partial class DatabaseManager : Singleton<DatabaseManager>
    {

        public string FirebaseDatabaseUrl = "";//"https://template-55686355.firebaseio.com/";
        [ReadOnlyPlaying]
        public bool WorkOnline = true;
#if FirebaseDatabase
        protected DatabaseReference Root
        {
            get
            {
                return FirebaseDatabase.DefaultInstance.RootReference;
            }
        }


        protected override void Awake()
        {
            base.Awake();
            Init(WorkOnline);
        }

        /// <summary>
        /// Init Firebase Database properties
        /// </summary>
        /// <param name="isOnline"></param>
        public virtual void Init(bool isOnline)
        {

#if UNITY_EDITOR
            // path in order for the database connection to work correctly in editor.
            FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(FirebaseDatabaseUrl);
            FirebaseApp.DefaultInstance.SetEditorServiceAccountEmail("bilal1993arikan@gmail.com");
            //FirebaseApp.DefaultInstance.SetEditorP12FileName("mice-company-98092420-P12.p12");
            //FirebaseApp.DefaultInstance.SetEditorP12Password("18631245998");

            if (FirebaseApp.DefaultInstance.Options.DatabaseUrl != null)
                FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(FirebaseApp.DefaultInstance.Options.DatabaseUrl);
#endif
            if (!isOnline)
                Root.Database.GoOffline();
            else
                Root.Database.GoOnline();

            CConsole.ActionsWithArg.Add("databaseonline", (arg) =>
            {
                bool result;
                if (bool.TryParse(arg, out result))
                {
                    if(result)
                        Root.Database.GoOnline();
                    else
                        Root.Database.GoOffline();
                    Debug.Log("Database Now " + (result ? "Online" : "Offline"));
                }
                else
                    CConsole.Log("Parameter Error ! exm:\"setlogdata true\"");
            });
        }

        /// <summary>
        /// Catching value changed events
        /// </summary>
        /// <param name="path">example "Levels/Forest/ch5"</param>
        /// <param name="onChanged"></param>
        /// <param name="orderByValue"></param>
        /// <param name="first"></param>
        /// <param name="last"></param>
        public virtual void AddListener(string path,Action<ValueChangedEventArgs> onChanged,bool orderByValue,int first, int last)
        {
            DatabaseReference r = Root.Child(path);
            Query q = orderByValue ? r.OrderByValue() : r.OrderByKey();
            q.LimitToLast(last).LimitToFirst(first).ValueChanged += (o,e) => 
            {
                onChanged.Invoke(e);
            };
        }

        public List<PassedSubCh> Passes;
        public DataSnapshot scoreList;
        private void Update()
        {

            if (Input.GetKeyDown(KeyCode.I))
            {
                Root.Child("Scores/Ch0101").OrderByChild("Score").LimitToLast(3).GetValueAsync().ContinueWith((t) =>
                {
                    CConsole.Log(ObjectSerializer.JsonToPretty(t.Result.GetRawJsonValue()),Color.green);

                    scoreList = t.Result;

                    List<PassedSubCh> scores = new List<PassedSubCh>();
                    foreach (var psc in t.Result.Children)
                        scores.Add(ObjectSerializer.FromJson<PassedSubCh>(psc.GetRawJsonValue()));

                    Passes = scores;

                    CConsole.Log(ObjectSerializer.JsonToPretty(scoreList.GetRawJsonValue()), CColor.purple);

                });
            }
        }

        /// <summary>
        /// Gets an object value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <param name="completed"></param>
        public virtual void Get<T>(string path, Action<T, DataSnapshot> completed, 
            string orderByChildPath = null, int limitLast = 0, int limitFirst = 0)
        {
#if UNITY_EDITOR
            path = path.Split('@')[0];
#endif
            Task<DataSnapshot> getTask;
            if (!string.IsNullOrEmpty(orderByChildPath))
            {
                var q = Root.Child(path).OrderByChild(orderByChildPath);
                if (limitLast > 0)
                    q = q.LimitToLast(limitLast);
                else if(limitFirst > 0)
                    q = q.LimitToFirst(limitFirst);

                getTask = q.GetValueAsync();
            }
            else
            {
                getTask = Root.Child(path).GetValueAsync();
            }

            getTask.ContinueWith(task =>
             {
                 if (task.IsFaulted || task.IsCanceled)
                 {
                     OnTaskEndedWithFault(task.Exception);
                     if (completed != null)
                         completed.Invoke(default(T), null);

                 }
                 else if (task.IsCompleted)
                 {
                     DataSnapshot snapshot = task.Result;

                     if((int)Firebase.FirebaseApp.LogLevel < 2)
                        CConsole.Log( "FirebaseGet:"+ ObjectSerializer.JsonToPretty(snapshot.GetRawJsonValue()),Color.green);

                     T obj = ObjectSerializer.FromJson<T>(snapshot.GetRawJsonValue());

                    if(completed !=null)
                        completed.Invoke(obj,snapshot);
                 }
                 else
                 {
                     Debug.LogError("YOK ARTIK (Database.Get)");
                 }
             });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="paths"></param>
        /// <param name="completed"></param>
        /// <param name="orderByChildPath"></param>
        /// <param name="limitLast"></param>
        /// <param name="limitFirst"></param>
        public virtual void Get(List<string> paths, Action< Dictionary<string, DataSnapshot>> completed,
            string orderByChildPath = null, int limitLast = 0, int limitFirst = 0)
        {
            List<Task<DataSnapshot>> getTasks = new List<Task<DataSnapshot>>();
            Dictionary<string, DataSnapshot> data = new Dictionary<string, DataSnapshot>();

            foreach (var p in paths)
            {
                if (!string.IsNullOrEmpty(orderByChildPath))
                {
                    var q = Root.Child(p).OrderByChild(orderByChildPath);
                    if (limitLast > 0)
                        q = q.LimitToLast(limitLast);
                    else if (limitFirst > 0)
                        q = q.LimitToFirst(limitFirst);

                    getTasks.Add(q.GetValueAsync());
                }
                else
                {
                    getTasks.Add(Root.Child(p).GetValueAsync());
                }
            }

            foreach (var t in getTasks)
            {
                t.ContinueWith(task =>
                {
                    if (task.IsFaulted || task.IsCanceled)
                    {
                        OnTaskEndedWithFault(task.Exception);
                        data.Add(paths[getTasks.IndexOf(t)], null);
                    }
                    else if (task.IsCompleted)
                    {
                        DataSnapshot snapshot = task.Result;
                        data.Add(paths[getTasks.IndexOf(t)], snapshot);

                        if ((int)Firebase.FirebaseApp.LogLevel < 2)
                            CConsole.Log("FirebaseGetArray("+ getTasks.IndexOf(t) + "):" + ObjectSerializer.JsonToPretty(snapshot.GetRawJsonValue()), Color.green);
                    }

                    if(getTasks.TrueForAll(t2 => t2.IsFaulted || t2.IsCanceled || t2.IsCompleted))
                    {
                        if (completed != null)
                            completed.Invoke( data );
                        completed = null;
                    }
                });
            }
        }

        /// <summary>
        /// Add data with Uniqe key
        /// </summary>
        /// <param name="path"></param>
        /// <param name="o"></param>
        /// <param name="completed"></param>
        public virtual void Put(string path, object o, Action<bool> completed, bool useKey = false)
        {
#if UNITY_EDITOR
            path = path.Split('@')[0];
#endif
            // if needs a key
            string key = useKey ? "/"+Root.Child(path).Push().Key : "";

            Dictionary<string, object> toPuts = new Dictionary<string, object>();

            if (o.GetType().IsPrimitive || o.GetType() == typeof(string))
                //includes key
                toPuts[path + key ] = o;
            else if (o.GetType() == typeof(Dictionary<string, object>))
                toPuts = (Dictionary<string, object>)o;
            else
                toPuts[path + key] = ObjectSerializer.ToDict(o);
            //Debug.Log("Putting . . . :" + toPuts.Keys.Count);

            Root.UpdateChildrenAsync(toPuts).ContinueWith( t => {
                if (t.IsFaulted || t.IsCanceled)
                {
                    OnTaskEndedWithFault(t.Exception);
                    if (completed != null)
                        completed.Invoke(false);
                }
                else if(t.IsCompleted)
                {
                    if(completed !=null)
                        completed.Invoke(true);
                }
            });
        }

        /// <summary>
        /// Change data directly
        /// </summary>
        /// <param name="path"></param>
        /// <param name="o"></param>
        /// <param name="completed"></param>
        public virtual void Post(string path, object o, Action<bool> completed)
        {
#if UNITY_EDITOR
            path = path.Split('@')[0];
#endif
            Task t;
            if (o.GetType().IsPrimitive || o.GetType() == typeof(string))
                t = Root.Child(path).SetValueAsync(o);
            else if (o.GetType() == typeof(Dictionary<string, object>))
                t = Root.Child(path).SetValueAsync((Dictionary<string, object>)o);
            else
                t = Root.Child(path).SetRawJsonValueAsync(ObjectSerializer.ToJson(o));

            t.ContinueWith(task =>
            {
                if (t.IsFaulted || t.IsCanceled)
                {
                    OnTaskEndedWithFault(task.Exception);
                    completed.Invoke(false);
                }
                else if (t.IsCompleted)
                {
                    if (completed != null)
                        completed.Invoke(true);
                }
            });
        }

        /// <summary>
        /// Delete path
        /// </summary>
        /// <param name="pathsToDelete"></param>
        /// <param name="completed"></param>
        public virtual void Del(string pathsToDelete, Action<bool> completed)
        {
            //Debug.Log("Deleting . . . :" );
            Root.Child(pathsToDelete).RemoveValueAsync().ContinueWith(t =>
            {
                if (t.IsFaulted || t.IsCanceled)
                {
                    OnTaskEndedWithFault(t.Exception);
                    completed.Invoke(false);
                }
                else if(t.IsCompleted)
                {
                    if (completed != null)
                        completed.Invoke(true);
                }
            });
        }


        /// <summary>
        /// if any process ended with fault calls this (for error Logging)
        /// </summary>
        /// <param name="completed"></param>
        /// <param name="e"></param>
        protected virtual void OnTaskEndedWithFault(AggregateException e)
        {
            Debug.LogError("DB: "+e);
        }
#endif

        void OnApplicationQuit()
        {
            Local.Save();
        }


        [Serializable]
        public class Deneme
        {
            public Dictionary<Point, string> dict = new Dictionary<Point, string>();
            public string abc = "000000";
            public Point p = new Point(3, 4);
        }

        /// <summary>
        /// For Local object Load/Save (PlayerPrefs)
        /// </summary>
        public static class Local
        {
            public static void Save()
            {
                PlayerPrefs.Save();
            }

            /// <summary>
            /// Delete all local datas
            /// </summary>
            public static void Reset()
            {
                PlayerPrefs.DeleteAll();
            }

            /// <summary>
            /// Set a value
            /// </summary>
            /// <param name="key"></param>
            /// <param name="value"></param>
            public static void Set(string key, object value)
            {
                PlayerPrefs.SetString(key, ObjectSerializer.ToJson(value));
            }

            /// <summary>
            /// Get an object 
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="key"></param>
            /// <returns></returns>
            public static T Get<T>(string key)
            {
                string json = PlayerPrefs.GetString(key, null);
                if (string.IsNullOrEmpty(json))
                    return default(T);
                else
                    return ObjectSerializer.FromJson<T>(json);
            }

            /// <summary>
            /// Get an object 
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="key"></param>
            /// <returns></returns>
            public static T Get<T>(string key,T defaultValue)
            {
                string json = PlayerPrefs.GetString(key, null);
                if (string.IsNullOrEmpty(json))
                    return defaultValue;
                else
                    return ObjectSerializer.FromJson<T>(json);
            }
        }
    }
}
//*/
