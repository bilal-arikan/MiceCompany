﻿using Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public partial class GamePlaySituation
    {
        public bool Passed
        {
            get
            {
                return PassedTimeRequirement && PassedArriveRequirement && PassedNotDeathRequirement;
            }
        }
        public bool PassedTimeRequirement = false;
        public bool PassedArriveRequirement = false;
        public bool PassedNotDeathRequirement = false;

        public int Score = 0;
        public int Star = 0;

        public int AliveMices = 0;
        public int ArrivedMices = 0;
        public int DeathMices = 0;
        public float PassedTime = 0;
        public float RemainingTime = 0;

        public int InGameRevenueMoney = 0;
        public int InGameRevenueSpecMoney = 0;
        public int InGameRevenueXp = 0;

        public PassedSubCh Result()
        {
            return new PassedSubCh()
            {
                Score = this.Score,
                Star = Star,
                Time = PassedTime
            };
        }
    }
}
