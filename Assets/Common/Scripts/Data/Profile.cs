﻿using Common.Managers;
using MarkLight;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UnityEngine;
#if GooglePlayGames
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System.Threading.Tasks;
#endif
#if GameJolt
using GameJolt.API;
using System.Collections;
#endif
#if Facebook
using Facebook.Unity;
#endif

namespace Common.Data
{
    [Serializable]
    public partial class Profile
    {
        [Header("Social Properties:")]
        public string NetworkUserId;
        public string Email;
        public string DisplayName = "Player";
        public string PhoneNumber;
        public string PhotoUrl;
        [JsonIgnore]
        public Sprite Photo;
        [JsonIgnore]
        public SpriteAsset PhotoAsset
        {
            get
            {
                return new SpriteAsset(DisplayName, Photo);
            }
            set
            {
                Photo = value.Sprite;
            }
        }

        public List<Friend> Friends = new List<Friend>();

        [Serializable]
        public class Friend
        {
            public string FirebaseId;
            public SocialManager.Network FriendNetwork;
            public string NetworkUserId;
        }

#if FirebaseAuthentication
        /// <summary>
        /// Overwrite Firebase user informations
        /// </summary>
        /// <param name="User"></param>
        public static Profile Create(Firebase.Auth.IUserInfo User)
        {
            //_FirebaseUser = User;

            var PlayerProfile = new Profile();
            PlayerProfile.NetworkUserId = "F:firebase";
            PlayerProfile.Email = User.Email;
            PlayerProfile.DisplayName = User.DisplayName;
            if (User.PhotoUrl != null)
                PlayerProfile.PhotoUrl = User.PhotoUrl.AbsoluteUri;

            return PlayerProfile;
        }
#endif
#if GooglePlayGames
        /// <summary>
        /// Overwrite GooglePlay user informations
        /// </summary>
        /// <param name="User"></param>
        public static Profile Create(GooglePlayGames.PlayGamesLocalUser User)
        {
            //_GooglePlayUser = User;
            var PlayerProfile = new Profile();
            PlayerProfile.NetworkUserId = "G:"+ User.id;
            PlayerProfile.Email = User.Email;
            PlayerProfile.DisplayName = User.userName;
            PlayerProfile.PhotoUrl = User.AvatarURL;

            PlayerProfile.Friends.RemoveAll(f => f.FriendNetwork == SocialManager.Network.GooglePlay);
            foreach (var f in User.friends)
            {
                // Arkadaşlarda mevcut deilse Ekle
                if (!PlayerProfile.Friends.Exists(fr => fr.NetworkUserId == f.id && fr.FriendNetwork == SocialManager.Network.GooglePlay))
                    PlayerProfile.Friends.Add(new Friend()
                    {
                        FriendNetwork = SocialManager.Network.GooglePlay,
                        NetworkUserId = f.id
                    });
            }
            Debug.Log("GooglePlay Friends: " + PlayerProfile.Friends.Count);

            //Debug.Log("GP:" + ObjectSerializer.ToJson(this));
            return PlayerProfile;
        }
#endif
#if GameJolt
        /// <summary>
        /// Overwrite GameJolt user informations
        /// </summary>
        /// <param name="User"></param>
        public static Profile Create(GameJolt.API.Objects.User User)
        {
            //_GameJoltUser = User;
            var PlayerProfile = new Profile();
            PlayerProfile.NetworkUserId = "J:"+User.ID;
            PlayerProfile.Email = User.Name + "@gamejolt.com";
            PlayerProfile.DisplayName = User.Name;
            PlayerProfile.Photo = User.Avatar;
            PlayerProfile.PhotoUrl = User.AvatarURL;

            return PlayerProfile;
        }
#endif
#if Facebook
        /// <summary>
        /// Overwrite Facebook user informations
        /// </summary>
        /// <param name="User"></param>
        public static Profile Create(Facebook.Unity.IGraphResult r)
        {
            var PlayerProfile = new Profile();

            r.ResultDictionary.TryGetValue("id", out PlayerProfile.NetworkUserId);
            PlayerProfile.NetworkUserId = "F:" + PlayerProfile.NetworkUserId;
            r.ResultDictionary.TryGetValue("name", out PlayerProfile.DisplayName);
            r.ResultDictionary.TryGetValue("email", out PlayerProfile.Email);

            Dictionary<string, object> subInfo;
            if (r.ResultDictionary.TryGetValue("picture", out subInfo))
                if (subInfo.TryGetValue("data", out subInfo))
                {
                    PlayerProfile.PhotoUrl = (string)subInfo["url"];
                    //Debug.Log("PhotoUrl:" + PlayerManager.CurrentPlayer.PhotoUrl);
                }

            PlayerProfile.Friends.RemoveAll(f => f.FriendNetwork == SocialManager.Network.Facebook);
            if (r.ResultDictionary.TryGetValue("friends", out subInfo))
            {
                List<object> friends = subInfo["data"] as List<object>;

                foreach (var f in friends)
                {
                    string id = (string)((Dictionary<string, object>)f)["id"];
                    if (!PlayerProfile.Friends.Exists(fr => fr.NetworkUserId == id && fr.FriendNetwork == SocialManager.Network.Facebook))
                        PlayerProfile.Friends.Add(new Friend()
                        {
                            FriendNetwork = SocialManager.Network.Facebook,
                            NetworkUserId = id
                        });
                }
            }

            return PlayerProfile;
        }
#endif
    }
}
