﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using UnityEngine;
using System.Text;
using UnityEngine.SceneManagement;
using Common.Managers;

namespace Common.Data
{
    public class BugReport
    {
        public static BugReport Create(string message, Exception e)
        {
            var r = new BugReport()
            {
                UsersMessage = message,

                deviceModel = SystemInfo.deviceModel,
                deviceName = SystemInfo.deviceName,
                deviceType = SystemInfo.deviceType,
                deviceUniqueIdentifier = SystemInfo.deviceUniqueIdentifier,

                operatingSystem = SystemInfo.operatingSystem,
                CurrentLanguage = LanguageManager.CurrentLanguage,
                systemMemorySize = SystemInfo.systemMemorySize,
                processorCount = SystemInfo.processorCount,
                processorType = SystemInfo.processorType,

                currentResolutionWidth = Screen.currentResolution.width,
                currentResolutionHeight = Screen.currentResolution.height,
                dpi = Screen.dpi,
                fullScreen = Screen.fullScreen,
                graphicsDeviceName = SystemInfo.graphicsDeviceName,
                graphicsDeviceVendor = SystemInfo.graphicsDeviceVendor,
                graphicsMemorySize = SystemInfo.graphicsMemorySize,
                maxTextureSize = SystemInfo.maxTextureSize,

                sceneCount = SceneManager.sceneCount,
                unityVersion = Application.unityVersion,
                adsEnabled = AdsManager.Instance.enabled
            };
            return r;
        }

        string UsersMessage;
        Type exceptionType;

        string deviceModel;
        string deviceName;
        DeviceType deviceType;
        string deviceUniqueIdentifier;

        string operatingSystem;
        string CurrentLanguage;
        int systemMemorySize;
        int processorCount;
        string processorType;

        int currentResolutionWidth;
        int currentResolutionHeight;
        float dpi;
        bool fullScreen;
        string graphicsDeviceName;
        string graphicsDeviceVendor;
        int graphicsMemorySize;
        int maxTextureSize;

        int sceneCount;
        string unityVersion;
        bool adsEnabled;

        public override string ToString()
        {
            var errorMessage = new StringBuilder();

            errorMessage.AppendLine("MiceCompany: " + Application.platform);

            /*errorMessage.AppendLine();
            errorMessage.AppendLine(message);
            errorMessage.AppendLine(stack);*/

            //if (exception.InnerException != null) {
            //    errorMessage.Append("\n\n ***INNER EXCEPTION*** \n");
            //    errorMessage.Append(exception.InnerException.ToString());
            //}

            errorMessage.AppendFormat
            (
                "{0} {1} {2} {3}\n{4}, {5}, {6}, {7}x {8}\n{9}x{10} {11}dpi FullScreen {12}, {13}, {14} vmem: {15} Max Texture: {16}\nScene {17}, Unity Version {18}, Ads Enabled {19}i Custom Message:{20}",
                deviceModel,
                deviceName,
                deviceType,
                deviceUniqueIdentifier,

                operatingSystem,
                CurrentLanguage,
                systemMemorySize,
                processorCount,
                processorType,

                currentResolutionWidth,
                currentResolutionHeight,
                dpi,
                fullScreen,
                graphicsDeviceName,
                graphicsDeviceVendor,
                graphicsMemorySize,
                maxTextureSize,

                sceneCount,
                unityVersion,
                adsEnabled,
                UsersMessage
            );

            return errorMessage.ToString();
        }
    }
}
