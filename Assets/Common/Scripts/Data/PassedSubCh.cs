﻿using System;

namespace Common.Data
{
    [Serializable]
    public struct PassedSubCh
    {
        public int Score;
        public int Star;
        public float Time;
    }
}
