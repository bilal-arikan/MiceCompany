﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using MarkLight;
using MarkLight.Views.UI;
using Common.Managers;
using UnityEngine.SceneManagement;

namespace Common.Data
{
    [Serializable]
    [CreateAssetMenu(fileName = "SubCh", menuName = "Common/Sub Chapter")]
    public partial class SubChapter : ScriptableObject
    {
        [SerializeField]
        int index;
        public int Index
        {
            get
            {
                return index;
            }
            set
            {
                index = value;
            }
        }

        public Point IndexPoint
        {
            get
            {
                if (ParentChapter != null)
                    return new Point(ParentChapter.Index, Index);
                else
                    return new Point(-1, -1);
            }
        }

        [NonSerialized]
        public Chapter ParentChapter;

        public int SceneBuildIndex;
        [SerializeField]
        UnityEngine.Object scene;
        /*public Scene Scene
        {
            get
            {
                return SceneManager.GetSceneByBuildIndex(SceneBuildIndex);
            }
        }*/

        public bool IsFirst
        {
            get
            {
                return IndexPoint.Y == 1;
            }
        }
        public bool IsLast
        {
            get
            {
                return IndexPoint.Y == LevelLoadManager.Instance.AllChapters[IndexPoint.X].Count;
            }
        }
        public string Header = "???Header";
        public string Summary = "???Summary";
        public Vector2 MapPosition;

        public List<int> StarScore = new List<int>() { 0, 70,120,180};
        public List<int> StarXp = new List<int>() { 0, 2, 5, 10};
        public List<int> StarMoney = new List<int>() { 0, 50,10,150};
        public List<int> StarSpecMoney = new List<int>() { 0,1,2,3};

        /*private void OnEnable()
        {
            //index = ParentChapter.SubChapters.FirstOrDefault(x => x.Value == this).Key;
            Debug.Log("SubChapter OnEnable:" + index);
        }*/

        public SubChapter NextSubCh()
        {
            //Debug.Log("NextSubCh:" + IndexPoint);
            // Son ch ve Son subch ise
            if (LevelLoadManager.Instance.AllChapters[IndexPoint.X].IsLast && this.IsLast)
            {
                return null;
            }
            else if (this.IsLast)
            {
                return LevelLoadManager.Instance.AllChapters[IndexPoint.X + 1][1];
            }
            else
            {
                return LevelLoadManager.Instance.AllChapters[IndexPoint.X][IndexPoint.Y + 1];
            }
        }

        public SubChapter PreviousSubCh()
        {
            if (LevelLoadManager.Instance.AllChapters[IndexPoint.X].IsFirst && this.IsFirst)
            {
                return null;
            }
            else if (this.IsFirst)
            {
                // Önceki Chapterin son SubChapteri
                return LevelLoadManager.Instance.AllChapters[IndexPoint.X - 1][LevelLoadManager.Instance.AllChapters[IndexPoint.X - 1].Count];
            }
            else
            {
                return LevelLoadManager.Instance.AllChapters[IndexPoint.X][IndexPoint.Y - 1];
            }
        }
    }
}
