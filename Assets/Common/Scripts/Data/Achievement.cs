﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Data
{
    [Serializable]
    [CreateAssetMenu(fileName = "Ach-",menuName = "Common/Achievement")]
    public class Achievement : ScriptableObject
    {
        public string GooglePlayId;
        public int GameJoltId;
        public string Name;
        public string Summary;
        public string ImageUrl;
        public float Value = 0;

        public override int GetHashCode()
        {
            int h = Name.GetHashCode();
            if (GooglePlayId != null)
                h *= GooglePlayId.GetHashCode();
            if (GameJoltId != 0)
                h *= GameJoltId.GetHashCode();
            return h;
        }
    }
}
