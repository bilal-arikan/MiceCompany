﻿using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace Common.Tools
{
    /// <summary>
    /// CommonScene
    /// Strores Unloaded Scenes to
    /// </summary>
    [Serializable]
    public partial class CScene
    {
        public CScene(int index, string name, int ch, int subch)
        {
            SceneBuildIndex = index;
            Name = name;
            Ch = ch;
            SubCh = subch;
        }

        /// <summary>
        /// This is not Scene File name !!!
        /// </summary>
        public string Name;
        public int SceneBuildIndex = -1;
        public int Ch;
        public int SubCh;
        public Point ChIndex
        {
            get
            {
                return new Point(Ch, SubCh);
            }
            set
            {
                Ch = value.X;
                SubCh = value.Y;
            }
        }
        public string ScenePath
        {
            get
            {
                return SceneUtility.GetScenePathByBuildIndex(SceneBuildIndex);
            }
        }

    }
}
