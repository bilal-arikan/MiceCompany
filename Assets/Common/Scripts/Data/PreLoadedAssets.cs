﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Common.Tools;
using System.Collections;

namespace Common.Data
{
    public partial class PreLoadedAssets : Singleton<PreLoadedAssets>
    {
        public List<Sprite> Sprites = new List<Sprite>();
        public List<Tutorial> Tutorials = new List<Tutorial>();
        public List<GameObject> GameObjects = new List<GameObject>();

        public Dictionary<string, GameObject> AllPreLoadedPrefabs = new Dictionary<string, GameObject>();

        protected override void Awake()
        {
            base.Awake();

            var fields = GetType().GetFields();
            for (int i=0; i < fields.Length; i++)
            {
                if (fields[i].FieldType.GetInterfaces().Contains(typeof(IEnumerable)))
                {
                    foreach(object o in fields[i].GetValue(this) as IEnumerable)
                    {
                        /*if(o.GetType() == typeof(GameObject))
                            Debug.Log(o + ":" + o.GetType() + ":" + o.GetHashCode() + ":" + ((UnityEngine.GameObject)o).GetPrefabCode() );
                        else if (o.GetType().IsSubclassOf( typeof(Component)))
                            Debug.Log(o + ":" + o.GetType() + ":" + o.GetHashCode() + ":" + ((UnityEngine.Component)o).gameObject.GetPrefabCode());
                    */}
                }
            }

        }
    }
}
