﻿using Common.Managers;
using Common.Tools;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UnityEngine;
#if GooglePlayGames
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System.Threading.Tasks;
#endif
#if GameJolt
using GameJolt.API;
using System.Collections;
#endif
#if Facebook
using Facebook.Unity;
#endif

namespace Common.Data
{
    [Serializable]
    public partial class Player
    {

        [JsonIgnore]
        public string ID
        {
            get
            {
                return AuthenticationManager.CurrentUserId;
            }
        }
        [JsonIgnore]
        public int Level
        {
            get
            {
                int level = 1;
                for(int i = 1; i< PlayerManager.Instance.LevelSteps.Length; i++)
                {
                    if (xp >= PlayerManager.Instance.LevelSteps[i])
                        level++;
                    else
                        break;
                }
                return level;
            }
        }

        [SerializeField]
        [JsonIgnore]
        int xp = 0;

        public int Xp
        {
            get{return xp;}
            set
            {
                int old = Level;
                xp = value;
                if (Level > old && PlayerManager.CurrentProfile != null)
                    EventManager.TriggerObjectEvent(E.PlayerLevelUp, Level);
            }
        }
        public int Money = 1000;
        public int SpecialMoney = 20;

        public HashSet<Achievement> Achievements = new HashSet<Achievement>();

        public HashSet<long> GameOpenedDays = new HashSet<long>();

        public HashSet<Point> WhatchedTutorials = new HashSet<Point>();

        public PublicPassedDict PassedSubChapterScores = new PublicPassedDict();

        public HashSet<Point> UnlockedSubChapters = new HashSet<Point>() {
            new Point(1, 1),
            new Point(1, 2),
            new Point(1, 3),
            new Point(1, 4),
            //new Point(1, 5),
            new Point(1, 6),
            new Point(1, 7),
            new Point(1, 8),
            new Point(1, 9),
            //new Point(1, 10)
        }; // 5. ve 10. bölüm hariç açıktır

        public void AddPassedChapter(int ch, int subch, PassedSubCh psc)
        {
            //Debug.Log("APC:" + ch + ":" + subch + ":" + PassedSubChapters.Count + ":"+ PassedSubChapters[ch].Count);

            // SubChapter mevcutsa
            if (PassedSubChapterScores.ContainsKey(new Point(ch, subch)))
            {
                // Skoru değştir
                PassedSubChapterScores[new Point(ch, subch)] = psc;
            }
            // Chapter mevcut deilse
            else
            {
                // Skoru ekle
                PassedSubChapterScores.Add(new Point(ch, subch), psc);
            }
            //Debug.Log("PSCs: " + ObjectSerializer.ToJson(PassedSubChapterScores, true));
        }
    }

}
