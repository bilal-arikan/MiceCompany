﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class FriendPassedSubCh
    {
        public int Rank = 0;
        public string Id;
        public Profile FriendProfile;
        public PassedSubCh FriendPSC;
    }
}
