﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Contains All Achievement keys
/// </summary>
public enum A
{
    FirstPlaying,
    Win3Stars,
    GeniousMice,
    ArrivedYellowMice,
    ArrivedRedMice,
    UnlockCh2,
    UnlockCh3,
    UnlockCh4,
    UnlockCh5,
    Play3Days,
    Play7Days,
    ZombieApocalypse,
    ShareOnFacebook,
    FirstPlace,
    Earn10VideoGifts,
    New3Friends,
    CallReaper,
    CompleteTheGame
}
