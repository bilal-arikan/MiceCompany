﻿using System;

/// <summary>
/// Contains Event Names
/// </summary>
public enum E
{
    Unknown = 0,

    AppStarting = 101,
    AppStarted = 102,
    ReceivedMessage = 103,
    ReceivedToken = 104,
    ConfigsFetched = 105,
    AvailableNewVersion = 106,

    //FirebaseSignIn = FirebaseSignIn,
    //FirebaseSignOut = FirebaseSignOut,
    PlayerDataSaved = 201,
    PlayerProfileSaved = 202,
    PlayerDataChanged = 203,
    PlayerProfileChanged = 204,
    PlayerPhotoChanged = 205,
    PlayerLevelUp = 206,

    DailyGift = 207,
    RewardVideoGift = 208,
    InvitedGift = 209,
    ItemPurchased = 210,


    LevelLoadingStarted = 301,
    LevelLoadingProgressed = 302,
    LevelLoadingCompleted = 303,
    SubChapterUnlocked = 304,

    PlayerWon = 401,
    PlayerLost = 402,
    GameOnPreparing = 403,
    GameStarted = 404,
    GamePaused = 405,
    GameUnPaused = 406,
    GameFinished = 407,


    MiceBorn = 901,
    MiceArrived = 902,
    MiceDeath = 903,
    ItemPlaced = 904,
    ItemRemoved = 905,
    ItemMoved = 906,
    ItemRotated = 907,
    ItemDestroyed = 908,
    LootableLooted = 909,

}
