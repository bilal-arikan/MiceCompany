using Common.Tools;
using Common.Managers;
using System.Collections.Generic;
using Common;
using Common.Data;

namespace Common.Managers {
    public partial class ConfigManager : Singleton<ConfigManager> {
        Dictionary<string, object> DefaultConfigs  = new Dictionary<string, object>(){

            {"googlestore_version","1"},
            {"def_language","en"},
            {"special_day"," "},
            {"invite_title","Mice Company"},
            {"invite_message","You Really Have to Try This Game !!!"},
            {"invite_image","https://firebasestorage.googleapis.com/v0/b/mice-company-98092420.appspot.com/o/Defaults%2FMiceCompany-Icon150x150.png?alt=media&token=a3c14b6b-af73-43d1-a923-a0f1b9f319c3"},
            {"invite_deep_link","https://trello.com/b/MWuPFYRb/mice-company"},
            {"website_url","https://www.flopar.com"},
            {"playstore_url","http://play.google.com/store/apps/details?id=com.bilalarikan.micecompany"},
            {"applestore_url","itms://itunes.apple.com/us/app/apple-store/{0}?mt=8"},
            {"facebook_page","https://www.facebook.com"},
            {"twitter_page","https://www.twitter.com"},
            {"googleplus_page","https://www.google.com"},
            {"instagram_page","https://www.instagram.com"},
            {"news_json","[]"},
            {"releases_json","[]"},

        };

        public static string googlestore_version { get { return ConfigManager.Instance.GetConfigValue("googlestore_version"); } }
        public static string def_language { get { return ConfigManager.Instance.GetConfigValue("def_language"); } }
        public static string special_day { get { return ConfigManager.Instance.GetConfigValue("special_day"); } }
        public static string invite_title { get { return ConfigManager.Instance.GetConfigValue("invite_title"); } }
        public static string invite_message { get { return ConfigManager.Instance.GetConfigValue("invite_message"); } }
        public static string invite_image { get { return ConfigManager.Instance.GetConfigValue("invite_image"); } }
        public static string invite_deep_link { get { return ConfigManager.Instance.GetConfigValue("invite_deep_link"); } }
        public static string website_url { get { return ConfigManager.Instance.GetConfigValue("website_url"); } }
        public static string playstore_url { get { return ConfigManager.Instance.GetConfigValue("playstore_url"); } }
        public static string applestore_url { get { return ConfigManager.Instance.GetConfigValue("applestore_url"); } }
        public static string facebook_page { get { return ConfigManager.Instance.GetConfigValue("facebook_page"); } }
        public static string twitter_page { get { return ConfigManager.Instance.GetConfigValue("twitter_page"); } }
        public static string googleplus_page { get { return ConfigManager.Instance.GetConfigValue("googleplus_page"); } }
        public static string instagram_page { get { return ConfigManager.Instance.GetConfigValue("instagram_page"); } }
        public static string news_json { get { return ConfigManager.Instance.GetConfigValue("news_json"); } }
        public static string releases_json { get { return ConfigManager.Instance.GetConfigValue("releases_json"); } }
    }
}

[System.Serializable]
public class PointTutorialDict : Dict<Point, Tutorial> { }

[System.Serializable]
public class IntChapterDict : Dict<int, Chapter> { }

[System.Serializable]
public class AEnumAchievementDict : Dict<A, Achievement> { }

[System.Serializable]
public class PublicPassedDict : Dict<Point, PassedSubCh> { }