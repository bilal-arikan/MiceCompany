﻿using System;
using System.Collections.Generic;

public static class IAP
{
    public static class Cons
    {
        public const string Gold50 = "com.bilalarikan.micecompany.gold50";
        public const string Gold200 = "com.bilalarikan.micecompany.gold200";
    }

    public static class NonCons
    {
        public const string NoAds = "com.bilalarikan.micecompany.noads";
        public const string NoAdsShort = "noads";
    }
}
