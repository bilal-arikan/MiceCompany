import csv
import sys
import urllib.request
from io import StringIO,BytesIO

if(len(sys.argv) == 1):
	file = open("Configs.csv","r",encoding="utf8")
else:
	file = open(sys.argv[1],"r",encoding="utf8")
reader = csv.DictReader(file, delimiter=',')

fileXml = open(r"C.cs","w",encoding="utf8")

fileXml.write('using System.Collections.Generic;\nnamespace Common.Managers{\npublic static partial class DatabaseManager{\n')
fileXml.write('static Dictionary<string, object> InitDefaultDict(){\n\tDictionary<string, object> defaults = new Dictionary<string, object>(){\n\n')
#---------------------------------------------------------------------
for row in reader:
	if(row["type"] == "s"):
		fileXml.write('\t\t{"'+row["key"]+'","'+row["value"]+'"},\n')
	elif(row["type"] == "i" or row["type"] == "f" or row["type"] == "b"):
		fileXml.write('\t\t{"'+row["key"]+'",'+row["value"]+'},\n')
#---------------------------------------------------------------------
fileXml.write('\n};return defaults;}}\npublic static class C{\n\n')
file.seek(0)
#---------------------------------------------------------------------
for row in reader:
	if(row["type"] == "b"):
		typ = "bool"
		ret = "BooleanValue"
	elif(row["type"] == "i"):
		typ = "int"
		ret = "LongValue"	
	elif(row["type"] == "f"):
		typ = "float"
		ret = "DoubleValue"
	elif(row["type"] == "s"):
		typ = "string"
		ret = "StringValue"
	else:
		continue

	fileXml.write('\t\tpublic static '+typ+' '+row["key"]+' {get{return DatabaseManager.GetConfigValue("'+row["key"]+'").'+ret+';}}\n')
#---------------------------------------------------------------------
fileXml.write('\n}}')
file.close()
fileXml.close()
print("SUCCESS")