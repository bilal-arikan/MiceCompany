﻿using System;
using System.Collections.Generic;

[Serializable]
public struct Point 
{
    [Newtonsoft.Json.JsonConstructor]
    public Point(int x, int y)
    {
        X = x;
        Y = y;
    }

    public int X;
    public int Y;

    public override string ToString()
    {
        return X.ToString("00") + Y.ToString("00");
    }

    public bool Equals(Point other)
    {
        return X == other.X && Y == other.Y;
    }

    public override int GetHashCode()
    {
        return X.GetHashCode() ^ Y.GetHashCode();
    }

    public static Point FromString(string s)
    {
        Point p = new Point();
        p.X = int.Parse(s.Substring(0, 2));
        p.Y = int.Parse(s.Substring(2, 2));
        return p;
    }
}
