﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Tools
{
    /// <summary>
    /// Açı işlemlerini kolaylaştırması için
    /// </summary>
    [Serializable]
    public struct Angle 
    {
        public const float TwoPI = 360;//6.283185307f;
        public float PI { get { return Mathf.PI; } }
        [SerializeField]
        private float value;
        [SerializeField]
        public readonly float Radian;
        static float mod = 0;

        Angle(float value)
        {
            this.value = value % TwoPI;
            if (value < 0)
                this.value += TwoPI;
            this.Radian = value * Mathf.Deg2Rad;
        }


        public static Angle operator +(Angle first, Angle second)
        {
            mod = (first.value + second.value) % TwoPI;
            return new Angle(mod < 0 ? mod + TwoPI : mod);
        }
        public static Angle operator -(Angle first, Angle second)
        {
            mod = (first.value - second.value) % TwoPI;
            return new Angle(mod < 0 ? mod + TwoPI : mod);
        }
        public static Angle operator *(Angle first, Angle second)
        {
            mod = (first.value * second.value) % TwoPI;
            return new Angle(mod < 0 ? mod + TwoPI : mod);
        }
        public static Angle operator /(Angle first, Angle second)
        {
            mod = (first.value / second.value) % TwoPI;
            return new Angle(mod < 0 ? mod + TwoPI : mod);
        }

        public static implicit operator float(Angle from)
        {
            return from.value;
        }
        public static implicit operator Angle(float value)
        {
            mod = value % TwoPI;
            return new Angle(mod < 0 ? mod + TwoPI : mod);
        }

        public static implicit operator Vector2(Angle from)
        {
            return new Vector2(Mathf.Cos(from.Radian), Mathf.Sin(from.Radian));
        }
        public static implicit operator Angle(Vector2 value)
        {
            //return new Angle(Vector2.Angle(Vector2.right, value)*Mathf.Deg2Rad);
            return new Angle(Mathf.Atan2(value.y, value.x) * Mathf.Rad2Deg);
        }

        public override string ToString()
        {
            return value.ToString();
        }

        public static Angle Lerp(Angle a, Angle b, float alpha, bool shortest = true)
        {
            float dist = Mathf.DeltaAngle(a, b);
            if (shortest)
                return a + (dist * alpha);
            else
                return a + (dist * alpha) + TwoPI/2;
        }

        public static Angle Random()
        {
            return new Angle(UnityEngine.Random.Range(0, TwoPI));
        }

        public static Angle Random(Angle a, Angle b, bool shortest = true)
        {
            return Lerp(a, b, UnityEngine.Random.Range(0f,1f), shortest);
        }

        public static Angle Mean(List<Angle> angles)
        {
            Vector2 sum = Vector2.zero;

            foreach (Angle a in angles)
                sum += (Vector2)a;

            return (Angle)sum;
        }
    }
}
