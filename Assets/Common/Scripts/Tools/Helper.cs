﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public static class Helper
{
    /// <summary>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="ts"></param>
    public static void Shuffle<T>(this IList<T> ts)
    {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i)
        {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }

    /// <summary>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="original"></param>
    /// <param name="destination"></param>
    /// <returns></returns>
    public static T CopyComponent<T>(T original, GameObject destination) where T : Component
    {
        System.Type type = original.GetType();
        var dst = destination.GetComponent(type) as T;
        if (!dst) dst = destination.AddComponent(type) as T;
        var fields = type.GetFields();
        foreach (var field in fields)
        {
            if (field.IsStatic) continue;
            field.SetValue(dst, field.GetValue(original));
        }
        var props = type.GetProperties();
        foreach (var prop in props)
        {
            if (!prop.CanWrite || !prop.CanWrite || prop.Name == "name") continue;
            prop.SetValue(dst, prop.GetValue(original, null), null);
        }
        return dst as T;
    }

    /// <summary>
    /// Moves an object from point A to point B in a given time
    /// </summary>
    /// <param name="movingObject">Moving object.</param>
    /// <param name="pointA">Point a.</param>
    /// <param name="pointB">Point b.</param>
    /// <param name="time">Time.</param>
    public static IEnumerator ObjectMoveFromTo(GameObject movingObject, Vector3 pointA, Vector3 pointB, float time, float approximationDistance)
    {
        float t = 0f;

        float distance = Vector3.Distance(movingObject.transform.position, pointB);

        while (distance >= approximationDistance)
        {
            distance = Vector3.Distance(movingObject.transform.position, pointB);
            t += Time.deltaTime / time;
            movingObject.transform.position = Vector3.Lerp(pointA, pointB, t);
            yield return 0;
        }
        yield break;
    }

}
