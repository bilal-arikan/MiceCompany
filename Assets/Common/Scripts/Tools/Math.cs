﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Tools
{
    public static class Math
    {
        /// <summary>
        /// 2 * PI
        /// </summary>
        public static float TwoPI
        {
            get
            {
                return 6.28318530718f;
            }
        }

        /// <summary>
		/// Returns the result of rolling a dice of the specified number of sides
		/// </summary>
		/// <returns>The result of the dice roll.</returns>
		/// <param name="numberOfSides">Number of sides of the dice.</param>
		public static int RollADice(int numberOfSides = 6)
        {
            return (UnityEngine.Random.Range(1, numberOfSides));
        }

        /// <summary>
		/// Returns the sum of all the int passed in parameters
		/// </summary>
		/// <param name="thingsToAdd">Things to add.</param>
		public static int Sum(params int[] thingsToAdd)
        {
            int result = 0;
            for (int i = 0; i < thingsToAdd.Length; i++)
            {
                result += thingsToAdd[i];
            }
            return result;
        }
        
        /// <summary>
        /// Verilen bütün Vector3 leri toplar
        /// </summary>
        /// <param name="thingsToAdd"></param>
        /// <returns></returns>
        public static Vector3 Vector3Sum(params Vector3[] thingsToAdd)
        {
            Vector3 result = Vector3.zero;
            for (int i = 0; i < thingsToAdd.Length; i++)
            {
                result += thingsToAdd[i];
            }
            return result;
        }

        /// <summary>
		/// Returns a random vector3 from 2 defined vector3.
		/// </summary>
		/// <returns>The vector3.</returns>
		/// <param name="min">Minimum.</param>
		/// <param name="max">Maximum.</param>
		public static Vector3 RandomVector3(Vector3 minimum, Vector3 maximum)
        {
            return new Vector3(UnityEngine.Random.Range(minimum.x, maximum.x),
                                             UnityEngine.Random.Range(minimum.y, maximum.y),
                                             UnityEngine.Random.Range(minimum.z, maximum.z));
        }

        /// <summary>
		/// Rotates a point around the given pivot.
		/// </summary>
		/// <returns>The new point position.</returns>
		/// <param name="point">The point to rotate.</param>
		/// <param name="pivot">The pivot's position.</param>
		/// <param name="angle">The angle we want to rotate our point.</param>
		public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, float angle)
        {
            angle = angle * (Mathf.Deg2Rad);
            var rotatedX = Mathf.Cos(angle) * (point.x - pivot.x) - Mathf.Sin(angle) * (point.y - pivot.y) + pivot.x;
            var rotatedY = Mathf.Sin(angle) * (point.x - pivot.x) + Mathf.Cos(angle) * (point.y - pivot.y) + pivot.y;
            return new Vector3(rotatedX, rotatedY, 0);
        }

        /// <summary>
        /// Rotates a point around the given pivot.
        /// </summary>
        /// <returns>The new point position.</returns>
        /// <param name="point">The point to rotate.</param>
        /// <param name="pivot">The pivot's position.</param>
        /// <param name="angles">The angle as a Vector3.</param>
        public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angle)
        {
            // we get point direction from the point to the pivot
            Vector3 direction = point - pivot;
            // we rotate the direction
            direction = Quaternion.Euler(angle) * direction;
            // we determine the rotated point's position
            point = direction + pivot;
            return point;
        }

        /// <summary>
        /// verilen 2 açıdan targete en yakın olanı 1.si uzak olanı 2. si
        /// olcak şekilde 2 boyutunda array döndürür
        /// </summary>
        /// <param name="target"></param>
        /// <param name="a1"></param>
        /// <param name="a2"></param>
        /// <returns></returns>
        public static Angle[] ClosestAngle(Angle target, Angle a1, Angle a2)
        {
            float a1Range = Mathf.DeltaAngle(a1, target);
            float a2Range = Mathf.DeltaAngle(a2, target);

            a1Range = Mathf.Abs(a1Range );
            a2Range = Mathf.Abs(a2Range );

            if (a1Range < a2Range)
                return new Angle[] { a1, a2 };
            else
                return new Angle[] { a2, a1 };
        }
    }


}
