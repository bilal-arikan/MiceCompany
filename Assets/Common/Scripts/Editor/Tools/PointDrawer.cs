﻿//C# Example

using UnityEngine;
using UnityEditor;
using System.Collections;

    [CustomPropertyDrawer(typeof (Point))]
    internal class PointDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            label = EditorGUI.BeginProperty(position, label, property);
            Rect contentPosition = EditorGUI.PrefixLabel(position, label);
            EditorGUIUtility.labelWidth = 12f;
            contentPosition.width = 80f;
            EditorGUI.indentLevel = 0;
            EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("X"), new GUIContent("X"));
            contentPosition.x += contentPosition.width + 2;
            EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("X"), new GUIContent("Y"));
            EditorGUI.EndProperty();
        }
    }
