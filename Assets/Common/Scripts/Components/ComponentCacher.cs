﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Components
{
    [ExecuteInEditMode]
    public class ComponentCacher : MonoBehaviour
    {
        private void Awake()
        {
            if (!gameObject.IsCached())
                gameObject.CacheComponents();
        }

        private void OnDestroy()
        {
            gameObject.UnCacheComponents();
        }
    }
}
