﻿using Common.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Common.Components
{
    public class Collider2DTrigger : MonoBehaviour
    {

        [Range(0, 1)]
        public float TriggerChance = 1;
        public UnityEvent ToTriggered;
        public List<Type> TriggerableComponentTypes = new List<Type>() { typeof(PlayerMice) };


        protected void OnCollisionEnter2D(Collision2D other)
        {
            if (TriggerableComponentTypes.Count > 0)
                foreach (var t in TriggerableComponentTypes)
                {
                    if (other.gameObject.GetComponent(t) != null)
                    {
                        if (TriggerChance == 1 || UnityEngine.Random.Range(0, 1) < TriggerChance)
                            ToTriggered.Invoke();
                        break;
                    }
                }
            else
            {
                if (TriggerChance == 1 || UnityEngine.Random.Range(0, 1) < TriggerChance)
                    ToTriggered.Invoke();
            }
        }

        protected void OnTriggerEnter2D(Collider2D other)
        {
            if (TriggerableComponentTypes.Count > 0)
                foreach (var t in TriggerableComponentTypes)
                {
                    if (other.gameObject.GetCachedComponent(t) != null)
                    {
                        if (TriggerChance == 1 || UnityEngine.Random.Range(0, 1) < TriggerChance)
                            ToTriggered.Invoke();
                        break;
                    }
                }
            else
            {
                if (TriggerChance == 1 || UnityEngine.Random.Range(0, 1) < TriggerChance)
                    ToTriggered.Invoke();
            }
        }
    }
}
