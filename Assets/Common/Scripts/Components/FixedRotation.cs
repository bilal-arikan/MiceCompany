﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[ExecuteInEditMode]
public class FixedRotation : MonoBehaviour
{
    public Vector3 rotation = new Vector3(0,1,0);

    private void Update()
    {
        if(rotation != Vector3.zero)
            transform.rotation = Quaternion.LookRotation(rotation);
    }
}
