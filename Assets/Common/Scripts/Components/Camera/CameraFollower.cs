﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Tools
{
    public class CameraFollower : MonoBehaviour
    {
        public Rigidbody2D Followed;
        public float CameraPositionLerp = 0.02f;
        public float VelocityMul = 1;
        public float VelocitySmoothnessLerp = 0.9f;
        public float MinAccountedSpeed = 10;
        public float CamBordersMul = 0.8f;
        public float InstantJumpDistance = 50;

        private Vector2 _smoothVelocity;
        private Camera _camera;

        private void OnEnable()
        {
            if(_camera == null)
                SetCamera(Camera.main);
            else
                SetCamera(_camera);

        }

        private void Start()
        {
            if (_camera == null)
                SetCamera(Camera.main);
            else
                SetCamera(_camera);
        }

        private void LateUpdate()
        {
            if (Followed != null)
            {
                var camPos = _camera.transform.position;
                var followedPos = Followed.position;

                var vel = Followed.velocity.sqrMagnitude > MinAccountedSpeed * MinAccountedSpeed
                    ? Followed.velocity
                    : Vector2.zero;
                _smoothVelocity = Vector2.Lerp(vel, _smoothVelocity, VelocitySmoothnessLerp);

                var camTargetPos = followedPos + _smoothVelocity * VelocityMul;
                var camHalfWidth = _camera.orthographicSize * _camera.aspect * CamBordersMul;
                var camHalfHeight = _camera.orthographicSize * CamBordersMul;
                var followedDir = followedPos - camTargetPos;

                if (followedDir.x > camHalfWidth)
                    camTargetPos.x = followedPos.x - camHalfWidth;
                if (followedDir.x < -camHalfWidth)
                    camTargetPos.x = followedPos.x + camHalfWidth;
                if (followedDir.y > camHalfHeight)
                    camTargetPos.y = followedPos.y - camHalfHeight;
                if (followedDir.y < -camHalfHeight)
                    camTargetPos.y = followedPos.y + camHalfHeight;

                var pos = (followedPos - (Vector2)camPos).sqrMagnitude < InstantJumpDistance * InstantJumpDistance
                    ? Vector2.Lerp(camPos, camTargetPos, CameraPositionLerp * Time.deltaTime)
                    : followedPos;

                _camera.transform.position = new Vector3(pos.x, pos.y, camPos.z);
                camPos = pos;
            }
        }

        public void SetCamera(Camera cam)
        {
            _camera = cam;
            if(Followed != null)
                _camera.transform.position = new Vector3(Followed.position.x, Followed.position.y, _camera.transform.position.z);
        }
    }
}