﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using UnityEngine;
using System.Collections;

namespace Common.Tools
{	
	/// <summary>
	/// Add this component to an object and it'll be auto destroyed X seconds after its Start()
	/// </summary>
	public class TimedAutoDestroy : MonoBehaviour
	{
	    /// The time (in seconds) before we destroy the object
	    public float TimeBeforeDestruction=2;
        public bool InRealTime = false;
		/// <summary>
		/// On Start(), we schedule the object's destruction
		/// </summary>
		protected virtual void Start ()
	    {
	        StartCoroutine(Destruction());
		}
		
		/// <summary>
		/// Destroys the object after TimeBeforeDestruction seconds
		/// </summary>
	    protected virtual IEnumerator Destruction()
	    {
            if(InRealTime)
                yield return new WaitForSecondsRealtime(TimeBeforeDestruction);
            else
                yield return new WaitForSeconds(TimeBeforeDestruction);
	        Destroy(gameObject);
	    }
	}
}
