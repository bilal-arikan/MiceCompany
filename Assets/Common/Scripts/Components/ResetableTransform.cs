﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arıkan. All Rights Reserved.
// Author: Bilal Arıkan
// Time  : 04.11.2017   
//***********************************************************************//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Common.Tools
{
    public class ResetableTransform : MonoBehaviour
    {
        static List<ResetableTransform> AllResetables = new List<ResetableTransform>();

        Rigidbody rb;
        Rigidbody2D rb2D;

        [SerializeField]
        Vector3 DefaultPosition;
        [SerializeField]
        Quaternion DefaultRotation;
        [SerializeField]
        Vector3 DefaultScale;
        [SerializeField]
        Vector3 DefaultVelocity;
        [SerializeField]
        Vector3 DefaultAngVelocity;


        void Start()
        {
            rb = GetComponent<Rigidbody>();
            rb2D = GetComponent<Rigidbody2D>();

            AllResetables.Add(this);
            SaveState();
        }

        public void SaveState()
        {
            DefaultPosition = transform.localPosition;
            DefaultRotation = transform.localRotation;
            DefaultScale = transform.localScale;
            if (rb != null)
            {
                DefaultVelocity = rb.velocity;
                DefaultAngVelocity = rb.angularVelocity;
            }
            else if (rb2D != null)
            {
                DefaultVelocity = rb2D.velocity;
                DefaultAngVelocity = new Vector3(rb2D.angularVelocity, 0, 0);
            }
        }

        public void Reset()
        {
            if (rb != null)
            {
                rb.velocity = DefaultVelocity;
                rb.angularVelocity = DefaultAngVelocity;
            }
            else if (rb2D != null)
            {
                rb2D.velocity = DefaultVelocity;
                rb2D.angularVelocity = DefaultAngVelocity.x;
            }


            transform.localPosition = DefaultPosition;
            transform.localRotation = DefaultRotation;
            transform.localScale = DefaultScale;
        }

        protected void OnDestroy()
        {
            AllResetables.Remove(this);
        }

        public static void ResetAll()
        {
            AllResetables.ForEach((r) => { r.Reset(); });
        }
    }
}
