﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[ExecuteInEditMode]
public class FixedLocalPosition : MonoBehaviour
{
    public bool useStartedPosition = false;
    public Transform Parent;
    public Vector3 startLocalPosition;


    private void Start()
    {
        if (Parent != null && useStartedPosition)
            startLocalPosition = transform.position - Parent.position;
    }

    private void Update()
    {
        if(Parent != null)
            transform.position = Parent.position + startLocalPosition;
    }
}
