﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Common.Components
{
    public class PeriodicTrigger : MonoBehaviour
    {
        public bool AutoStart = true;
        public float MinStepSeconds = 3;
        public float MaxStepSeconds = 4;
        public bool UseRealTime = false;
        [Range(0,1)]
        public float TriggerChance = 1;
        public UnityEvent ToTriggered;

        private void Start()
        {
            if (AutoStart)
                StartCoroutine(TriggerCoroutine());
        }

        public void StartPeriodicTrigger()
        {
            StartCoroutine(TriggerCoroutine());
        }

        IEnumerator TriggerCoroutine()
        {
            while (true)
            {
                if (!UseRealTime)
                    yield return new WaitForSeconds(UnityEngine.Random.Range(MinStepSeconds,MaxStepSeconds));
                else
                    yield return new WaitForSecondsRealtime(UnityEngine.Random.Range(MinStepSeconds, MaxStepSeconds));

                if(TriggerChance == 1 || UnityEngine.Random.Range(0,1) < TriggerChance)
                    ToTriggered.Invoke();
            }
        }
    }
}
